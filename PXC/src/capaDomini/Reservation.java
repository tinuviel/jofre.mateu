/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 *
 * @author Jofre
 */
public class Reservation implements Serializable{
    private int id;
    private ArrayList<Comand> usuaris;
    private GregorianCalendar date;
    private boolean pendent;
    private ArrayList<Reservation> reservations;
    
    public Reservation() {
        this.usuaris = new ArrayList<Comand>();
        this.reservations = new ArrayList<Reservation>();
    }
    
    public Reservation(int id, GregorianCalendar date, boolean pendent) {
        this.id = id;
        this.date = date;
        this.pendent = pendent;
        this.usuaris = new ArrayList<Comand>();
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public boolean isPendent() {
        return pendent;
    }

    public ArrayList<Comand> getUsuaris() {
        return usuaris;
    }

    public ArrayList<Reservation> getReservations() {
        return reservations;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsuaris(ArrayList<Comand> usuaris) {
        this.usuaris = usuaris;
    }

    public void setPendent(boolean pendent) {
        this.pendent = pendent;
    }

    public void setReservations(ArrayList<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Reservation searchId(int id) {
        Iterator<Reservation> it = this.reservations.listIterator();
        Reservation reservation;
        while (it.hasNext()) {
            reservation = it.next();
            if(reservation.id == id) {
                return reservation;
            }
        }
        return null;
    }
    
    
}

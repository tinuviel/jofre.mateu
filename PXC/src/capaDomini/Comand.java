/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;


/**
 *
 * @author Jofre
 */
public class Comand implements Serializable{
    private String username;
    private String starter;
    private String second;
    private String dessert;
    private String drink;

    public Comand(String username, String starter, String second, String dessert, String drink) {
        this.username = username;
        this.starter = starter;
        this.second = second;
        this.dessert = dessert;
        this.drink = drink;
    }

    public Comand() {
        
    }

    public String getDessert() {
        return dessert;
    }

    public String getDrink() {
        return drink;
    }

    public String getSecond() {
        return second;
    }

    public String getStarter() {
        return starter;
    }

    public String getUsername() {
        return username;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public void setStarter(String starter) {
        this.starter = starter;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import capaDades.DataControler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author Jofre
 */
public class DomainControler {
    private DataControler data;
    private Restaurant usuari = null;
    private Dish dish_backup = null;
    private Menu menu_backup;
    private Restaurant rest_backup;
    private ArrayList<Reservation> reservations;
    private int MAX_TYPE_IN_MENU = 3;
    
    private SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MMMM/yyyy");
    private SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm");
    
    static private DomainControler domini = null;
    
    

    private DomainControler() {
        data = DataControler.getInstancia();
    }
    
    static public DomainControler getInstancia(){

        if (domini == null) {
            domini = new DomainControler();
        }
        return domini;
    }
    
    public boolean login(String restname, String password) throws RemoteException, NotBoundException, MalformedURLException, AccessException, IOException {        
        if(data.correctLogin(restname, password)) {
            this.usuari = data.login(restname, password);            
            System.out.println("Login succesful");
            return true;
        }   
        System.out.println("Incorrect username or password");
        return false;
    }
    
    public void registre(String rest_name, String password, String mail, String phone, String address, String description, String category, String img) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        this.usuari = new Restaurant(rest_name, mail, phone, address, description, password, category, img);
        data.registre(rest_name, password, mail, phone, address, description, category, img);
    }

    public boolean existRestname(String restname) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        boolean res = data.existRestaurant(restname);
        if(res) {
            System.out.println("This restaurant name is already in use, insert a new one");
        }
        return res;
    }

    public Restaurant getUser() {
        return this.usuari;
    }

    public void updateRestaurant() throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        String old_rest_name = this.usuari.getName();
        this.usuari.setAll(rest_backup);
        data.updateRestaurant(old_rest_name, usuari.getName(), usuari.getPassword(), usuari.getAddress(), 
                usuari.getMail(), usuari.getPhone(), usuari.getDescription(), usuari.getCategory(), usuari.getImg());
    }

    public void logout() {
        this.usuari = null;
    }

    public void listDishes() throws RemoteException, NotBoundException, AccessException, MalformedURLException, IOException {
        ArrayList<Dish> list = this.usuari.getDishes();
        Iterator<Dish> iterador = list.listIterator();
        int i = 1;
        while(iterador.hasNext()) {
            System.out.println(i++ + "-" + iterador.next().getName());
        }
    }

    public void addDish(String dishName, String dishType, float dishPrice, String dishDescription) throws AccessException, RemoteException, NotBoundException, MalformedURLException, IOException {
        Dish dish = new Dish(dishName, dishType, dishPrice, dishDescription);
        this.listDishes();        
        this.usuari.addDish(dish);
        data.addDish(this.usuari.getName(),dish);
        this.sortDishes(this.usuari.getDishes());
        this.listDishes();
    }

    public boolean existDish(String dishName) throws RemoteException, AccessException, MalformedURLException, NotBoundException, IOException {
        boolean res = false;
        Iterator<Dish> iterator = this.usuari.getDishes().listIterator();
        while (iterator.hasNext() && !res) {
            if (iterator.next().getName().equals(dishName)) res = true;
        }
        if(res) System.out.println("This dish name is already in use, insert a new one");
        return res;
    }

    public void showDish(int dish_index) {
        Dish dish = this.usuari.getDishes().get(dish_index);
        System.out.println("\nName: " + dish.getName());
        System.out.println("Type: " + dish.getType());
        System.out.println("Description: " + dish.getDescription());
        System.out.println("Price: " + ((Float)dish.getPrice()).toString());
    }

    public void editingDish(int editing_dish) {
        System.out.println("\nEditing: " + this.usuari.getDishes().get(editing_dish).getName());
    }

    public void updateDishType(String dish_type) {
        this.dish_backup.setType(dish_type);
    }

    public void updateDishDescription(String dish_description) {
        this.dish_backup.setDescription(dish_description);
    }

    public void updateDishPrice(float dish_price) {
        this.dish_backup.setPrice(dish_price);
    }

    public void updateDish(int editing_dish) throws AccessException, RemoteException, NotBoundException, MalformedURLException, IOException {
        String old_dish_name = this.usuari.getDishes().get(editing_dish).getName();
        this.usuari.getDishes().get(editing_dish).setAll(this.dish_backup);
        data.updateDish(this.usuari.getName(), old_dish_name, this.dish_backup);
        //Busco pels menus on hi és i canvio el plat
        Iterator<Menu> menu_it = this.usuari.getMenus().listIterator();
        while(menu_it.hasNext()) {
            Iterator<Dish> dishes_it = menu_it.next().getDishes().listIterator();
            boolean t = false;
            while(dishes_it.hasNext() && !t) {
                Dish dish = dishes_it.next();
                if (dish.getName().equals(old_dish_name)) {
                    t = true;
                    dish.setAll(this.dish_backup);
                }
            }
        }
    }

    public void deleteDish(int deleting_dish) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        data.deleteDish(this.usuari.getName(), this.getUser().getDishes().get(deleting_dish).getName());
        this.usuari.getDishes().remove(deleting_dish);
    }

    public boolean isValidDishIndex(int dish_index) {
        return (dish_index >=0 && dish_index < this.usuari.getDishes().size());
    }

    public void copyDish(int editing_dish) {
        this.dish_backup = new Dish(this.usuari.getDishes().get(editing_dish));
    }

    public void updateDishName(String dishName) {
        this.dish_backup.setName(dishName);
    }

    public void listMenus() {
        ArrayList<Menu> list = this.usuari.getMenus();
        Iterator<Menu> iterador = list.listIterator();
        int i = 1;
        while(iterador.hasNext()) {
            System.out.println(i++ + "-" + iterador.next().getName());
        }
    }

    public boolean isValidMenuIndex(int menu_index) {
        return (menu_index >=0 && menu_index < this.usuari.getMenus().size());
    }

    public void showMenu(int menu_index) {
        this.showMenu(this.usuari.getMenus().get(menu_index));
    }

    private void listDishes(ArrayList<Dish> dishes) {
        Iterator<Dish> iterador = dishes.listIterator();
        int i = 1;
        while(iterador.hasNext()) {
            System.out.println(i++ + "-" + iterador.next().getName());
        }
    }

    public boolean canModifyDish(int editing_dish) {
        String dish_name = this.usuari.getDishes().get(editing_dish).getName();
        Iterator<Menu> iterador = this.usuari.getMenus().listIterator();
        Menu menu;
        while(iterador.hasNext()) {
            menu = iterador.next();
            Iterator<Dish> it = menu.getDishes().listIterator();
            while(it.hasNext()) {
                if (it.next().getName().equals(dish_name)) {
                    if (menu.isAvailable())return false;
                    else {
                        System.out.println("This menú will be modified:");
                        this.showMenu(menu);
                    }
                }
            }
        }
        return true;
    }

    public boolean existMenu(String menu_name) {
        boolean res = false;
        Iterator<Menu> iterator = this.usuari.getMenus().listIterator();
        while (iterator.hasNext() && !res) {
            if (iterator.next().getName().equals(menu_name)) res = true;
        }
        if(res) System.out.println("This menu name is already in use, insert a new one");
        return res;
    }

    public void addMenu(String menu_name, float menu_price, boolean menu_available) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        Menu menu = new Menu(menu_name, menu_price, menu_available);
        this.usuari.addMenu(menu);
        data.addMenu(this.usuari.getName(), menu);
    }

    public boolean canModifyMenu(int editing_menu) {
        return !this.usuari.getMenus().get(editing_menu).isAvailable();
    }

    public void copyMenu(int editing_menu) {
        this.menu_backup = new Menu(this.usuari.getMenus().get(editing_menu));
    }

    public void updateMenuName(String menu_name) {
        this.menu_backup.setName(menu_name);
    }

    public void updateMenuPrice(float menu_price) {
        this.menu_backup.setPrice(menu_price);
    }

    public void updateMenuAvailable(boolean menu_available) {
        this.menu_backup.setAvailable(menu_available);
    }

    public void updateMenu(int editing_menu) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        String old_menu_name = this.usuari.getMenus().get(editing_menu).getName();
        this.usuari.getMenus().get(editing_menu).setAll(this.menu_backup);
        data.updateMenu(this.usuari.getName(), old_menu_name, this.menu_backup);
    }

    public void listMenuNoDishes() {
        Iterator<Dish> dishes = this.usuari.getDishes().listIterator();
        int i = 1;
        while(dishes.hasNext()) {
            Dish dish = dishes.next();
            String exist = (this.existDishInMenu(this.menu_backup, dish.getName()))?" (THIS DISH IS ALREADY ADDED IN THIS MENU)":"";
            System.out.println(i++ + "-" + dish.getName() + exist);
        }
    }

    public void listMenuDishes() {
        Iterator<Dish> dishes = this.menu_backup.getDishes().listIterator();
        int i = 1;
        while(dishes.hasNext()) {
            System.out.println(i++ + "-" + dishes.next().getName());
        }
    }

    private boolean existDishInMenu(Menu menu_backup, String dish_name) {
        boolean res = false;
        Iterator<Dish> iterator = menu_backup.getDishes().listIterator();
        while (iterator.hasNext() && !res) {
            if (iterator.next().getName().equals(dish_name)) res = true;
        }
        return res;
    }

    public boolean addDishInMenu(int dish_index) {
        Dish dish = this.usuari.getDishes().get(dish_index);
        if(!existDishInMenu(menu_backup, dish.getName())) {
            menu_backup.getDishes().add(dish);
            return true;
        }
        else return false;
    }
    
    public boolean canAddInMenu(int dish_index) {
        return this.countMenu(this.usuari.getDishes().get(dish_index).getType()) < MAX_TYPE_IN_MENU;
    }

    public boolean deleteDishInMenu(int dish_index) {
        menu_backup.getDishes().remove(dish_index);
        return true;
    }

    public boolean deleteMenu(int menu_index) {
        if (!this.usuari.getMenus().get(menu_index).isAvailable()) {
            this.usuari.getMenus().remove(menu_index);
            return true;
        }
        return false;
    }
    
    public void sortDishes(ArrayList<Dish> dishes) {
        Collections.sort(dishes, new Comparator(){
 
            public int compare(Object o1, Object o2) {
                Dish d1 = (Dish) o1;
                Dish d2 = (Dish) o2;
                if(d1.getType().equals("starter")){
                    return (d2.getType().equals("starter"))?0:-1;
                }
                
                else if(d1.getType().equals("second")){
                    if (d2.getType().equals("starter")) return 1;
                    return (d2.getType().equals("second"))?0:-1;
                }
                
                else if(d1.getType().equals("dessert")){
                    if (d2.getType().equals("drink")) return -1;
                    return (d2.getType().equals("dessert"))?0:1;
                }
                
                else return (d2.getType().equals("drink"))?0:1;
            }
 
        });
    }

    private void showMenu(Menu menu) {
        System.out.println("\nName: " + menu.getName());
        System.out.println("Price: " + ((Float)menu.getPrice()).toString());
        String available = (menu.isAvailable())?"Yes":"No";
        System.out.println("Enabled: " + available);
        System.out.println("Dishes:");
        this.listDishes(menu.getDishes());
    }

    public void getReservations() throws AccessException, NotBoundException, MalformedURLException, IOException {
        this.reservations = this.data.getReservations(this.usuari.getName());
        this.sortReservations();
    }

    public void listPendentReservations() {
        Iterator<Reservation> iterator = this.reservations.listIterator();
        Reservation reservation;
        while(iterator.hasNext()) {
            reservation = iterator.next();
            if(reservation.isPendent()) {
                System.out.println(reservation.getId() + "- Date:" + formatDate.format(reservation.getDate().getTime())
                        + " Hour:" + formatHour.format(reservation.getDate().getTime()) + " Number: " + reservation.getUsuaris().size());
            }
        }
    }

    public boolean isValidReservationId(int id) {
        Iterator<Reservation> iterator = this.reservations.listIterator();
        while(iterator.hasNext()) {
            if(iterator.next().getId() == id) return true;
        }
        return false;
    }

    public void showReservation(int id) {
        Reservation res = this.getReservation(id);
        System.out.println("ID:     " + res.getId());
        System.out.println("DATE:   " + this.formatDate.format(res.getDate().getTime()));
        System.out.println("HOUR:   " + this.formatHour.format(res.getDate().getTime()));
        System.out.println("GUESTS: " + res.getUsuaris().size());
        System.out.println("MENUS:");
        this.showComands(res.getUsuaris());
    }

    private Reservation getReservation(int id) {
        Iterator<Reservation> iterator = this.reservations.listIterator();
        Reservation res;
        while(iterator.hasNext()) {
            res = iterator.next();
            if(res.getId() == id) return res;
        }
        return null;
    }

    private void showComands(ArrayList<Comand> comands) {
        Iterator<Comand> it = comands.listIterator();
        int i = 1;
        while (it.hasNext()) {
            System.out.println("\nGuest " + i++ + ":");
            this.showComand(it.next());
        }
    }

    private void showComand(Comand comand) {
        System.out.println("guest:  " + comand.getUsername());
        System.out.println("starter:" + comand.getStarter());
        System.out.println("second: " + comand.getSecond());
        System.out.println("drink: " + comand.getDrink());
    }

    public void confirmReservation(int id) throws RemoteException, AccessException, MalformedURLException, NotBoundException, IOException {
        Reservation res = this.getReservation(id);
        res.setPendent(false);
        data.confirmReservation(id, res.getUsuaris());
    }

    public void listNonPendentReservations() {
        Iterator<Reservation> iterator = this.reservations.listIterator();
        Reservation reservation;
        while(iterator.hasNext()) {
            reservation = iterator.next();
            if(!reservation.isPendent()) {
                System.out.println(reservation.getId() + "- Date:" + formatDate.format(reservation.getDate().getTime())
                        + " Hour:" + formatHour.format(reservation.getDate().getTime()) + " Number: " + reservation.getUsuaris().size());
            }
        }
    }
    
    public void sortReservations() {
        Collections.sort(this.reservations, new Comparator(){
 
            public int compare(Object o1, Object o2) {
                Reservation r1 = (Reservation) o1;
                Reservation r2 = (Reservation) o2;
                if(r1.getDate().before(o2)) return 1;
                else if(r1.getDate().after(o2)) return -1;  
                else return 0;
            }
 
        });
    }

    private int countMenu(String type) {
        int count = 0;
        Iterator<Dish> iterator = menu_backup.getDishes().listIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getType().equals(type)) count++;
        }
        return count;
    }

    public void copyRest() {
        this.rest_backup = new Restaurant();
        rest_backup.setAll(this.usuari);
    }

    public void updateRestName(String new_restname) {
        this.rest_backup.setName(new_restname);
    }

    public void updateRestMail(String mail) {
        this.rest_backup.setMail(mail);
    }

    public void updateRestPhone(String phone) {
        this.rest_backup.setPhone(phone);
    }

    public void updateRestAddress(String address) {
        this.rest_backup.setAddress(address);
    }

    public void updateRestDescription(String description) {
        this.rest_backup.setDescription(description);
    }

    public void updateRestCategory(String category) {
        this.rest_backup.setCategory(category);
    }

    public void updateRestPassword(String pass) {
        this.rest_backup.setPassword(pass);
    }
    
    public void updateRestImg(String img) {
        this.rest_backup.setImg(img);
    }

    public void editingMenu(int editing_menu) {
        System.out.println("\nEditing: " + this.usuari.getMenus().get(editing_menu).getName());
    }

    
}


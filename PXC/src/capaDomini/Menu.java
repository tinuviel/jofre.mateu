/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jofre
 */
public class Menu implements Serializable{
    private String name;
    private float price;
    private ArrayList<Dish> dishes;
    private boolean available;

    public Menu(String menu_name, float menu_price, boolean menu_available) {
        this.name = menu_name;
        this.price = menu_price;
        this.available = menu_available;
        this.dishes = new ArrayList<Dish>();
    }

    public Menu(Menu menu) {
        this.name = menu.name;
        this.price = menu.price;
        this.available = menu.available;
        this.dishes = menu.dishes;
    }

    public boolean isAvailable() {
        return available;
    }

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    void setAll(Menu menu_backup) {
        this.name = menu_backup.name;
        this.price = menu_backup.price;
        this.available = menu_backup.available;
        this.dishes = menu_backup.dishes;
    }
}

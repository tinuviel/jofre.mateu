/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;

/**
 *
 * @author Jofre
 */
public class Client implements Serializable {
    private String username;
    private String password;
    
    public Client() {
    }
    
    public Client(String user, String pass) {
        this.username = user;
        this.password = pass;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getPassword() {
        return this.password;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;

/**
 *
 * @author Jofre
 */
public class Dish implements Serializable {
    private String name;
    private String type;
    private float price;
    private String description;
    //private foto??
    
    public Dish(String name, String type, float price, String description) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;        
    }

    public Dish(Dish dish) {
        this.name = dish.name;
        this.type = dish.type;
        this.price = dish.price;
        this.description = dish.description;        
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }
    
    public String getType() {
        return this.type;
    }
    
    public float getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    void setAll(Dish dish_backup) {
        this.name = dish_backup.name;
        this.type = dish_backup.type;
        this.price = dish_backup.price;
        this.description = dish_backup.description;
    }
}

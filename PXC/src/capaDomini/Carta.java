/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jofre
 */
public class Carta implements Serializable{
    private String name;
    private ArrayList<Dish> dishes;
    private boolean available;
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
    
    public String getName() {
        return this.name;
    } 
    
    public ArrayList<Dish> getDishes() {
        return this.dishes;
    }

    public boolean isAvailable() {
        return available;
    }
}

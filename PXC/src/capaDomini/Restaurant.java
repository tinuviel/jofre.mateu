/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author Jofre
 */
public class Restaurant implements Serializable {
    private String name;
    private String phone;
    private String address;
    private String description;
    private String mail;
    private String category;
    private String password;
    private String img;
    private ArrayList<Dish> dishes;
    private ArrayList<Menu> menus;
    private ArrayList<Carta> cartes;
    
    public Restaurant() {
        this.dishes = new ArrayList<Dish>();
        this.menus = new ArrayList<Menu>();
        this.cartes = new ArrayList<Carta>();
    }
    
    public Restaurant(String name, String mail, String phone, String address, String description, String password, String category, String img) {
        this.name = name;
        this.mail = mail;
        this.phone = phone;
        this.address = address;
        this.description = description;
        this.password = password;
        this.category = category;
        this.img = img;
        this.dishes = new ArrayList<Dish>();
        this.menus = new ArrayList<Menu>();
        this.cartes = new ArrayList<Carta>();
    }
    
    void setAll(Restaurant usuari) {
        this.name = usuari.name;
        this.mail = usuari.mail;
        this.phone = usuari.phone;
        this.address = usuari.address;
        this.description = usuari.description;
        this.password = usuari.password;
        this.category = usuari.category;
        this.dishes = usuari.dishes;
        this.menus = usuari.menus;
        this.cartes = usuari.cartes;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    
    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }
    
    public void setMenus(ArrayList<Menu> menus) {
        this.menus = menus;
    }
    
    public void setCartes(ArrayList<Carta> cartes) {
        this.cartes = cartes;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return this.name;
    }
    
    public String getMail() {
        return this.mail;
    }
    
    public String getPhone() {
        return this.phone;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getCategory() {
        return this.category;
    }
    
    public ArrayList<Dish> getDishes() {
        return this.dishes;
    }
    
    public ArrayList<Menu> getMenus() {
        return this.menus;
    }
    
    public ArrayList<Carta> getCartes() {
        return this.cartes;
    }
    
    public String getPassword() {
        return this.password;
    }

    public String getImg() {
        return img;
    }

    void addDish(Dish dish) {
        this.dishes.add(dish);
    }
    
    void addMenu(Menu menu) {
        this.menus.add(menu);
    }
    
    public ArrayList<String> getDishesName() {
        this.sortDishes();
        ArrayList<String> res = new ArrayList<String>();
        Iterator<Dish> it = this.dishes.listIterator();
        while (it.hasNext())
            res.add(it.next().getName());
        return res;
    }

    public Dish getDishByName(String dish_name){
        Dish dish;
        Iterator<Dish> it = this.dishes.listIterator();
        while (it.hasNext()) {
            dish = it.next();
            if (dish.getName().equals(dish_name)) return dish;
        }
        return null;
    }
    
    public void sortDishes() {
        Collections.sort(dishes, new Comparator(){
 
            public int compare(Object o1, Object o2) {
                Dish d1 = (Dish) o1;
                Dish d2 = (Dish) o2;
                if(d1.getType().equals("starter")){
                    return (d2.getType().equals("starter"))?0:-1;
                }
                
                else if(d1.getType().equals("second")){
                    if (d2.getType().equals("starter")) return 1;
                    return (d2.getType().equals("second"))?0:-1;
                }
                
                else if(d1.getType().equals("dessert")){
                    if (d2.getType().equals("drink")) return -1;
                    return (d2.getType().equals("dessert"))?0:1;
                }
                
                else return (d2.getType().equals("drink"))?0:1;
            }
 
        });
    }
}

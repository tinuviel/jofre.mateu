/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDomini;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author Jofre
 */
public final class Main {
    
    public static void main(String[] args) throws IOException, RemoteException, NotBoundException {
        DomainControler domain = DomainControler.getInstancia();
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        
        boolean exit = false;
        boolean account_changed = false;
        boolean dish_changed = false;
        boolean menu_changed = false;
        boolean carte_changed = false;
        int option = 0;
                                
        while (!exit) {
            
            switch (option) {
                
                /***************************************************
                 *                  FIRST MENU                     *
                 ***************************************************/
                case 0: 
                    System.out.println("#####################  WELCOME MENU  ########################");
                    System.out.println("\n1-Login");
                    System.out.println("2-Register");
                    System.out.println("3-Exit");
                    System.out.println("\nWhat do you want to do?");
                    option = Integer.parseInt(reader.readLine());
                    if (option > 3 || option < 1) {
                        System.out.println("WRONG OPTION");
                        option = 0;
                    }
                    break;
                    
                    
                /***************************************************
                 *                      LOGIN                      *
                 ***************************************************/    
                case 1:
                    System.out.println("\nRestaurant name:");
                    String restname = reader.readLine();
                    System.out.println("Password");
                    String password = reader.readLine();

                    if (domain.login(restname, password)) {
                        option = 4; // To main Menu
                    }
                    else option = 0; // To first Menú
                    break;                  
                    
                    
                /***************************************************
                 *                   REGISTER                      *
                 ***************************************************/    
                case 2:
                    System.out.println("\nRestaurant name:");
                    restname = reader.readLine();

                    if (!domain.existRestname(restname)) {
                        System.out.println("Password:");
                        password = reader.readLine();
                        System.out.println("Re-type Password:");
                        String confirm = reader.readLine();

                        if (password.equals(confirm)) {

                            System.out.println("E-mail:");
                            String mail = reader.readLine();

                            System.out.println("Telephone:");
                            String phone = reader.readLine();

                            System.out.println("Address:");
                            String address = reader.readLine();

                            System.out.println("Description:");
                            String description = reader.readLine();
                            
                            System.out.println("Category:");
                            String category = reader.readLine();
                            
                            domain.registre(restname, password, mail, phone, address, description, category, category);
                            
                            option = 4; // To main Menu
                        }
                        else System.out.println("Passwords don't match!");
                    }
                    else System.out.println("This restaurant already exists!");
                    break;
                
                    
                /***************************************************
                 *                      EXIT                       *
                 ***************************************************/    
                case 3:
                    exit = true;
                    break;
                 
                
                /***************************************************
                 *                   MAIN MENU                     *
                 ***************************************************/    
                case 4:
                    System.out.println("\n#####################  MAIN MENU  ########################");
                    System.out.println("\n5-Your Acount");            
                    System.out.println("6-Manage Restaurant");
                    System.out.println("7-Manage Reservations");
                    System.out.println("8-Logout");
                    System.out.println("\nWhat do you want to do?");
                    option = Integer.parseInt(reader.readLine());
                    if (option < 5 || option > 8) {
                        System.out.println("WRONG OPTION");
                        option = 4;
                    }
                    break;
                        
                    
                /***************************************************
                 *                 YOUR ACCOUNT                    *
                 ***************************************************/    
                case 5:
                    int suboption;
                    domain.copyRest();
                    do {
                        System.out.println("\n#####################  YOUR ACCOUNT  ########################");
                        System.out.println("\n1-Change restaurant name");
                        System.out.println("2-Change restaurant e-mail");
                        System.out.println("3-Change restaurant telephone");
                        System.out.println("4-Change restaurant address");
                        System.out.println("5-Change restaurant description");
                        System.out.println("6-Change restaurant category");
                        System.out.println("7-Change restaurant password");
                        System.out.println("8-Back to main Menu");
                        System.out.println("\nWhat do you want to do?");

                        suboption = Integer.parseInt(reader.readLine());

                        switch (suboption) {
                            case 1:
                                String new_restname;
                                do {
                                    System.out.println("\n*** CHANGE NAME ***");
                                    System.out.println("Insert new restaurnat name");
                                    new_restname = reader.readLine();
                                } while (domain.existRestname(new_restname));
                                domain.updateRestName(new_restname);
                                account_changed = true;
                                break;

                            case 2:
                                System.out.println("\n*** CHANGE E-MAIL ***");
                                System.out.println("Insert new restaurnat e-mail");
                                domain.updateRestMail(reader.readLine());
                                account_changed = true;
                                break;

                            case 3:
                                System.out.println("\n*** CHANGE TELEPHONE ***");
                                System.out.println("Insert new restaurnat telephone");
                                domain.updateRestPhone(reader.readLine());
                                account_changed = true;
                                break;

                            case 4:
                                System.out.println("\n*** CHANGE ADDRESS ***");
                                System.out.println("Insert new restaurnat address");
                                domain.updateRestAddress(reader.readLine());
                                account_changed = true;
                                break;

                            case 5:
                                System.out.println("\n*** CHANGE DESCRIPTION ***");
                                System.out.println("Insert new restaurnat description");
                                domain.updateRestDescription(reader.readLine());
                                account_changed = true;
                                break;

                            case 6:
                                System.out.println("\n*** CHANGE CATEGORY ***");
                                System.out.println("Insert new restaurnat category");
                                domain.updateRestCategory(reader.readLine());
                                account_changed = true;
                                break;

                            case 7:
                                System.out.println("\n*** CHANGE PASSWORD ***");
                                boolean b = false; // Mentre b sigui fals, no sortim de l'opció change password
                                while (!b) {
                                    System.out.println("Insert the previous password");
                                    domain.getUser().getPassword().equals(reader.readLine());
                                    System.out.println("Insert new restaurnat password");
                                    String pass = reader.readLine();
                                    System.out.println("Confirm new restaurnat password");
                                    String conf = reader.readLine();
                                    if(pass.equals(conf)) {
                                        domain.updateRestPassword(pass);
                                        account_changed = true;
                                        b = true;
                                    }
                                    else {
                                        System.out.println("\nPasswords don't match");
                                        System.out.println("1-Retry");
                                        System.out.println("2-Back");
                                        if (!reader.readLine().equals("1")) b = true;
                                    }
                                }
                                break;

                            case 8:
                                if (account_changed) {
                                    int saveoption = 0;
                                    while(saveoption != 1 && saveoption != 2) {
                                        System.out.println("\nYou've changed some details about the restaurant:");
                                        System.out.println("\n1-Save changes");
                                        System.out.println("2-Ignore changes");

                                        saveoption = Integer.parseInt(reader.readLine());
                                        if (saveoption == 1) {
                                            domain.updateRestaurant();
                                            System.out.println("Saved!");
                                        }
                                        else if (saveoption == 2) System.out.println("Ignored!");
                                        else System.out.println("WRONG OPTION");
                                    }
                                }
                                option = 4; // to main menu
                                break;
                            default:
                                System.out.println("WRONG OPTION");
                        }             
                    } while (suboption != 8);
                    break;
                        
                    
                /***************************************************
                 *               MANAGE RESTAURANT                 *
                 ***************************************************/    
                case 6:                   
                    do {
                        System.out.println("\n#####################  MANAGE RESTAURANT  ########################");
                        System.out.println("\n1-Dishes");
                        System.out.println("2-Menu");
                        System.out.println("3-Back");
                        suboption = Integer.parseInt(reader.readLine());
                        switch (suboption) {
                            case 1:          
                                int dishoption = 0;
                                while(dishoption != 5) {
                                    System.out.println("\n*** DISHES ***");
                                    System.out.println("1-Consult");
                                    System.out.println("2-Add");
                                    System.out.println("3-Edit");
                                    System.out.println("4-Delete");
                                    System.out.println("5-Back");
                                    dishoption = Integer.parseInt(reader.readLine());
                                    switch (dishoption) {
                                        case 1:
                                            System.out.println("\n--- list of dishes ---");
                                            domain.listDishes();
                                            int list_option;
                                            do {
                                                System.out.println("\n(1-X)-Show dish details");
                                                System.out.println("0-Back");
                                                list_option = Integer.parseInt(reader.readLine());
                                                int dish_index = list_option - 1;
                                                if (list_option != 0 ) {
                                                    if(domain.isValidDishIndex(dish_index)) domain.showDish(dish_index);
                                                    else System.out.println("WRONG DISH INDEX");
                                                }
                                            } while (list_option != 0);
                                            break;
                                            
                                        case 2:
                                            String dishName;
                                            do {
                                                System.out.println("\n--- add a dish ---");
                                                System.out.println("Dish name:");
                                                dishName = reader.readLine();
                                            } while (domain.existDish(dishName));
                                            
                                            System.out.println("Dish type:");
                                            System.out.println("1-Starter");
                                            System.out.println("2-Second");
                                            System.out.println("3-Dessert");
                                            System.out.println("4-Drink");
                                            int type_option = Integer.parseInt(reader.readLine());
                                            String dishType = "";
                                            while(type_option < 1 || type_option > 4) {
                                                System.out.println("INVALID TYPE");
                                                
                                                System.out.println("Dish type:");
                                                System.out.println("1-Starter");
                                                System.out.println("2-Second");
                                                System.out.println("3-Dessert");
                                                System.out.println("4-Drink");
                                                type_option = Integer.parseInt(reader.readLine());
                                            }
                                            switch (type_option) {
                                                case 1:
                                                    dishType = "starter";
                                                    break;
                                                case 2:
                                                    dishType = "second";
                                                    break;
                                                case 3:
                                                    dishType = "dessert";
                                                    break;
                                                case 4:
                                                    dishType = "drink";
                                                    break;
                                            }
                                            System.out.println("Dish price:");
                                            float dishPrice = Float.parseFloat(reader.readLine());
                                            System.out.println("Dish description:");
                                            String dishDescription = reader.readLine();

                                            domain.addDish(dishName, dishType, dishPrice, dishDescription);                            
                                            break;

                                        case 3:                                            
                                            System.out.println("\n--- edit a dish ---");
                                            System.out.println("Press return to list the dishes. Then, introduce the number of the dish that you want to edit");
                                            reader.readLine();
                                            domain.listDishes();
                                            System.out.println("\nIntroduce de number of the dish that you want to edit");
                                            int editing_dish = Integer.parseInt(reader.readLine());
                                            editing_dish--;
                                            while (!domain.isValidDishIndex(editing_dish) || !domain.canModifyDish(editing_dish)) {
                                                System.out.println((!domain.isValidDishIndex(editing_dish))?"\nWRONG DISH INDEX":"\nTHIS DISH CAN'T BE EDITED BECAUSE IS IN A AVAILABLE MENÚ");
                                                System.out.println("\nIntroduce de number of the dish that you want to edit");
                                                editing_dish = Integer.parseInt(reader.readLine());
                                                editing_dish--;
                                            }
                                            domain.copyDish(editing_dish);
                                            int edit_option;
                                            do {
                                                domain.editingDish(editing_dish);
                                                System.out.println("1-Change name");
                                                System.out.println("2-Change type");
                                                System.out.println("3-Change description");
                                                System.out.println("4-Change price");
                                                System.out.println("5-Back");
                                                edit_option = Integer.parseInt(reader.readLine());

                                                switch (edit_option) {
                                                    case 1:
                                                        System.out.println("\n··· change name ···");

                                                        System.out.println("Insert new dish name:");
                                                        dishName = reader.readLine();
                                                        while (domain.existDish(dishName)) {
                                                            System.out.println("Ther is another dish with this name!");
                                                            System.out.println("Insert the new dish name:");
                                                            dishName = reader.readLine();
                                                        }

                                                        domain.updateDishName(dishName);
                                                        dish_changed = true;
                                                        break;
                                                    case 2:
                                                        System.out.println("··· change type ···");
                                                        System.out.println("Insert the new dish type:");
                                                        System.out.println("1-Starter");
                                                        System.out.println("2-Second");
                                                        System.out.println("3-Dessert");
                                                        System.out.println("4-Drink");
                                                        type_option = Integer.parseInt(reader.readLine());
                                                        String dish_type = "";
                                                        while(type_option < 1 || type_option > 4) {
                                                            System.out.println("INVALID TYPE");

                                                            System.out.println("Insert the new dish type:");
                                                            System.out.println("1-Starter");
                                                            System.out.println("2-Second");
                                                            System.out.println("3-Dessert");
                                                            System.out.println("4-Drink");
                                                            type_option = Integer.parseInt(reader.readLine());
                                                        }
                                                        switch (type_option) {
                                                            case 1:
                                                                dish_type = "starter";
                                                                break;
                                                            case 2:
                                                                dish_type = "second";
                                                                break;
                                                            case 3:
                                                                dish_type = "dessert";
                                                                break;
                                                            case 4:
                                                                dish_type = "drink";
                                                                break;
                                                        }
                                                        domain.updateDishType(dish_type);
                                                        dish_changed = true;
                                                        break;
                                                    case 3:
                                                        System.out.println("··· change description ···");
                                                        System.out.println("Insert the new dish description:");
                                                        String dish_description = reader.readLine();
                                                        domain.updateDishDescription(dish_description);
                                                        dish_changed = true;
                                                        break;
                                                    case 4:
                                                        System.out.println("··· change price ···");
                                                        System.out.println("Insert the new dish price:");
                                                        float dish_price = Float.parseFloat(reader.readLine());
                                                        domain.updateDishPrice(dish_price);
                                                        dish_changed = true;
                                                        break;
                                                    case 5: // Back
                                                        if (dish_changed) {
                                                            dish_changed = false;
                                                            int saveoption = 0;
                                                            while(saveoption != 1 && saveoption != 2) {
                                                                System.out.println("\nYou've changed some details about the dish:");
                                                                /* TODO
                                                                 * mostrar les implicacions del canvi, quins menus afecta.
                                                                 */
                                                                System.out.println("\n1-Save changes");
                                                                System.out.println("2-Ignore changes");

                                                                saveoption = Integer.parseInt(reader.readLine());
                                                                if (saveoption == 1) {
                                                                    domain.updateDish(editing_dish);
                                                                    System.out.println("Saved!");
                                                                }
                                                                else if (saveoption == 2) System.out.println("Ignored!");
                                                                else System.out.println("WRONG OPTION");
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        System.out.println("WRONG OPTION");
                                                }
                                            }
                                            while(edit_option != 5);
                                            break;

                                        case 4:
                                            System.out.println("\n--- delete a dish ---");
                                            System.out.println("Press return to list the dishes. Then, introduce the number of the dish that you want to delete");
                                            reader.readLine();
                                            domain.listDishes();
                                            System.out.println("Introduce de number of the dish that you want to edit");
                                            int deleting_dish = Integer.parseInt(reader.readLine());
                                            deleting_dish--;
                                            while (!domain.isValidDishIndex(deleting_dish) || !domain.canModifyDish(deleting_dish)) {
                                                System.out.println((domain.isValidDishIndex(deleting_dish))?"\nWRONG DISH INDEX":"\nTHIS DISH CAN'T BE EDITED BECAUSE IS IN A AVAILABLE MENÚ");
                                                System.out.println("\nIntroduce de number of the dish that you want to delete");
                                                editing_dish = Integer.parseInt(reader.readLine());
                                                editing_dish--;
                                            }
                                            int delete_option = 0;
                                            while(delete_option != 1 && delete_option != 2) {
                                                System.out.println("\nAre you sure?");
                                                /* TODO
                                                 * mostrar les implicacions del canvi, quins menus afecta.
                                                 */
                                                System.out.println("\n1-Yes");
                                                System.out.println("2-No");

                                                delete_option = Integer.parseInt(reader.readLine());
                                                if (delete_option == 1) {
                                                    domain.deleteDish(deleting_dish);
                                                    System.out.println("Deleted!");
                                                }
                                                else if (delete_option == 2) System.out.println("Ignored!");
                                                else System.out.println("WRONG OPTION");
                                            }
                                            
                                            dish_changed = true;
                                            break;

                                        case 5:
                                            option = 6; //to manage restaurant
                                            break;

                                        default:
                                            System.out.println("WRONG OPTION");
                                    }
                                }
                                break;
                                
                            case 2:
                                int menuoption = 0;
                                while (menuoption != 5) {
                                    System.out.println("\n*** MENU ***");
                                    System.out.println("1-Consult");
                                    System.out.println("2-Add");
                                    System.out.println("3-Edit");
                                    System.out.println("4-Delete");
                                    System.out.println("5-Back");
                                    menuoption = Integer.parseInt(reader.readLine());
                                    switch (menuoption) {
                                        case 1:
                                            System.out.println("\n--- consult existing menus ---");
                                            domain.listMenus();
                                            int list_option;
                                            do {
                                                System.out.println("\n(1-X)-Show menu details");
                                                System.out.println("0-Back");
                                                list_option = Integer.parseInt(reader.readLine());
                                                int menu_index = list_option - 1;
                                                if (list_option != 0 ) {
                                                    if(domain.isValidMenuIndex(menu_index)) domain.showMenu(menu_index);
                                                    else System.out.println("WRONG MENU INDEX");
                                                }
                                            } while (list_option != 0);
                                            break;
                                        case 2:
                                            String menu_name;
                                            do {
                                                System.out.println("\n--- add a menu ---");
                                                System.out.println("Menu name:");
                                                menu_name = reader.readLine();
                                            } while (domain.existMenu(menu_name));
                                            
                                            System.out.println("Menu price:");
                                            float menu_price = Float.parseFloat(reader.readLine());
                                            
                                            boolean menu_available;
                                            int available_option;
                                            do {
                                                System.out.println("Menu availability:");
                                                System.out.println("1-Yes");
                                                System.out.println("2-No");
                                                available_option = Integer.parseInt(reader.readLine());
                                                if(available_option != 1 && available_option !=2) System.out.println("WRONG OPTION");
                                            } while(available_option != 1 && available_option !=2);
                                            menu_available = (available_option == 1)?true:false;

                                            domain.addMenu(menu_name, menu_price, menu_available);                            
                                            break;

                                        case 3:
                                            System.out.println("\n--- edit a menu ---");
                                            System.out.println("Press return to list the menus. Then, introduce the number of the menu that you want to edit");
                                            reader.readLine();
                                            domain.listMenus();
                                            System.out.println("\nIntroduce de number of the menu that you want to edit");
                                            int editing_menu = Integer.parseInt(reader.readLine());
                                            editing_menu--;
                                            while (!domain.isValidMenuIndex(editing_menu) || !domain.canModifyMenu(editing_menu)) {
                                                System.out.println((domain.isValidMenuIndex(editing_menu))?"\nWRONG DISH INDEX":"\nTHIS MENU CAN'T BE EDITED BECAUSE IT'S AVAILABLE");
                                                System.out.println("\nIntroduce de number of the menu that you want to edit");
                                                editing_menu = Integer.parseInt(reader.readLine());
                                                editing_menu--;
                                            }
                                            domain.copyMenu(editing_menu);
                                            int edit_option;
                                            do {
                                                domain.editingMenu(editing_menu);
                                                System.out.println("1-Change name");
                                                System.out.println("2-Change price");
                                                System.out.println("3-Change availability");
                                                System.out.println("4-Manage available dishes");                                                
                                                System.out.println("5-Back");
                                                edit_option = Integer.parseInt(reader.readLine());

                                                switch (edit_option) {
                                                    case 1:
                                                        System.out.println("\n··· change name ···");

                                                        System.out.println("Insert new menu name:");
                                                        menu_name = reader.readLine();
                                                        while (domain.existDish(menu_name)) {
                                                            System.out.println("Ther is another menu with this name!");
                                                            System.out.println("Insert the new menu name:");
                                                            menu_name = reader.readLine();
                                                        }

                                                        domain.updateMenuName(menu_name);
                                                        menu_changed = true;
                                                        break;
                                                        
                                                    case 2:
                                                        System.out.println("··· change price ···");
                                                        System.out.println("Insert the new menu price:");
                                                        menu_price = Float.parseFloat(reader.readLine());
                                                        domain.updateMenuPrice(menu_price);
                                                        menu_changed = true;
                                                        break;
                                                        
                                                    case 3:
                                                        System.out.println("··· change availability ···");                                                        
                                                        do {
                                                            System.out.println("Set the new menu availability:");
                                                            System.out.println("1-Yes");
                                                            System.out.println("2-No");
                                                            available_option = Integer.parseInt(reader.readLine());
                                                            if(available_option != 1 && available_option !=2) System.out.println("WRONG OPTION");
                                                        } while(available_option != 1 && available_option !=2);
                                                        menu_available = (available_option == 1)?true:false;
                                                        domain.updateMenuAvailable(menu_available);
                                                        menu_changed = true;
                                                        break;
                                                        
                                                    case 4:
                                                        System.out.println("··· Manage available dishes ···");
                                                        int dish_option;
                                                        do {
                                                            System.out.println("1-Add dishes");
                                                            System.out.println("2-Delete dishes");
                                                            System.out.println("3-Back");
                                                            dish_option = Integer.parseInt(reader.readLine());
                                                            switch (dish_option) {
                                                                case 1:
                                                                    System.out.println("+++ add dishes +++");
                                                                    System.out.println("Pres return to list all dishes of the restaurant. "
                                                                            + "Then, insert the number of the dish that you want to add");
                                                                    reader.readLine();                                                          
                                                                    domain.listMenuNoDishes();
                                                                    int add_option;
                                                                    do {
                                                                        System.out.println("(1-X) Insert the number of the dish that you want to add");
                                                                        System.out.println("(0) Back");
                                                                        add_option = Integer.parseInt(reader.readLine());
                                                                        int dish_index = add_option - 1;
                                                                        if (add_option != 0 ) {
                                                                            if(domain.isValidDishIndex(dish_index)) {
                                                                                if(domain.canAddInMenu(dish_index))
                                                                                    System.out.println((domain.addDishInMenu(dish_index))?"Correctly added!":"ERROR! This dish is already in the menu");
                                                                                else System.out.println("ERROR! You can't add more dishes of this type");
                                                                            }
                                                                            else System.out.println("WRONG DISH INDEX");
                                                                        }
                                                                    } while (add_option != 0);
                                                                    break;
                                                                case 2:
                                                                    System.out.println("+++ delete dishes +++");
                                                                    System.out.println("Pres return to list all dishes of the menu. "
                                                                            + "Then, insert the number of the dish that you want to delete");
                                                                    reader.readLine();                                                          
                                                                    domain.listMenuDishes();
                                                                    do {
                                                                        System.out.println("(1-X) Insert the number of the dish that you want to delete");
                                                                        System.out.println("(0) Back");
                                                                        add_option = Integer.parseInt(reader.readLine());
                                                                        int dish_index = add_option - 1;
                                                                        if (add_option != 0 ) {
                                                                            if(domain.isValidDishIndex(dish_index))
                                                                                System.out.println((domain.deleteDishInMenu(dish_index))?"Correctly deleted!":"Unespected ERROR!");
                                                                            else System.out.println("WRONG DISH INDEX");
                                                                        }
                                                                    } while (add_option != 0);
                                                                    break;
                                                                case 3:
                                                                    
                                                                    break;
                                                                default:
                                                                    System.out.println("WRONG OPTION");
                                                            }
                                                            
                                                        } while (dish_option != 3);
                                                        menu_changed = true;
                                                        break;
                                                        
                                                    case 5: // Back
                                                        if (menu_changed) {
                                                            menu_changed = false;
                                                            int saveoption = 0;
                                                            while(saveoption != 1 && saveoption != 2) {
                                                                System.out.println("\nYou've changed some details about the menu:");
                                                                System.out.println("\n1-Save changes");
                                                                System.out.println("2-Ignore changes");

                                                                saveoption = Integer.parseInt(reader.readLine());
                                                                if (saveoption == 1) {
                                                                    domain.updateMenu(editing_menu);
                                                                    System.out.println("Saved!");
                                                                }
                                                                else if (saveoption == 2) System.out.println("Ignored!");
                                                                else System.out.println("WRONG OPTION");
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        System.out.println("WRONG OPTION");
                                                }
                                            } while(edit_option != 5);
                                            break;

                                        case 4:
                                            System.out.println("\n--- delete a menu ---");
                                            domain.listMenus();
                                            do {
                                                System.out.println("\n(1-X)- Delete menu");
                                                System.out.println("0-Back");
                                                list_option = Integer.parseInt(reader.readLine());
                                                int menu_index = list_option - 1;
                                                if (list_option != 0 ) {
                                                    if(domain.isValidMenuIndex(menu_index)) {
                                                        int delete_option = 0;
                                                        while(delete_option != 1 && delete_option != 2) {
                                                            System.out.println("\nAre you sure that you want to delete this menu?");
                                                            System.out.println("\n1-Yes");
                                                            System.out.println("2-No");

                                                            delete_option = Integer.parseInt(reader.readLine());
                                                            if (delete_option == 1) {
                                                                System.out.println((domain.deleteMenu(menu_index))?"Deleted!":"This menu can't be deleted, it's available");
                                                            }
                                                            else if (delete_option == 2) System.out.println("Ignored!");
                                                            else System.out.println("WRONG OPTION");
                                                        }
                                                        domain.showMenu(menu_index);
                                                    }
                                                    else System.out.println("WRONG MENU INDEX");
                                                }
                                            } while (list_option != 0);
                                            break;

                                        case 5: // Back
                                            option = 6; //to manage restaurant
                                            break;

                                        default:
                                            System.out.println("WRONG OPTION");
                                    }
                                }
                                break;

                            case 3:
                                System.out.println("suboption = 4");
                                option = 4; // to main menu
                                break;
                                
                            default:
                                System.out.println("WRONG OPTION");
                        }
                    } while(suboption != 3);
                    break;
                        
                    
                /***************************************************
                 *               MANAGE RESERVATION                *
                 ***************************************************/    
                case 7:
                    domain.getReservations();
                    do {
                        System.out.println("\n#####################  MANAGE RESERVATIONS  ########################");
                        System.out.println("1-Pendent reservations");
                        System.out.println("2-Confirmed reservations");
                        System.out.println("3-Back");
                        System.out.println("\nWhat do you want to do?");

                        suboption = Integer.parseInt(reader.readLine());

                        switch (suboption) {
                            case 1: // llista només les reserves pendents per dates superiors a avui
                                System.out.println("\n--- list of pendent reservations ---");
                                domain.listPendentReservations();
                                int id;
                                do {
                                    System.out.println("\n(id)-Show reservation details");
                                    System.out.println("0-Back");
                                    id = Integer.parseInt(reader.readLine());
                                    if (id != 0 ) {
                                        if(domain.isValidReservationId(id)) {
                                            domain.showReservation(id);
                                            int save_option;
                                            do {
                                                System.out.println("Do you want to confirm this reservation?");
                                                System.out.println("1-Yes");
                                                System.out.println("2-Not yet");
                                                save_option = Integer.parseInt(reader.readLine());
                                                switch (save_option) {
                                                    case 1:
                                                        domain.confirmReservation(id);
                                                        break;
                                                    default:
                                                        System.out.println("WRONG OPTION");
                                                }
                                            } while (save_option != 1 && save_option != 2);
                                        }
                                        else System.out.println("WRONG RESERVATION ID");
                                    }
                                } while (id != 0);
                                break;

                            case 2: // llista només les reserves confirmades per dates superiors a avui
                                System.out.println("\n--- list of confirmed reservations ---");
                                domain.listNonPendentReservations();
                                do {
                                    System.out.println("\n(id)-Show reservation details");
                                    System.out.println("0-Back");
                                    id = Integer.parseInt(reader.readLine());
                                    if (id != 0 ) {
                                        if(domain.isValidReservationId(id)) domain.showReservation(id);
                                        else System.out.println("WRONG RESERVATION ID");
                                    }
                                } while (id != 0);
                                break;

                            case 3:
                                option = 4; // to main menu;
                                break;
                            default:
                                System.out.println("WRONG OPTION");
                        } 
                    } while (suboption != 3);
                    break;
                    
                    
                /***************************************************
                 *                      LOGOUT                     *
                 ***************************************************/    
                case 8:
                    domain.logout();
                    option = 0; // to first menu
                    break;

            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDades;
import capaDomini.*;
import java.util.ArrayList;

/**
 *
 * @author Jofre
 */
public interface TestRemote extends java.rmi.Remote{
    
    /* correctLogin(String rest_name, String password): 
     * retorna cert si existeix el restaurant restname amb contrasenya password
     * retorna fals altrament */
    public boolean correctLogin(String rest_name, String password) throws java.rmi.RemoteException;
    
    /* login(String rest_name, String password): 
     * retorna arraylist amb tots els paràmetres del restaurant si existeix el restaurant restname amb contrasenya password
     * retorna null altrament */
    public Restaurant login(String rest_name, String password) throws java.rmi.RemoteException;

    /* existRestaurant(String rest_name): 
     * retorna cert si existeix el restaurant restname
     * retorna fals altrament */
    public boolean existRestaurant(String rest_name) throws java.rmi.RemoteException;

    /* insertRestaurant(String rest_name, String password, String mail, String phone, String address, String description, String category): 
     * insereix el restaurant amb tots els seus paràmetres */
    public void insertRestaurant(String rest_name, String password, String mail, String phone, String address, String description, String category, String img) throws java.rmi.RemoteException;

    /* updateRestaurant(String name, String password, String mail, String phone, String address, String description): 
     * actualitza el restaurant amb tots els seus paràmetres */
    public void updateRestaurant(String old_name, String name, String password, String address, String mail, String phone, String description, String category, String img) throws java.rmi.RemoteException;
    
    /* updateDish(String rest_name, String old_dish_name, Dish dish)
     * actualitza tots els paràmetres de dish per al restaurant rest_name */
    public void updateDish(String rest_name, String old_dish_name, Dish dish) throws java.rmi.RemoteException;
    
    /* deleteDish(String rest_name, String dish_name)
     * esborra el plat dish_name del restaurant rest_name */
    public void deleteDish(String rest_name, String dish_name) throws java.rmi.RemoteException;

    /* addDish(String rest_name, Dish dish)
     * afegeix el plat dish_name del restaurant rest_name */
    public void addDish(String rest_name, Dish dish) throws java.rmi.RemoteException;
    
    /* addMenu(String rest_name, Menu menu)
     * afegeix el menu al restaurant rest_name */
    public void addMenu(String rest_name, Menu menu) throws java.rmi.RemoteException;

    /* updateMenu(String rest_name, String old_menu_name, Menu menu_backup)
     * actualitza tots els paràmetres de menu per al restaurant rest_name */
    public void updateMenu(String rest_name, String old_menu_name, Menu menu_backup) throws java.rmi.RemoteException;

    /* getReservations(String rest_name)
     * retorna un objecte reservation on hi ha totes les reserves per el restauran rest_name al camp reservations */
    public Reservation getReservations(String rest_name) throws java.rmi.RemoteException;

    /* confirmReservation(int id)
     * actulitza el camp pendent de la reserva amb id id a false
     */
    public void confirmReservation(int id, ArrayList<Comand> users) throws java.rmi.RemoteException;

}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaDades;
import capaDomini.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AccessException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;



/**
 *
 * @author Jofre
 */
public class DataControler {
    private String serverIP = "tres-bon.no-ip.org";
    //private String serverIP = "tres-bon.no-ip.org";
    private int serverPort = 1099;
    private String serverURL = "rmi://" + serverIP + ":" + serverPort;
    private TestRemote remote = null;
    
    static private DataControler data = null;
    
    private DataControler() {
    }

    static public DataControler getInstancia(){

        if (data == null) {
            data = new DataControler();
        }
        return data;
    }
    
    private TestRemote remote() throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        if (remote == null) {
            System.setProperty("javax.net.ssl.trustStore", "/Users/Jofre/RMI/client.keystore");
            System.setProperty("javax.net.ssl.keyStore", "/Users/Jofre/RMI/client.keystore");
            System.setProperty("javax.net.ssl.keyStorePassword", "client");
            System.setProperty("java.rmi.server.hostname", "tres-bon.no-ip.org");
            System.out.println("Registering secure RMI socket factory ...");

            java.rmi.server.RMISocketFactory.setSocketFactory (new SecureRMISocketFactory());
            String name = serverURL + "/X";

            System.out.println("Looking up " + name + " ...");
            remote = (TestRemote) Naming.lookup(name);
            System.out.println("Conected!");
        }
        return remote;
    }
    
    public boolean correctLogin(String restname, String password) throws RemoteException, MalformedURLException, NotBoundException, AccessException, IOException {
        return remote().correctLogin(restname, password);
    }



    public Restaurant login(String restname, String password) throws RemoteException, MalformedURLException, NotBoundException, AccessException, IOException {
        return remote().login(restname, password);
    }
    
    public boolean existRestaurant(String restname) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        return remote().existRestaurant(restname);
    }

    public void registre(String name, String password, String mail, String phone, String address, String description, String category, String img) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().insertRestaurant(name, password, mail, phone, address, description, category, img);
    }

    public void updateRestaurant(String old_name, String name, String password, String address, String mail, String phone, String description, String category, String img) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().updateRestaurant(old_name, name, password, address, mail, phone, description, category, img);
    }

    public void updateDish(String rest_name, String old_dish_name, Dish dish) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().updateDish(rest_name, old_dish_name, dish);
    }

    public void deleteDish(String rest_name, String dish_name) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().deleteDish(rest_name, dish_name);
    }

    public void addDish(String rest_name, Dish dish) throws AccessException, RemoteException, NotBoundException, MalformedURLException, IOException {
        remote().addDish(rest_name, dish);
    }

    public void addMenu(String rest_name, Menu menu) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().addMenu(rest_name, menu);
    }

    public void updateMenu(String rest_name, String old_menu_name, Menu menu_backup) throws RemoteException, AccessException, NotBoundException, MalformedURLException, IOException {
        remote().updateMenu(rest_name, old_menu_name, menu_backup);
    }

    public ArrayList<Reservation> getReservations(String rest_name) throws AccessException, NotBoundException, MalformedURLException, IOException {
        return remote().getReservations(rest_name).getReservations();
    }

    public void confirmReservation(int id, ArrayList<Comand> users) throws RemoteException, AccessException, MalformedURLException, NotBoundException, IOException {
        remote().confirmReservation(id, users);
    }
    
}
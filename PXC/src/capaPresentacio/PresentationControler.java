/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package capaPresentacio;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Jofre
 */
public class PresentationControler {
    
    private VistaLogin vistalogin;
    private VistaSignin vistasignin;
    private VistaIni vistaini;
    private JFrame frame;
    
    static private PresentationControler presentation = null;
    private VistaMenu vistamenu;
    private VistaGestioPlats vistagestioplats;
    private VistaGestioReserves vistagestioreserves;
    private VistaGestioMenus vistagestiomenus;
    private VistaGestioRestaurant vistadetalls;
    private VistaConsutarPlats vistaconsultarplats;
    private VistaEditarPlats vistaeditarplats;
    private VistaAddPlat vistaaddplat;
    //private VistaDelPlats vistadelplats;
    
    
    private PresentationControler() {
        frame = TresBonApp.getInstancia();
        vistaini = new VistaIni(frame);
        
    }
    
    static public PresentationControler getInstancia(){

        if (presentation == null) {
            presentation = new PresentationControler();
        }
        return presentation;
    }
    
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                PresentationControler.getInstancia().initialize();
          }
        });
    }

    private void initialize() {
        frame.setContentPane(vistaini);
    }

    void MostraSingin(boolean nova) {
        if(nova){
            frame.setContentPane(vistaini);
            frame.setVisible(false);
            vistasignin = new VistaSignin(frame);
            frame.setContentPane(vistasignin);
            frame.setVisible(true);
            vistasignin.CanviaColor(178,111,0);
            vistasignin.CanviaMida(800, 600);


        }
        else{
            frame.setContentPane(vistasignin);
            frame.setVisible(true);
            vistasignin.CanviaColor(178,111,0);
            vistasignin.CanviaMida(800, 600);
        }
    }

    public void MostraLogin(boolean nova){
        if(nova){
            frame.setContentPane(vistaini);
            frame.setVisible(false);
            vistalogin= new VistaLogin(frame);
            frame.setContentPane(vistalogin);
            frame.setVisible(true);
            vistalogin.CanviaColor(178,111,0);
            vistalogin.CanviaMida(800, 600);

        }
        else{
            frame.setContentPane(vistalogin);
            frame.setVisible(true);
            vistalogin.CanviaColor(178,111,0);
            vistalogin.CanviaMida(800, 600);
        }
    }

    void sortidaJoc() {
        TresBonApp.getInstancia().setVisible(false);
        System.exit(0);
    }

    public void MostraPantallaPrincipal(boolean nova) {
         if(nova){
             vistaini = new VistaIni(frame);
             frame.setContentPane(vistaini);
             frame.setVisible(true);
             vistaini.CanviaColor(178,111,0);
             vistaini.CanviaMida(800, 600);
         }
         else {
            frame.setContentPane(vistaini);
            frame.setVisible(true);
            vistaini.CanviaColor(178,111,0);
            vistaini.CanviaMida(800, 600);
         }
    }

    void MostraMenu(boolean b) {
        if(b){
             vistamenu = new VistaMenu(frame);
             frame.setContentPane(vistamenu);
             frame.setVisible(true);
             vistaini.CanviaColor(178,111,0);
             vistaini.CanviaMida(1200,700);
        }
    }

    void MostraGestioPlats(boolean b) {
        if(b){
             vistagestioplats = new VistaGestioPlats(frame);
             frame.setContentPane(vistagestioplats);
             frame.setVisible(true);
             vistagestioplats.CanviaColor(178,111,0);
             vistagestioplats.CanviaMida(1200,700);
        }
    }

    void MostraGestioReserves(boolean b) {
        if(b){
             vistagestioreserves = new VistaGestioReserves(frame);
             frame.setContentPane(vistagestioreserves);
             frame.setVisible(true);
             vistagestioreserves.CanviaColor(178,111,0);
             vistagestioreserves.CanviaMida(1200,700);
        }
    }

    void MostraGestioMenus(boolean b) {
        if(b){
             vistagestiomenus = new VistaGestioMenus(frame);
             frame.setContentPane(vistagestiomenus);
             frame.setVisible(true);
             vistagestiomenus.CanviaColor(178,111,0);
             vistagestiomenus.CanviaMida(1200,700);
        }
    }

    void MostraDetalls(boolean b) {
        if(b){
             vistadetalls = new VistaGestioRestaurant(frame);
             frame.setContentPane(vistadetalls);
             frame.setVisible(true);
             vistadetalls.CanviaColor(178,111,0);
             vistadetalls.CanviaMida(1200,700);
        }
    }

    void MostraConsultarPlats() {
        vistaconsultarplats = new VistaConsutarPlats(frame);
        frame.setContentPane(vistaconsultarplats);
        frame.setVisible(true);
        vistaconsultarplats.CanviaColor(178,111,0);
        vistaconsultarplats.CanviaMida(1200,700);
    }

    void MostraEditarPlats() {
        vistaeditarplats = new VistaEditarPlats(frame);
        frame.setContentPane(vistaeditarplats);
        frame.setVisible(true);
        vistaeditarplats.CanviaColor(178,111,0);
        vistaeditarplats.CanviaMida(1200,700);
    }

    void MostraAddPlat() {
        vistaaddplat = new VistaAddPlat(frame);
        frame.setContentPane(vistaaddplat);
        frame.setVisible(true);
        vistaaddplat.CanviaMida(1200,700);
    }

    
   

}



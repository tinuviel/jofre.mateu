/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Jofre
 */
public class RMIServer extends UnicastRemoteObject implements X {
    
    public RMIServer() throws RemoteException {
	// super();
	super(0, new RMISSLClientSocketFactory(),
		 new RMISSLServerSocketFactory());
    }

    
    @Override
    public boolean login(String name, String password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public static void main(String args[]) {

	// Create and install a security manager
	/*if (System.getSecurityManager() == null) {
	    System.setSecurityManager(new RMISecurityManager());
	}*/

	try {
	    RMIServer obj = new RMIServer();

	    // Bind this object instance to the name "HelloServer"
	    Naming.rebind("//" +
		InetAddress.getLocalHost().getHostName()+"/X", obj);

	    System.out.println("X bound in registry");
	} catch (Exception e) {
	    System.out.println("X err: " + e.getMessage());
	    e.printStackTrace();
	}
    }

    
}

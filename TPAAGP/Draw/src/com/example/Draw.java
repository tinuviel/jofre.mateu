package com.example;



import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;

public class Draw extends Activity implements OnClickListener{
    DrawView drawView;
    ToggleButton b;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    
        setContentView(R.layout.main);
        drawView = (DrawView) findViewById(R.id.drawView1);
        b = (ToggleButton) findViewById(R.id.toggleButton1);
        b.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
			drawView.setEnabled(b.isChecked());
	}
}
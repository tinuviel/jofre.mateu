package com.example;

import java.util.ArrayList;
import java.util.List;

import android.R.bool;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DrawView extends View implements OnTouchListener {
    ArrayList<Point> points = new ArrayList<Point>();
    ArrayList<ArrayList<Point>> lines = new ArrayList<ArrayList<Point>>();
    Paint paint = new Paint();
    Boolean enabled = true;

    public DrawView(Context context) {
        super(context);
        setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }
    
    public DrawView(Context context, AttributeSet attrs) {  	 
    	super( context, attrs );
    	setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }
    
    public DrawView(Context context, AttributeSet attrs, int defStyle) {    	 
    	super( context, attrs, defStyle );
    	setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Canvas canvas) {
    	for(int t = 0; t < lines.size(); t++) {
    		ArrayList<Point> pnts = lines.get(t);
	    	for(int i = 0; i < pnts.size() - 1; i++){
	        	canvas.drawLine(pnts.get(i).x, pnts.get(i).y, pnts.get(i+1).x, pnts.get(i+1).y, paint);
	        }
    	}
    	
    	for(int i = 0; i < points.size() - 1; i++){
    		canvas.drawLine(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y, paint);
        }
    }

    public boolean onTouch(View view, MotionEvent event) {   
    	if(enabled) {
	    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    		System.out.println("--------------------------------Action down---------------");
	    		points = new ArrayList<Point>();
	    	}
	    	else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	    		System.out.println("--------------------------------Action move---------------");
	    		Point point = new Point();
	            point.x = event.getX();
	            point.y = event.getY();
	            points.add(point);
	            invalidate();
	    	}
	    	else if (event.getAction() == MotionEvent.ACTION_UP) {
	    		System.out.println("--------------------------------Action up---------------");
	    		lines.add(points);
	    	}
    	}
        return true;
    }
    
    public boolean getDrawEnabled() {
    	return enabled;
    }
    
    public void setDrawEnabled(Boolean e) {
    	enabled = e;
    }

    
}

class Point {
    float x, y;

    @Override
    public String toString() {
        return x + ", " + y;
    }
}
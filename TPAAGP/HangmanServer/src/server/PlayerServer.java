package server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

// TODO Server of a player
public class PlayerServer extends Thread {
	private Connection con;
	private Socket s;
	private String player;
	
	/**
	 * @param incoming
	 */
	public PlayerServer(Connection c, Socket incoming, String player) {
		con = c;
		s = incoming;
		this.player = player;
	}
	
	/**
	 * Boots up this server
	 */
	public void run() {
		try {
			System.out.println("Starting player " + player);
			
			InputStreamReader input = new InputStreamReader(s.getInputStream());
			OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
			
			String read = DataSocket.readLine(input);
			while(!read.equals("close")) {
				System.out.println(read);
				String[] cmd = read.split("#");
				if (cmd[0].equals("newGame")) newGame(output, cmd);
				else if (cmd[0].equals("showGame")) showGame(output, cmd);
				else if (cmd[0].equals("showAllGames")) showAllGames(output);
				else if (cmd[0].equals("checkPlayers")) checkPlayers(output, cmd);
				else if (cmd[0].equals("submitLetter")) submitLetter(output,cmd);
				else if (cmd[0].equals("showGameOwner")) showGameOwner(output, cmd);
				else if (cmd[0].equals("markLetter")) markLetter(output, cmd);
				else if (cmd[0].equals("showGame")) showGame(output, cmd);
				else if (cmd[0].equals("submitWord")) submitWord(output, cmd);
				else if (cmd[0].equals("showStats")) showStatistics(output);
				else if (cmd[0].equals("randomUsers")) randomUsers(output);
				else if (cmd[0].equals("drawLines")) drawLines(output, cmd);
				else if (cmd[0].equals("changePass")) changePass(output, cmd);
				
				read = DataSocket.readLine(input);
			}
			
			input.close();
			output.close();
			s.close();
		} catch(IOException e) {
			System.out.println("IO Error");
			e.printStackTrace();
		} catch(NullPointerException e) {
			System.out.println("Player " + player + " unexpectly disconnected. Finishing this Thread");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQL Error");
			e.printStackTrace();
		} 
	}
	
	private void changePass (OutputStreamWriter output, String[] cmd) throws SQLException, IOException  {
		System.out.println("Canviar contrasenya");
		System.out.println(player);
		System.out.println(cmd[1]);
		System.out.println(cmd[2]);
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT password FROM users WHERE username = '" + player+ "'");
		
		rs.next();
		String pass = rs.getString("password");
		
		if (pass.equals(cmd[1])) {
			
			query.executeUpdate("UPDATE users SET password = '" + cmd[2] + "' WHERE username = '" + player + "'");
			DataSocket.writeLine(output, "changePass#OK");
		}
		else {
			DataSocket.writeLine(output, "changePass#ERROR");
		}
	}
	
	private void submitWord(OutputStreamWriter output, String[] cmd) throws SQLException, IOException  {
		// TODO Auto-generated method stub
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT word FROM games WHERE id = " + cmd[2]);
		
		rs.next();
		String finalWord = rs.getString("word");
		
		if (finalWord.equals(cmd[1])) {
			query.executeUpdate("UPDATE users SET punts = punts + 10 WHERE username = '" + player + "'");
			query.executeUpdate("UPDATE users SET punts = punts + 10 WHERE username = '" + player + "'");
			query.executeUpdate("DELETE FROM games WHERE id = " + cmd[2]);
			query.executeUpdate("DELETE FROM letters WHERE id = " + cmd[2]);
			query.executeUpdate("DELETE FROM `lines` WHERE idgame = " + cmd[2]);
			DataSocket.writeLine(output, "submitWord#OK");
		}
		
		else {
			DataSocket.writeLine(output, "submitWord#ERROR");
		}
	}

	private void submitLetter(OutputStreamWriter output, String[] cmd) throws SQLException, IOException {
		// TODO Auto-generated method stub
				
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT nMoves, host, move FROM games WHERE id = " + cmd[2]);
		
		rs.next();
		int moves = (rs.getInt("move")+1)%rs.getInt("nMoves");
		String owner = rs.getString("host");

		
		int num = query.executeUpdate("UPDATE games " +
				"SET move = " + moves + ", curPlayer = '" + owner + "', lastLetter = '" + cmd[1] +
				"' WHERE id = " + cmd[2]);
		
		if (num == 1) {
			DataSocket.writeLine(output, "submitLetter#OK");
		}
		
		else {
			DataSocket.writeLine(output, "submitLetter#ERROR");
		}

	}

	// Helper methods	
	private void newGame(OutputStreamWriter output, String[] cmd) throws SQLException, IOException {
		// newGame#WORD#host#player1#player2...
		String word = cmd[1];
		String host = cmd[2];
	
		int numPlayers = cmd.length - 3;
		String players = "'" + cmd[3] + "'";
		for (int i = 2; i <= 3; i++) {
			if (i > numPlayers) players = players + ", NULL";	
			else players = players + ", '" + cmd[2 + i] + "'";
		}
		
		Statement query = (Statement) con.createStatement();
		int num = query.executeUpdate("INSERT INTO games(word, host, player1, player2, player3, nMoves, curPlayer) " +
											"VALUES('" + word + "', '" + host + 
													"', " + players + ", " + 
													numPlayers * 2 + ", '" + cmd[3] + "')"); 
		
		if (num == 1) {
			DataSocket.writeLine(output, "newGame#OK");
		}
		
		else {
			DataSocket.writeLine(output, "newGame#ERROR");
		}
	}
	
	private void showGame(OutputStreamWriter output, String[] cmd) throws IOException, SQLException {
		// showGame#game_id
		Statement query = (Statement) con.createStatement();
		Statement query2 = (Statement) con.createStatement();
		
		ResultSet rs = query.executeQuery("SELECT word, curPlayer FROM games WHERE id = " + cmd[1]);
		ResultSet rs2 = query2.executeQuery("SELECT letter, hit_miss FROM letters WHERE id = " + cmd[1] + " ORDER BY letter ASC");
		
		rs.next();
		String word = rs.getString("word");
		String curPlayer = rs.getString("curPlayer");
		
		String listLetters = "";
		
		char[] word_tmp = new char[word.length()];
		for (int i = 0; i < word.length(); i++) {
			word_tmp[i] = '_';
		}
		
		while (rs2.next()) {
			String letter = rs2.getString("letter");
			if (rs2.getString("hit_miss").equals("h")) {
				for (int i = 0; i < word.length(); i++) {
					if (word.charAt(i) == letter.charAt(0)) {
						word_tmp[i] = letter.charAt(0);
					}
				}
			}
			
			listLetters = listLetters + "/" + letter;
		}
		
		int len = word.length();
		word = "";
		for (int i = 0; i < len; i++) {
			word = word + word_tmp[i];
		}
		
		String packet = "showGame#" + word + "#LETTERS" + listLetters + "#" + curPlayer;
		packet = addDraws(packet, Integer.parseInt(cmd[1]));
		System.out.println(packet);
		DataSocket.writeLine(output, packet);
	}
	
	private void showAllGames(OutputStreamWriter output) throws IOException, SQLException {
		// showAllGames
		PreparedStatement pStat = (PreparedStatement) con.prepareStatement("SELECT letter FROM letters WHERE id = ? AND hit_miss = 'H'");
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT id, host, player1, player2, player3, word, move, nMoves, time, curPlayer " +
											"FROM games g " +
											"WHERE host = '" + player + "' OR " +
													"player1 = '" + player + "' OR " +
													"player2 = '" + player + "' OR " +
													"player3 = '" + player + "' " +
											"ORDER BY time ASC");
		
		// Send back a packet showAllGames#id1/admin1/curPlayer1/wordState1#id2/admin2...
		String packet = "showAllGames";
		
		while (rs.next()) {
			String gameInfo =  String.valueOf(rs.getInt("id")) + "/" + 
								rs.getString("host") + "/" +
								rs.getString("curPlayer");
			
			pStat.setString(1, String.valueOf(rs.getInt("id")));
			ResultSet tmp = pStat.executeQuery();
			String word2 = rs.getString("word");
			int len = word2.length();
			char[] wordState = new char[len];
			
			for (int i = 0; i < word2.length(); i++) {
				wordState[i] = '_';
			}
			
			while (tmp.next()) {
				char aux = tmp.getString("letter").charAt(0);
				for (int i = 0; i < word2.length(); i++) {
					if (word2.charAt(i) == aux) {
						wordState[i] = aux;
					}
				}
			}
			
			String wordState_str = "";
			for (int i = 0; i < len; i++) {
				wordState_str = wordState_str + wordState[i];
			}
			
			gameInfo = gameInfo + "/" + wordState_str;
			packet = packet + "#" + gameInfo;
		}
		
		DataSocket.writeLine(output, packet);
	}
	
	private void checkPlayers(OutputStreamWriter output, String[] cmd) throws IOException, SQLException {
		int len = cmd.length;
		ResultSet rs;
		PreparedStatement query = (PreparedStatement) con.prepareStatement("SELECT username " +
																			"FROM users " +
																			"WHERE username = ?");
		
		for (int i = 1; i < len; i++) {
			query.setString(1, cmd[i]);
			rs = query.executeQuery();
			if (!(rs.next() && rs.getString("username").equals(cmd[i]))) {
				DataSocket.writeLine(output, "checkPlayers#ERROR#" + cmd[i]);
				return;
			}
		}
		
		DataSocket.writeLine(output, "checkPlayers#OK");
	}
	


	private void showGameOwner(OutputStreamWriter output, String[] cmd) throws SQLException, IOException {
		// showGameOwner#WORD#lastPlayer#lastLetter#HIT/hit1/hit2/...#MISS/miss1/miss2/...#draws
		Statement query = (Statement) con.createStatement();
		ResultSet resSet = query.executeQuery("SELECT word, player1, player2, player3, move, nMoves, lastLetter " + 
												"FROM games " + 
												"WHERE id = " + cmd[1]);
		
		resSet.next();
		
		String player = "null";
		int nMoves = resSet.getInt("nMoves");
		
		if (nMoves == 2) {
			switch (resSet.getInt("move")) {
			case 0:
				player = resSet.getString("player1");
				break;
			default:
				player = "null";
			}
		}
		
		else if (nMoves == 4) {
			switch (resSet.getInt("move")) {
			case 2:
				player = resSet.getString("player1");
				break;
			case 0:
				player = resSet.getString("player2");
				break;
			default:
				player = "null";
			}
		}
		
		else if (nMoves == 6) {
			switch (resSet.getInt("move")) {
			case 2:
				player = resSet.getString("player1");
				break;
			case 4:
				player = resSet.getString("player2");
				break;
			case 0:
				player = resSet.getString("player3");
				break;
			default:
				player = "null";
			}
		}
		
		String lastLetter = resSet.getString("lastLetter");
		String packet = "showGameOwner#" + resSet.getString("word") + "#" + player + "#" + lastLetter;
		
		resSet = query.executeQuery("SELECT letter, hit_miss " +
									"FROM letters " +
									"WHERE id = " + cmd[1] + " " +
									"ORDER BY letter ASC");
		
		String hit = "HIT";
		String miss = "MISS";
		while (resSet.next()) {
			if (resSet.getString("hit_miss").equals("h")) {
				hit = hit + "/" + resSet.getString("letter");
			}
			
			else {
				miss = miss + "/" + resSet.getString("letter");
			}
		}
		
		packet = packet + "#" + hit + "#" + miss;
		packet = addDraws(packet, Integer.parseInt(cmd[1]));
		
		DataSocket.writeLine(output, packet);
		
		System.out.println(packet);
	}
	
	private void markLetter(OutputStreamWriter output, String[] cmd) throws SQLException, IOException {
		// markLetter#gameId#hit or miss
		Statement query = (Statement) con.createStatement();
		ResultSet resSet = query.executeQuery("SELECT player1, player2, player3, move, nMoves, lastLetter " + 
												"FROM games " + 
												"WHERE id = " + cmd[1]);
		resSet.next();
		
		// Mark the letter as hit or miss
		char hit;
		if (cmd[2].equals("hit")) hit = 'h';
		else hit = 'm';
		
		Statement update = (Statement) con.createStatement();
		
		try {
			update.executeUpdate("INSERT INTO letters(id, letter, hit_miss) " +
								"VALUES(" + cmd[1] + ", '" + resSet.getString("lastLetter") + "', '" + hit + "')");
		} catch(SQLException e) {
			// Ignore
			System.out.println("Letter already marked. Ignoring this action...");
		}
		
		// Next move
		int move = resSet.getInt("move");
		int nMoves = resSet.getInt("nMoves");
		move = (move + 1) % nMoves;
		
		String curPlayer = player;
		switch (move) {
		case 1:
			curPlayer = resSet.getString("player1");
			break;
		case 3:
			curPlayer = resSet.getString("player2");
			break;
		case 5:
			curPlayer = resSet.getString("player3");
		}
		
		int num = update.executeUpdate("UPDATE games " +
							"SET move = " + move + ", curPlayer = '" + curPlayer + "', lastLetter = NULL " +
							"WHERE id = " + cmd[1]);
		
		if (num == 1) {
			DataSocket.writeLine(output, "markLetter#OK");
		}
		
		else {
			DataSocket.writeLine(output, "markLetter#ERROR");
		}
	}
	
	private void showStatistics(OutputStreamWriter output) throws SQLException, IOException {
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT username, punts FROM users ORDER BY punts DESC");
		
		String packet = "showStats";
		while (rs.next()) {
			packet = packet + "#" + rs.getString("username") + "/" + rs.getInt("punts");
		}
		
		DataSocket.writeLine(output, packet);
		
		System.out.println(packet);
	}
	
	private void randomUsers(OutputStreamWriter output) throws SQLException, IOException {
		int num = (int) (((Math.random() * 1000) % 3) + 1);
		ArrayList<String> names = new ArrayList<String>();
		
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT username FROM users WHERE username <> '" + player + "'");
				
		while (rs.next()) {
			names.add(rs.getString("username"));
		}
		
		String packet = "randomUsers#" + num;
		int[] listNums = new int[num];
		for (int i = 0; i < num; ) {
			int rand = (int) ((Math.random() * 1000) % names.size());
			boolean repetit = false;
			
			for (int j = 0; j < i && !repetit; j++) {
				if (listNums[j] == rand) repetit = true;
			}
			
			if (!repetit) {
				listNums[i] = rand;
				packet = packet + "#" + names.get(rand);
				i++;
			}
		}
		
		DataSocket.writeLine(output, packet);
		System.out.println(packet);
	}
	
	private void drawLines(OutputStreamWriter output, String[] cmd) throws SQLException {
		int len = cmd.length;
		
		System.out.println("SELECT idline FROM lines WHERE idgame = " + cmd[1] + " ORDER BY idline ASC");
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT idline FROM `lines` WHERE idgame = " + cmd[1] + " ORDER BY idline DESC, idpoint ASC");
		rs.next();
		
		int nLine;
		if (rs.next()) nLine = rs.getInt("idline") + 1;
		else nLine = 0;
		
		Statement ps = (Statement) con.createStatement();
		
		for (int i = 2; i < len; i++) {
			String[] points = cmd[i].split("/");
			for (int j = 0; j < points.length; j++) {
				ps.executeUpdate("INSERT INTO `lines`(idgame, idline, point) " +
									"VALUES(" + cmd[1] + ", " + nLine + ", '" + points[j] +"')");
			}
			
			nLine++;
		}
	}
	
	private String addDraws(String packet, int gameId) throws SQLException {
		Statement query = (Statement) con.createStatement();
		ResultSet rs = query.executeQuery("SELECT idline, point FROM `lines` WHERE idgame = " + gameId + " ORDER BY idline, idpoint ASC");
		
		int idline_ant = -1;
		while (rs.next()) {
			if (rs.getInt("idline") != idline_ant) {
				System.out.println("Linia nova");
				packet = packet + "#" + rs.getString("point");
			}
			
			else packet = packet + "/" + rs.getString("point");
			
			idline_ant = rs.getInt("idline");
		}
		
		System.out.println(packet);
		return packet;
	}
}

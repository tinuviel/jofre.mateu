package server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class LoginServer extends Thread {
	private Socket incoming;
	
	public LoginServer(Socket inc) {
		incoming = inc;
	}
	
	public void run() {
		try {
			
			InputStreamReader input = new InputStreamReader(incoming.getInputStream());
			OutputStreamWriter output = new OutputStreamWriter(incoming.getOutputStream());
			
			String[] cmd = DataSocket.readLine(input).split("#");
			System.out.println("ouch"+cmd[0]);
			if (cmd.length == 3 && cmd[0].equals("login")) {
				// login#username#password
				String username = cmd[1];
				String password = cmd[2];

				Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/hangman", "root", "");
	            Statement query = (Statement) con.createStatement();
	            ResultSet log = query.executeQuery("SELECT username FROM users " +
	            									"WHERE username = '" + username + "' AND " +
	            											"password = '" + password + "'");
				
				if (log.next() && log.getString("username").equals(username)) {
					PlayerServer player = new PlayerServer(con, incoming, username);
					player.start();
		
					DataSocket.writeLine(output, "login#OK");
				}
				
				else {
					DataSocket.writeLine(output, "login#ERROR");
				}
			}
			
			else if (cmd.length == 5 && cmd[0].equals("signup")) {
				// signup#username#password#country#age
				String username = cmd[1];
				String password = cmd[2];
				String country = cmd[3];
				String age = cmd[4];

				boolean okInfo = checkSignInInfo(username, password, age);
				if (okInfo) {	
					Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/hangman", "root", "");
					Statement query = (Statement) con.createStatement();
					int num = 0;
					
					// This try-catch cannot be added to the outer one
					try {
			            num = query.executeUpdate("INSERT INTO users(username, password, country, age) " +
			            								"VALUES('" + username + "', '" + password + "', '" + 
			            											country + "', " + age + ")");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
					
					if (num == 1) {
						PlayerServer player = new PlayerServer(con, incoming, username);
						player.start();
						
						DataSocket.writeLine(output, "signup#OK");
					}
					
					else {
						DataSocket.writeLine(output, "signup#ERROR");
					}
				}
				
				else {
					DataSocket.writeLine(output, "signup#ERROR");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IO Exception");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	// Helper methods
	private boolean checkSignInInfo(String user, String pwd, String ageString) {
		String regExpression = "[a-zA-Z0-9]{4,10}";
		
		if (!user.matches(regExpression) ||
				!pwd.matches(regExpression) ||
					ageString.equals("")) return false;
		
		else {
			int ageInt = Integer.parseInt(ageString);
			if (ageInt >= 150) {
				return false;
			}
		}
		
		return true;
	}
}

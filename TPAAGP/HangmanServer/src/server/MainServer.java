package server;

import java.net.ServerSocket;
import java.net.Socket;

// TODO Main server receiving every player first request
public class MainServer {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub;
		int i = 1;
		try {
			// Initialize data structures			
			Class.forName ("com.mysql.jdbc.Driver").newInstance ();
			
			// Listen for new connections
			ServerSocket s = new ServerSocket(new PortNumber().port);
			while (true) {
				Socket incoming = s.accept();
				LoginServer loginServer = new LoginServer(incoming);
				loginServer.start();
				
				System.out.println("Game server " + i + " succesfully booted up");
				
				i++;
			}
		} catch (Exception e) {
			System.out.println("Exception at MainServer");
			e.printStackTrace();
		}
	}

}

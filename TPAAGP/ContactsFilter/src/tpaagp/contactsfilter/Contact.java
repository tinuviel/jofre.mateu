package tpaagp.contactsfilter;

public class Contact {
    private String name;
    private String phone;
    private String mail;
 
    public Contact(String n, String p, String m){
        name = n;
        phone = p;
        mail = m;
    }
 
    public String getName(){
        return name;
    }
 
    public String getPhone(){
        return phone;
    }
    
    public String getMail(){
        return mail;
    }
}

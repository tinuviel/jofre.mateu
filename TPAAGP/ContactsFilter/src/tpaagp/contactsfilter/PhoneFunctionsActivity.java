package tpaagp.contactsfilter;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PhoneFunctionsActivity extends Activity implements OnClickListener{
	
	private String phone, mail, name;
	private EditText sms_text;
	private Button call, sms;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
        	phone = extras.getString("contact_phone");
            mail = extras.getString("contact_mail");
            name = extras.getString("contact_name");
        }
        
        TextView cn = (TextView) findViewById(R.id.cName);
        TextView cp = (TextView) findViewById(R.id.cPhone);
        TextView cm = (TextView) findViewById(R.id.cMail);
        
        cn.setText(name);
        cp.setText(phone);
        cm.setText(mail);
        
        call = (Button) findViewById(R.id.btnCall);
        sms = (Button) findViewById(R.id.btnSMS);
        sms_text = (EditText) findViewById(R.id.editText2);
        
        call.setOnClickListener(this);
        sms.setOnClickListener(this);
  
    }
    
    private void call(String phone) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phone));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.e("helloandroid dialing example", "Call failed", e);
        }
    }
    
    private void sms(String phone, String text) {
    	PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, PhoneFunctionsActivity.class), 0);
    	SmsManager sm = SmsManager.getDefault();
	    sm.sendTextMessage(phone, null, text, pi, null);
    }


	@Override
	public void onClick(View v) {
		if(!phone.equals("")) {
			int id = v.getId();
			switch (id) {
			case R.id.btnCall:
				this.call(phone);
				break;
			case R.id.btnSMS:
				String text = sms_text.getText().toString();
				if(!text.equals("")) {
					this.sms(phone, text);
					sms_text.setText("");
				}
				break;
			}
		}	
	}
}

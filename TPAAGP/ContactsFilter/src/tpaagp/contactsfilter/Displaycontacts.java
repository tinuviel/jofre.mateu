package tpaagp.contactsfilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;



public class Displaycontacts extends Activity {
	
	boolean phone, mail, image;
	String name, name2;
	ArrayList<Contact> contacts = new ArrayList<Contact>();
	Contact[] cont;
	boolean mostrar;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactslist);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
        	phone = extras.getBoolean("phone");
            mail = extras.getBoolean("mail");
            image = extras.getBoolean("image");
            name = extras.getString("name");
        }
        
        ContentResolver cr = getContentResolver();
        ContentResolver ecr = getContentResolver();
        ContentResolver pcr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Cursor ecur = ecr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
        Cursor pcur = pcr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        
        
        if (cur.moveToFirst()) {
    	    while (cur.moveToNext()) {
    	      	mostrar = true;
    	        name2 = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
    	        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
    	        String img = null;
    	        String email = null;
    	        String phone_number = null;
    	        Boolean fi = false;
    	        Boolean has_phone = Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0;
    	        Boolean has_img = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.PHOTO_ID)) != null;
    	        //Cerca del email
	        	if(ecur.moveToFirst()) {
	        		do {
	        			if(id.equals(ecur.getString(ecur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)))) {
	        				email = ecur.getString(ecur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1));
	        				fi = true;
	        			}
	        		} while(ecur.moveToNext() && !fi);	        		
	        	}	
	        		        	        	
    	        if (phone && !has_phone && mostrar) mostrar = false;
    	        else if (mostrar && mail && email==null) mostrar = false;
    	        else if (mostrar && image && !has_img) mostrar = false;
    	        else if (mostrar && !name.equals("") && name2 != null && !name2.contains(name)) mostrar = false;
    	        
    	        if (mostrar) {
    	        	fi = false;
    	        	//Cerca del tel�fon
    	        	if(has_phone) {
    		        	if(pcur.moveToFirst()) {
    		        		do {
    		        			if(id.equals(pcur.getString(pcur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)))) {
    		        				phone_number = pcur.getString(pcur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
    		        				fi = true;
    		        			}     		        			  	
    		        		} while(pcur.moveToNext() && !fi);
    		        	}
    	        	}       	
    	        	
    	        	contacts.add(new Contact(name2, phone_number, email));
    	        }
            }
    	    
    	    int size = contacts.size();    	    
    	    Collections.sort(contacts, new Comparator<Contact>(){ 	    	 
                public int compare(Contact c1, Contact c2) {
                   return c1.getName().compareToIgnoreCase(c2.getName());
                }
    	    });
    	    
    	    cont = new Contact[size];
    	    for (int i = 0; i < size; i++) {
    	    	cont[i] = contacts.get(i);
    	    }
    	    
    	    ContactsAdapter adapter = new ContactsAdapter(this);

    	    ListView lv = (ListView) findViewById(R.id.list);
    	    lv.setAdapter(adapter);
    	    lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
					Contact aux = cont[position];
					Intent i = new Intent(Displaycontacts.this, PhoneFunctionsActivity.class);
					i.putExtra("contact_name", aux.getName());
					i.putExtra("contact_phone", aux.getPhone());
					i.putExtra("contact_mail", aux.getMail());
					startActivity(i);
				}
			});
     	}
    }
    
class ContactsAdapter extends ArrayAdapter {
    	
    	Activity context;
    	
    	@SuppressWarnings("unchecked")
		ContactsAdapter(Activity context) {
    		super(context, R.layout.listitem_contact, cont);
    		this.context = context;
    	}
    	
    	public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = context.getLayoutInflater();
			View item = inflater.inflate(R.layout.listitem_contact, null);
			
			TextView Name = (TextView)item.findViewById(R.id.Name);
			Name.setText(cont[position].getName());
			
			TextView Phone = (TextView)item.findViewById(R.id.Phone);
			Phone.setText(cont[position].getPhone());
			
			TextView Mail = (TextView)item.findViewById(R.id.Mail);
			Mail.setText(cont[position].getMail());
			
			return(item);
		}
    }
}

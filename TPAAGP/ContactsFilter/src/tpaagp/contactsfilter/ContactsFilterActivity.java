package tpaagp.contactsfilter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.view.View.OnClickListener;import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;

public class ContactsFilterActivity extends Activity {
    /** Called when the activity is first created. */
    EditText name;
    CheckBox mail, phone, image;
    Button ok;
    
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        name = (EditText)findViewById(R.id.name);
        phone = (CheckBox)findViewById(R.id.phone);
        image = (CheckBox)findViewById(R.id.image);
        mail = (CheckBox)findViewById(R.id.mail);
        ok = (Button)findViewById(R.id.ok);
        
        ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showContacts();
			}
		});     
    }
    
	private void showContacts() {
		Intent i = new Intent(this, Displaycontacts.class);
		i.putExtra("phone", phone.isChecked());
		i.putExtra("mail", mail.isChecked());
		i.putExtra("image", image.isChecked());
		i.putExtra("name", name.getText().toString());
		startActivity(i);
	}
}
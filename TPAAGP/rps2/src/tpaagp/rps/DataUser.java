package tpaagp.rps;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class DataUser extends Activity implements OnClickListener{
	
	EditText name, surname, age, nation;
	String dbname, dbsurname, dbnation;
	Integer dbage;
	DataBaseHelper dbh;
	
	int lang;
	ArrayList<String> items = new ArrayList<String>();
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
        	lang = extras.getInt("lang");
            items = extras.getStringArrayList("items");
        }
		dbh = new DataBaseHelper(this, "db", null, 1);
		
		MainRegister();
	}

	private void MainRegister() {
		setContentView(R.layout.maindatauser);
        
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.signin).setOnClickListener(this);	
	}

	public void StartGameSignin() {
		
		dbnation = nation.getText().toString();
		dbname = name.getText().toString();
		dbsurname = surname.getText().toString();
		dbage = Integer.parseInt(age.getText().toString());
		
		dbh.addContent(dbname, dbsurname, dbnation, dbage);
		
		
        Intent i = new Intent(this, GameActivity.class);
        i.putExtra("lang", lang);
        i.putExtra("items", items);
        i.putExtra("name", dbname);
        i.putExtra("surname", dbsurname);
        startActivity(i);
	}

	@Override
	public void onClick(View v) {
		Integer id = v.getId();
		switch(id) {
			case (R.id.signinok):
				StartGameSignin();
			break;
			case(R.id.loginok):
				StartGameLogin();
			break;
			case(R.id.login):
				login();
			break;
			case(R.id.signin):
				signin();
			break;
			case(R.id.loginback):
				MainRegister();
			break;
			case(R.id.signinback):
				MainRegister();
			break;
		}
	}

	private void StartGameLogin() {
		dbname = name.getText().toString();
		dbsurname = surname.getText().toString();
		
		SQLiteDatabase db = dbh.getWritableDatabase();
		
		Cursor c = db.rawQuery("SELECT NAME FROM players WHERE NAME='" + dbname + "' AND SURNAME ='" + dbsurname + "'", null);	
		if(!c.moveToFirst()) {
			String text = "No existeix l'usuari";
			Toast.makeText( getApplicationContext(), text, Toast.LENGTH_SHORT).show();
		}
		
		else {
	        Intent i = new Intent(this, GameActivity.class);
	        i.putExtra("lang", lang);
	        i.putExtra("items", items);
	        i.putExtra("name", dbname);
	        i.putExtra("surname", dbsurname);
	        startActivity(i);
		}
	}

	private void signin() {
        setContentView(R.layout.userdata);
        
        name = (EditText)findViewById(R.id.username);
        surname = (EditText)findViewById(R.id.usersurname);
        nation = (EditText)findViewById(R.id.usernationality);
        age = (EditText)findViewById(R.id.userage);
        
        findViewById(R.id.signinok).setOnClickListener(this);
        findViewById(R.id.signinback).setOnClickListener(this);
	}

	private void login() {
		setContentView(R.layout.login);
        
        name = (EditText)findViewById(R.id.username);
        surname = (EditText)findViewById(R.id.usersurname);
        
        findViewById(R.id.loginok).setOnClickListener(this);
        findViewById(R.id.loginback).setOnClickListener(this);
	}
	
}

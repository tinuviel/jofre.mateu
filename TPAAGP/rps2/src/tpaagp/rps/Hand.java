package tpaagp.rps;

import java.util.ArrayList;
import android.widget.TextView;

abstract class Mano {
	protected TextView jugador;
	protected TextView machine;
	protected TextView resultat;
	protected ArrayList<String> items;
	protected GameActivity act;
	
	Mano(TextView play, TextView res, TextView mach, ArrayList<String> it, GameActivity a) {
		jugador = play;
		machine = mach;
		resultat = res;
		items = it;
		act = a;
	}
}

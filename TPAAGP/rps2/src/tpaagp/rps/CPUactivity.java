package tpaagp.rps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class CPUactivity extends Activity {
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.cpu);
		System.out.println("--------------------INICIANT CPUactivity-----------------------------------");
		Intent i = this.getIntent();
		i.putExtra("chose", (int)(Math.random() * 1000 % 3));
		System.out.println("--------------------resultat ja calculat-----------------------------------");
		this.setResult(RESULT_OK, i);
		System.out.println("--------------------FINALITZANT CPUactivity-----------------------------------");
		finish();	
	}
}

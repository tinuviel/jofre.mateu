package tpaagp.rps;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
	 
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
	 
public class MapaActivity extends MapActivity {
    private MapView mapView;
    private GeoPoint gp;
    
    class MapOverlay extends com.google.android.maps.Overlay
    {
    	private ArrayList<GeoPoint> geop = new ArrayList<GeoPoint>();
    	
        @Override
        public void draw(Canvas canvas, MapView mapView, boolean shadow) 
        {
        	super.draw(canvas, mapView, shadow); 
        	
        	//---translate the GeoPoint to screen pixels---
        	Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pushpin); 
            Point screenPts = new Point();
            int size = geop.size();
            for (int i = 0; i < size; i++) {
	            mapView.getProjection().toPixels(geop.get(i), screenPts);
	        	canvas.drawBitmap(bmp, screenPts.x - 5, screenPts.y - 55, null);
	        	Paint p = new Paint();
	        	p.setColor(Color.BLUE);
	        	canvas.drawCircle(screenPts.x, screenPts.y, 3, p);
            }
        }
        
        public void addGeoPoint(GeoPoint p) {
        	geop.add(p);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        	super.onCreate(savedInstanceState);
	        setContentView(R.layout.map);
	        mapView = (MapView) findViewById(R.id.map);      
	        mapView.setBuiltInZoomControls(true);
	        
	        MapOverlay mapOverlay = new MapOverlay();
	        List<Overlay> listOfOverlays = mapView.getOverlays(); 
	        
	        DataBaseHelper dbh = new DataBaseHelper(this, "db", null, 1);
	        SQLiteDatabase db = dbh.getReadableDatabase();
	        
	        Cursor c = db.rawQuery("SELECT lat, lng FROM locations", null);
	        if (c.moveToFirst()) {
	        	do {
	        		Double lat = Double.parseDouble(c.getString(0)) * 1E6;
					Double lng = Double.parseDouble(c.getString(1)) * 1E6;
					gp = new GeoPoint(lat.intValue(), lng.intValue());
					mapOverlay.addGeoPoint(gp);
	        	} while (c.moveToNext());
	        }
	        
	        c.close();
	        db.close();
	        
	        /*try {
				in = openFileInput("locations.data");
				InputStreamReader tmp = new InputStreamReader(in);
				reader = new BufferedReader(tmp);
				
				String t = reader.readLine();
				while (t != null) {
					String pos[] = t.split(":");
					Double lat = Double.parseDouble(pos[0]) * 1E6;
					Double lng = Double.parseDouble(pos[1]) * 1E6;
					gp = new GeoPoint(lat.intValue(), lng.intValue());
					mapOverlay.addGeoPoint(gp);
		
				    t = reader.readLine();
		        }
				
				in.close();
				reader.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	        
	        listOfOverlays.add(mapOverlay);
	        mapView.invalidate();
    }
	 
	@Override
	protected boolean isRouteDisplayed() {
	    return false;
	}
}
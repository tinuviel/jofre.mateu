package tpaagp.rps;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
//import com.commonsware.android.maps.R;

public class RPSActivity extends Activity {
    /** Called when the activity is first created. */
	
	private int language;
	TextView txtWins;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        findViewById(R.id.cat).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				language = 0;
				StartGame();
			}
		});
        
        findViewById(R.id.cast).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				language = 1;
				StartGame();
			}
		});
        
        findViewById(R.id.en).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				language = 2;
				StartGame();
			}
		});

        txtWins = (TextView) findViewById(R.id.txtWins);
        txtWins.setText("Wins " + readWins());
    }
    
    public void onResume() {
    	super.onResume();
        txtWins.setText("Wins " + readWins());
    }
    
	private void StartGame() {
		ArrayList<String> items = new ArrayList<String>();
		InputStream in;
		switch(language) {
        case 0:
        	in=getResources().openRawResource(R.raw.catala);
        	break;
        case 1:
        	in=getResources().openRawResource(R.raw.castellano);
        	break;
        case 2:
        	in=getResources().openRawResource(R.raw.english);
        	break;
        default:
        	in=getResources().openRawResource(R.raw.catala);
        }
		
		try {
			DocumentBuilder builder=DocumentBuilderFactory
																.newInstance()
																.newDocumentBuilder();
			
			Document doc=builder.parse(in, null);
			NodeList words=doc.getElementsByTagName("word");
			
			for (int i=0;i<words.getLength();i++) {
				items.add(((Element)words.item(i)).getAttribute("value"));
			}
			
			in.close();
        }
        catch (Throwable t) {
        	Toast
        		.makeText(this, "Exception: " + t.toString(), 2000)
        		.show();
        }
		
        Intent i = new Intent(this, DataUser.class);
        i.putExtra("lang", language);
        i.putExtra("items", items);
        startActivity(i);
	}
	
	private int readWins() {
		int wins = 0;
		FileInputStream in;
		BufferedReader reader;
		
		try {
			in = openFileInput("wins.data");
			if (in != null) {
				InputStreamReader tmp = new InputStreamReader(in);
				reader = new BufferedReader(tmp);
				String t = reader.readLine();
				if (t != null) wins = Integer.parseInt(t);
				in.close();
				reader.close();
			}
		}
		catch (java.io.FileNotFoundException e) {
			// that's OK, we probably haven't created it yet
		}
		catch (Throwable t) {
			Toast
				.makeText(this, "Exception: "+t.toString(), 2000)
				.show();
		}
		
		return wins;
	}
}
package tpaagp.rps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DataBaseHelper extends SQLiteOpenHelper {
	//private static final String DATABASE_NAME="db";
	static final String PlayersTable="Players";
	static final String NAME="name";
	static final String SURNAME="surname";
	static final String AGE="age";
	static final String NATIONALITY="nationality";
	static final String GAMES_PLAYED="games_played";
	static final String GAMES_WON="games_won";
	static final String TIME = "time";
	static final String DATABASE_VERSION = "2";
	
public DataBaseHelper(Context context, String namedb, CursorFactory cf, int version) {
	super(context, namedb, cf, version);
}

@Override
public void onCreate(SQLiteDatabase db) {
    db.execSQL("CREATE TABLE IF NOT EXISTS players (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, surname TEXT, age INTEGER, nationality TEXT, games_played INTEGER, games_won INTEGER, time INTEGER);");
    db.execSQL("CREATE TABLE IF NOT EXISTS locations (_id INTEGER PRIMARY KEY AUTOINCREMENT, lat DOUBLE, lng DOUBLE, wins INTEGER);");
}

public void addContent(String nom, String surname, String nation, int age) {
	System.out.println("");
	
	SQLiteDatabase db = this.getWritableDatabase();
	
	Cursor c = db.rawQuery("SELECT NAME FROM players WHERE NAME='" + nom + "' AND SURNAME ='" + surname + "'", null);	
	if(!c.moveToFirst()) {
		ContentValues cv=new ContentValues();
		cv.put(NAME, nom);
		cv.put(SURNAME, surname);
		cv.put(NATIONALITY, nation);
		cv.put(AGE, age);
		cv.put(GAMES_PLAYED, 1);
		cv.put(GAMES_WON, 0);
		cv.put(TIME, 0);
		db.insert(PlayersTable, NAME, cv);
		c.close();
	}

}

public void updateValues(String nom, String surname, Integer won, int current_locid) {
	
	SQLiteDatabase db = this.getWritableDatabase();
	db.execSQL("UPDATE players SET GAMES_PLAYED=GAMES_PLAYED+1,GAMES_WON = GAMES_WON +" + won.toString() + " WHERE NAME='" + nom + "' AND SURNAME ='" + surname + "'");
	db.execSQL("UPDATE locations SET wins = wins + " + won.toString() + " WHERE _id = " + current_locid);
}

//bot� per netejar la base de dades
@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	db.execSQL("DROP TABLE IF EXISTS constants");
	onCreate(db);
}
}
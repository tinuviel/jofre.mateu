package tpaagp.rps;

import java.io.IOException;
import java.util.ArrayList;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

class Rock extends Mano implements OnClickListener {
	Rock(TextView play, TextView res, TextView mach, ArrayList<String> items, GameActivity a) {
		super(play, res, mach, items, a);
	}
	
	public void onClick(View view) {
		jugador.setText(items.get(7) + " " + items.get(3));
		act.auxStart();
		int a = act.auxResult();
		switch (a) {
		case 0:
			machine.setText(items.get(6) + " " + items.get(3));
			resultat.setText(items.get(2));
			act.updateDB(false);
			break;
		case 1:
			machine.setText(items.get(6) + " " + items.get(4));
			resultat.setText(items.get(1));
			act.updateDB(false);
			break;
		case 2:
			machine.setText(items.get(6) + " " + items.get(5));
			resultat.setText(items.get(0));
			try {
				act.incrementarWins();
				act.updateDB(true);
			} catch(IOException e) {}
			break;
		}
	}
}

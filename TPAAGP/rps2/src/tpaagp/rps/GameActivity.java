package tpaagp.rps;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends Activity implements OnClickListener{
	
	TextView textJugador, textResult, textMachine, mytext;
	Button buttonRock, buttonScissors, buttonPaper, map, rank;
	Rock rock;
	Paper paper;
	Scissors scissors;
	Double last_lat, last_lng;
	boolean located;
	String name, surname;
	DataBaseHelper dbh;
	Boolean isRunning = false, finish = false;
	Integer tics = 0;
	Integer chose = new Integer((int) (Math.random() * 1000 % 3));
	
	int lang;
	ArrayList<String> items = new ArrayList<String>();
	
	FileInputStream in;
	FileOutputStream fos;
	BufferedReader reader;
	int wins = 0;
	LocationManager locmgr;
	
	int current_locid;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	//System.out.println("-----------------------------Iniciant onCreate() GameActivity-----------------------------------");
        setContentView(R.layout.game);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
        	lang = extras.getInt("lang");
            items = extras.getStringArrayList("items");
            name = extras.getString("name");
            surname = extras.getString("surname");
        }
        
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener ll = new MyLocationListener();
     // Canviar a NETWORK_PROVIDER, GPS per el emulador
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, ll); 
        
        located = false;
        
        textJugador = (TextView) findViewById(R.id.txtPlayer);
		textResult = (TextView) findViewById(R.id.txtResult);
		textMachine = (TextView) findViewById(R.id.txtMachine);
		mytext = (TextView)findViewById(R.id.mytext);
		
		rock = new Rock(textJugador, textResult, textMachine, items, this);
		paper = new Paper(textJugador, textResult, textMachine, items, this);
		scissors = new Scissors(textJugador, textResult, textMachine, items, this);
		
		buttonRock = (Button) findViewById(R.id.btnRock);
		buttonPaper = (Button) findViewById(R.id.btnPaper);
		buttonScissors = (Button) findViewById(R.id.btnScissors);
		map = (Button) findViewById(R.id.map);
		rank = (Button)findViewById(R.id.rankings);
		
        map.setOnClickListener(this);
        rank.setOnClickListener(this);

		
		switch(lang) {
        case 1:
        	textJugador.setText("Jugador");
        	textMachine.setText("Máquina");
        	textResult.setText("RESULTADO");
        	buttonRock.setText(items.get(3));
        	buttonPaper.setText(items.get(4));
        	buttonScissors.setText(items.get(5));
        	break;
        case 2:
        	textJugador.setText("Player");
        	textMachine.setText("Computer");
        	textResult.setText("RESULT");
        	buttonRock.setText(items.get(3));
        	buttonPaper.setText(items.get(4));
        	buttonScissors.setText(items.get(5));
        	break;
		}
		
		buttonRock.setOnClickListener(rock);
		buttonPaper.setOnClickListener(paper);
		buttonScissors.setOnClickListener(scissors);
		
		try {
			
			in = openFileInput("wins.data");
			if (in != null) {
				InputStreamReader tmp = new InputStreamReader(in);
				reader = new BufferedReader(tmp);
				String t = reader.readLine();
				if (t != null) wins = Integer.parseInt(t);
				in.close();
			}
		}
		catch (java.io.FileNotFoundException e) {
			// that's OK, we probably haven't created it yet
		}
		catch (Throwable t) {
			Toast
				.makeText(this, "Exception: "+t.toString(), 2000)
				.show();
		}
		
		dbh = new DataBaseHelper(this, "db", null, 1);
		//System.out.println("-----------------------------Finalitzant onCreate() GameActivity-----------------------------------");
    }
    
    public void onStart() {
    	super.onStart();
    	//System.out.println("-----------------------------Iniciant onStart() GameActivity-----------------------------------");
    	isRunning = true;
    	Thread timecounter = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (isRunning) {
						Thread.sleep(1000);
						tics++;
						System.out.println(tics.toString());
					}
			    	tics = 0;
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
    	
    	timecounter.start();
    	//System.out.println("-----------------------------Finalitzant onStart() GameActivity-----------------------------------");
    }
    
    public void onStop() {
    	super.onStop();
    	//System.out.println("-----------------------------Iniciant onStop() GameActivity-----------------------------------");
    	SQLiteDatabase db = dbh.getWritableDatabase();
    	db.execSQL("UPDATE players SET time = time + " + tics.toString() +
    				" WHERE name = '" + name + "' AND surname = '" + surname + "'");
    	
    	db.close();
    	isRunning = false;
    	//System.out.println("-----------------------------Finalitzant onStop() GameActivity-----------------------------------");
    }

	public void incrementarWins() throws IOException {
    	wins++;
		fos = openFileOutput("wins.data", Context.MODE_PRIVATE);
    	fos.write(String.valueOf(wins).getBytes());	
    	fos.close();
    }
	
	public void updateDB(boolean win) {
		if (win) dbh.updateValues(name, surname, 1, current_locid);
		else dbh.updateValues(name, surname, 0, current_locid);
	}
	
	class MyLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location loc) {
			Double new_lat = (Double) loc.getLatitude();
			Double new_lng = (Double) loc.getLongitude();
			
			SQLiteDatabase db;
			db = dbh.getWritableDatabase();
			Cursor c = db.rawQuery("SELECT _id " +
									"FROM locations WHERE abs(lat - " + new_lat + ") < 0.03 AND " +
														"abs(lng - " + new_lng + ") < 0.03", null);	
			
			if (!c.moveToFirst()) {
				db.execSQL("INSERT INTO locations(lat, lng, wins) " +
							"VALUES (" + new_lat + ", " + new_lng + ", 0)");
				Cursor c1 = db.rawQuery("SELECT _id " +
										"FROM locations WHERE lat = " + new_lat + " AND " +
										"lng = " + new_lng, null);
				
				c1.moveToFirst();
				current_locid = Integer.parseInt(c1.getString(0));
				c1.close();
				String Text = "My current location is: Latitud = " + loc.getLatitude() + " Longitud = " + loc.getLongitude();
				Toast.makeText( getApplicationContext(),
				Text,
				Toast.LENGTH_SHORT).show();
			}
			
			else {
				current_locid = Integer.parseInt(c.getString(0));
			}
			
			c.close();
			db.close();
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public void onClick(View v) {
		Integer id = v.getId();
		Intent in;
		switch (id) {
			case (R.id.map):
		        in = new Intent(this, MapaActivity.class);
	        	startActivity(in);
	        	break;
			case (R.id.rankings):
		        in = new Intent(this, RankingActivity.class);
        		startActivity(in);
        		break;
			default:
				break;
		}
		
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		System.out.println("--------------------Iniciant onActivityResult()-----------------------------------");
		//super.onActivityResult(requestCode, resultCode, data);
		finish = true;
		chose = data.getIntExtra("chose", 0);
		System.out.println("--------------------Finalitzant onActivityResult()-----------------------------------");
	}
	
	public void auxStart() {
		System.out.println("--------------------Iniciant auxStart()-----------------------------------");
	//	isRunning = false;
		Intent i = new Intent(this, CPUactivity.class);
		startActivityForResult(i, 1);
		System.out.println("--------------------Finalitzant auxStart()-----------------------------------");
	}
	
	public Integer auxResult() {
		System.out.println("--------------------Iniciant auxResult()-----------------------------------");
		System.out.println("--------------------Finalitzant auxResult()-----------------------------------");
		return chose;
	}
}

package tpaagp.rps;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.TextView;

public class RankingActivity extends Activity {
	DataBaseHelper dbh;
	SQLiteDatabase db;
	TextView name, surname, games_won, time;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rank);
		dbh = new DataBaseHelper(this, "db", null, 1);
		db = dbh.getReadableDatabase();
		
		name = (TextView)findViewById(R.id.dataname);
		surname = (TextView)findViewById(R.id.datasurname);
		games_won = (TextView)findViewById(R.id.datagameswon);
		time = (TextView)findViewById(R.id.datatime);
		
		Cursor c = db.rawQuery("SELECT name, surname, games_won, time FROM players ORDER BY games_won DESC", null);
		showData(c);
		db.close();
	}
	
	private void showData (Cursor c) {
		String a = "", b = "", d = "", e = "";
		if (c.moveToFirst()) {
			do {
				a = a + c.getString(0) + "\n";
				b = b + c.getString(1) + "\n";
				d = d + c.getString(2) + "\n";
				e = e + c.getString(3) + "\n";
			} while (c.moveToNext());
		}
		c.close();
		name.setText(a);
		surname.setText(b);
		games_won.setText(d);
		time.setText(e);
	}
}

package tpaagp.call;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class CallSMSActivity extends Activity implements OnClickListener{
    private EditText phone_number, sms_text;
    private Button call, sms;
	
	/** Called when the activity is first created. */
    @Override
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        phone_number = (EditText) findViewById(R.id.editText1);
        call = (Button) findViewById(R.id.btnCall);
        sms = (Button) findViewById(R.id.btnSMS);
        sms_text = (EditText) findViewById(R.id.editText2);
        
        call.setOnClickListener(this);
        sms.setOnClickListener(this);
  
    }
    
    private void call(String phone) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phone));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.e("helloandroid dialing example", "Call failed", e);
        }
    }
    
    private void sms(String phone, String text) {
    	PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, CallSMSActivity.class), 0);
    	SmsManager sm = SmsManager.getDefault();
	    sm.sendTextMessage(phone, null, text, pi, null);
    }


	@Override
	public void onClick(View v) {
		String phone = phone_number.getText().toString();
		if(!phone.equals("")) {
			int id = v.getId();
			switch (id) {
			case R.id.btnCall:
				this.call(phone);
				phone_number.setText("");
				break;
			case R.id.btnSMS:
				String text = sms_text.getText().toString();
				if(text.equals("")) {
					this.sms(phone, text);
					phone_number.setText("");
					sms_text.setText("");
				}
				break;
			}
		}
		
	}
}
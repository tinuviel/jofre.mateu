package tpaagp.rps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class DataUser extends Activity {
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userdata);
        
        findViewById(R.id.ok).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				StartGame();
			}
		});
	}

	public void StartGame() {
        Intent i = new Intent(this, GameActivity.class);
        startActivity(i);
	}
	
}

package tpaagp.rps;

//import com.google.android.maps.MapView;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;

	 
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
	 
public class MapaActivity extends MapActivity {
    
    private MapView mapView;
    GeoPoint p;
    Boolean done;
    FileInputStream in;
    BufferedReader reader;
    Double lat, lng;
    
    
    class MapOverlay extends com.google.android.maps.Overlay
    {
        @Override
        public boolean draw(Canvas canvas, MapView mapView, 
        boolean shadow, long when) 
        {
            super.draw(canvas, mapView, shadow);                  
 
            //---translate the GeoPoint to screen pixels---
            Point screenPts = new Point();
            mapView.getProjection().toPixels(p, screenPts);
 
            //---add the marker---
            Bitmap bmp = BitmapFactory.decodeResource(
                getResources(), R.drawable.pushpin);            
            canvas.drawBitmap(bmp, screenPts.x, screenPts.y-50, null);         
            return true;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
	        setContentView(R.layout.map);
	        
	        done = false;
	        mapView = (MapView) findViewById(R.id.map);      
	        mapView.setBuiltInZoomControls(true);
	        
	        MapOverlay mapOverlay = new MapOverlay();
	        List<Overlay> listOfOverlays = mapView.getOverlays();
	        listOfOverlays.clear();
	        
	        try {
				in = openFileInput("locations.data");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        String t = ".";
	        while(t != null) {
	        	try {
					InputStreamReader tmp = new InputStreamReader(in);
					reader = new BufferedReader(tmp);
					t = reader.readLine();
					if (t != null){
						lat = Double.parseDouble(t);
						System.out.println("Latitud carregada correctament " + lat.toString() + "-----------------------------------");
					}
					
					t = reader.readLine();
					if (t != null) {
						lng = Double.parseDouble(t);
						System.out.println("Longitud carregada correctament " + lng.toString() + "----------------------------------");
					}
	        	}
	        	catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	
	        	p = new GeoPoint(lat.intValue(), lng.intValue());
	        	System.out.println("Punt carregat---------------------------------------------------------------");
	        	listOfOverlays.add(mapOverlay);       
	        }
	        System.out.println("FINALITZAT EL BUCLE!!!!!!!!-----------------------------------------------------------------");
	        try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         
	 
	        //mapView.invalidate();
	    }
	 
	    @Override
	    protected boolean isRouteDisplayed() {
	        return false;
	    }
	     
	}
package tpaagp.rps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.SensorManager;

public class DataBase extends SQLiteOpenHelper {
private static final String DATABASE_NAME="db";
static final String NAME="name";
static final String SURNAME="surname";
static final String AGE="age";
static final String NATIONALITY="nationality";
static final String GAMES_PLAYED="games_played";
static final String GAMES_WON="games_won";

public DataBase(Context context) {
	super(context, DATABASE_NAME, null, 1);
}

@Override
public void onCreate(SQLiteDatabase db) {
	db.execSQL("CREATE TABLE players (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, surname TEXT, age INTEGER, nationality TEXT, games_played INTEGER, games_won INTEGER);");
	
	ContentValues cv=new ContentValues();
	
	
	/*cv.put(TITLE, "Gravity, Death Star I");
	cv.put(VALUE, SensorManager.GRAVITY_DEATH_STAR_I);
	db.insert("constants", TITLE, cv);*/
}


//bot� per netejar la base de dades
@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	android.util.Log.w("Constants", "Upgrading database, which will destroy all old data");
	db.execSQL("DROP TABLE IF EXISTS constants");
	onCreate(db);
}
}
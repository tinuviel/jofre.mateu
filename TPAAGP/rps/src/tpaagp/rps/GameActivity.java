package tpaagp.rps;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import com.commonsware.android.maps.R;

public class GameActivity extends Activity {
	
	TextView textJugador, textResult, textMachine, mytext;
	Button buttonRock, buttonScissors, buttonPaper, map;
	Rock rock;
	Paper paper;
	Scissors scissors;
	Boolean located;
	
	int lang;
	ArrayList<String> items = new ArrayList<String>();
	
	FileInputStream in;
	FileOutputStream fos;
	BufferedReader reader;
	int wins = 0;
	LocationManager locmgr;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
        	lang = extras.getInt("lang");
            items = extras.getStringArrayList("items");
        }
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener ll = new MyLocationListener();
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, ll);
        
        located = false;
        
        textJugador = (TextView) findViewById(R.id.txtPlayer);
		textResult = (TextView) findViewById(R.id.txtResult);
		textMachine = (TextView) findViewById(R.id.txtMachine);
		mytext = (TextView)findViewById(R.id.mytext);
		
		rock = new Rock(textJugador, textResult, textMachine, items, this);
		paper = new Paper(textJugador, textResult, textMachine, items, this);
		scissors = new Scissors(textJugador, textResult, textMachine, items, this);
		
		buttonRock = (Button) findViewById(R.id.btnRock);
		buttonPaper = (Button) findViewById(R.id.btnPaper);
		buttonScissors = (Button) findViewById(R.id.btnScissors);
		map = (Button) findViewById(R.id.map);
		
        map.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ConsultMap();
			}
		});
		
		switch(lang) {
        case 1:
        	textJugador.setText("Jugador");
        	textMachine.setText("Máquina");
        	textResult.setText("RESULTADO");
        	buttonRock.setText(items.get(3));
        	buttonPaper.setText(items.get(4));
        	buttonScissors.setText(items.get(5));
        	break;
        case 2:
        	textJugador.setText("Player");
        	textMachine.setText("Computer");
        	textResult.setText("RESULT");
        	buttonRock.setText(items.get(3));
        	buttonPaper.setText(items.get(4));
        	buttonScissors.setText(items.get(5));
        	break;
		}
		
		buttonRock.setOnClickListener(rock);
		buttonPaper.setOnClickListener(paper);
		buttonScissors.setOnClickListener(scissors);
		
		try {
			
			in = openFileInput("wins.data");
			if (in != null) {
				InputStreamReader tmp = new InputStreamReader(in);
				reader = new BufferedReader(tmp);
				String t = reader.readLine();
				if (t != null) wins = Integer.parseInt(t);
				in.close();
			}
		}
		catch (java.io.FileNotFoundException e) {
			// that's OK, we probably haven't created it yet
		}
		catch (Throwable t) {
			Toast
				.makeText(this, "Exception: "+t.toString(), 2000)
				.show();
		}
    }
    
    
	private void ConsultMap() {
        Intent in = new Intent(this, MapaActivity.class);
        //in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);
	}

	public void incrementarWins() throws IOException {
    	wins++;
		fos = openFileOutput("wins.data", Context.MODE_PRIVATE);
    	fos.write(String.valueOf(wins).getBytes());	
    	fos.close();
    }
	
	public class MyLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location loc) {
			if (!located) {
				try {
					fos = openFileOutput("locations.data", MODE_APPEND);
					fos.write(((Double)loc.getLatitude()).toString().getBytes());
					fos.write("\n".getBytes());
					fos.write(((Double)loc.getLongitude()).toString().getBytes());
					fos.write("\n".getBytes());
					
			    	fos.close();
			    	located = true;
			    	String Text = "My current location is:" + "Latitud = " + loc.getLatitude() + "Longitud = " + loc.getLongitude();
					Toast.makeText( getApplicationContext(),
					Text,
					Toast.LENGTH_SHORT).show();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	}
}

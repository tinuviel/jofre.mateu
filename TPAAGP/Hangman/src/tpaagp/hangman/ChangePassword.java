package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChangePassword extends Activity implements OnClickListener {
	private String username;
	private Socket s;
	private EditText oldp, newp, repp;
	private Button save;
	private String[] info = {};
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepassword);
		
		Bundle b = getIntent().getExtras();
		username = b.getString("username");
		System.out.println(username);

		info = new String [3];
		
		oldp = (EditText)findViewById(R.id.oldPass);
		newp = (EditText)findViewById(R.id.newPass);
		repp = (EditText)findViewById(R.id.repPass);
		

		save = (Button)findViewById(R.id.save);
		save.setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.save:
			info[0] = oldp.getText().toString();
			info[1] = newp.getText().toString();
			info[2] = repp.getText().toString();
			if (info[1].equals(info[2])) new ChangePass().execute(info);
			else {
				System.out.println("és broma que peta?");
				Popup popup = new Popup(ChangePassword.this);
				popup.show("Game", "The passwords do not match. Try again!");
			}
			break;
		}
	}
	
	private class ChangePass extends AsyncTask<String, Void, Boolean> {
		  ProgressDialog pDialog;
	       
	        protected Boolean doInBackground(String... params) {
	            // TODO Auto-generated method stub
	            publishProgress();           

	            try {
	                InputStreamReader input = new InputStreamReader(s.getInputStream());
	                OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
	                System.out.println(params[0]);
	                System.out.println(params[1]);
	                DataSocket.writeLine(output, "changePass#" + params[0] + "#" + params[1]);
	                String[] response = DataSocket.readLine(input).split("#");
	               
	                if (response[1].equals("OK")) {
	                    return true;
	                }
	               
	                return false;
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	           
	            return false;
	        }
	       
	        protected void onProgressUpdate(Void... v) {
	            pDialog = ProgressDialog.show(ChangePassword.this, "", "Changing your password. Please wait...", true);
	        }
	       
	        protected void onPostExecute(Boolean b) {
	            pDialog.dismiss();           
	            if (!b) {
	                Popup popup = new Popup(ChangePassword.this);
	                popup.show("Game", "The old password does not match with the user's one. Try again.");
	            }
	           
	            else {
	                Popup popup = new Popup(ChangePassword.this);
	                popup.show("Game", "Password changed succesfully");
	            }
	        }
	    
	}
	
}

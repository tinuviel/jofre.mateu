package tpaagp.hangman;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class DataSocket {
	/**
	 * 
	 * @param input
	 * @return
	 * @throws IOException
	 */
	public static String readLine(InputStreamReader input) throws IOException {
		BufferedReader buffer = new BufferedReader(input);
		return buffer.readLine();
	}
	
	/**
	 * 
	 * @param output
	 * @param text
	 * @throws IOException
	 */
	public static void writeLine(OutputStreamWriter output, String text) throws IOException {
		BufferedWriter buffer = new BufferedWriter(output);
		buffer.write(text);
		buffer.newLine();
		buffer.flush();
	}
}

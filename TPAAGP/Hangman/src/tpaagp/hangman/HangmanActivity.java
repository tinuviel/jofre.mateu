package tpaagp.hangman;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import android.content.Context;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class HangmanActivity extends Activity implements OnClickListener {
	private Socket s;
	private EditText user, pass;
	private Button login, account;
	private TextView error;
	private CheckBox remember;
	private FileOutputStream fos;
	private FileInputStream fis;
	Boolean isRunning;
	MediaPlayer mp;
	Integer tics = 0;

	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        System.out.println("Entro a la Hangman Activity");
        
        user = (EditText)findViewById(R.id.username);
        pass = (EditText)findViewById(R.id.password);
        login = (Button)findViewById(R.id.login);
        error = (TextView)findViewById(R.id.error);
        account = (Button) findViewById(R.id.account);
        remember = (CheckBox)findViewById(R.id.remember);
        
        user.setHint("Username");
        pass.setHint("Password");
                
        login.setOnClickListener(this);
        account.setOnClickListener(this);
        
        if (consultaUsuariExistent()) {
        	remember.setChecked(true);
        	login(user.getText().toString(), pass.getText().toString(), false);
        }
        
    }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		int vid = arg0.getId();
		switch (vid) {
		case R.id.login:
			String u, p;
			u = user.getText().toString();
			p = pass.getText().toString();
			Boolean check;
			check = remember.isChecked();
			login(u, p, check);
			break;
		case R.id.account:
			Intent i = new Intent(this, SignUpActivity.class);
			startActivity(i);
			break;
		}
	}
	
	public void onStart() {
    	super.onStart();
    	//System.out.println("-----------------------------Iniciant onStart() GameActivity-----------------------------------");
        mp = MediaPlayer.create(getApplicationContext(), R.raw.aisia);
        mp.start();
        
        mp.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.start();
				
			}
		});
    	/*Thread timecounter = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (tics < 117) {
						Thread.sleep(1000);
						tics++;
						System.out.println(tics.toString());
						if (tics == 116) {
							try {
								mp.reset();
								mp.prepare();
								mp.start();
							} catch (IllegalStateException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							tics = 0;
						}
						//System.out.println(tics.toString());
					}
			    	
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
    	
    	timecounter.start();*/
    	//System.out.println("-----------------------------Finalitzant onStart() GameActivity-----------------------------------");
    }
	
	// Asynchronous tasks in order to avoid blocking UI-Thread
	private class LoginTask extends AsyncTask<String, Void, Boolean> {
		ProgressDialog pDialog;
		
		@Override
		protected Boolean doInBackground(String... login) {
			// TODO Auto-generated method stub
			String username = login[0];
			String password = login[1];
			
			try {
				publishProgress();
				String address = ServerIP.address;
				int port = ServerIP.port;
				s = new Socket(InetAddress.getByName(address), port);
				
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "login#" + username + "#" + password);
				String[] response = DataSocket.readLine(input).split("#");
				if (response[0].equals("login") && response[1].equals("OK")) {
					return true;
				}
				
				else {
					return false;
				}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				System.out.println("Unknown Host Exception");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("IO Exception");
				e.printStackTrace();
			}
			
			return false;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(HangmanActivity.this, "", "Logging in. Please wait...", true);
		}
		
		protected void onPostExecute(Boolean result) {
			pDialog.dismiss();
			
			if (result) {
				// Login OK so move ahead to the main menu window
				Intent i = new Intent(HangmanActivity.this, MainMenuActivity.class);
				i.putExtra("username", user.getText().toString());
				SocketApplication sApp = (SocketApplication) getApplication();
				sApp.socket = s;
				startActivity(i);
			}
			
			else {
				// Wrong login so pop up an alert
				Popup popup = new Popup(HangmanActivity.this);
				popup.show("Login", "Incorrect login");
				
				try {
					s.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("IO Exception");
					e.printStackTrace();
				}
			}
		}
	}
	
	// Helper methods
	private void login(String u, String p, Boolean b) {
    	if (u.equals("")) error.setText("Incorrect username");
    	else if (p.equals("")) error.setText("Incorrect password");    	
    	else {
    		if (b) {
    			try {
					fos = openFileOutput("user.data", Context.MODE_PRIVATE);
	    	    	fos.write(new String(u + "#" + p).getBytes());	
	    	    	fos.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		new LoginTask().execute(u, p);
    	}
    }
	
	private boolean consultaUsuariExistent() {
		try {
			fis = openFileInput("user.data");
	    	BufferedReader buff = new BufferedReader(new InputStreamReader(fis));
	    	String text = buff.readLine();
	    	String[] login = text.split("#");
	    	user.setText(login[0]);
	    	pass.setText(login[1]);
	    	buff.close();
	    	fis.close();
	    	return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
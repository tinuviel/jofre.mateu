package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class FindPlayersActivity extends Activity implements OnClickListener {
	private String username;
	
	private Socket s;
	
	private int numPlayers;
	
	private TextView[] txtPlayer;
	private EditText[] editPlayer;
	private ImageButton[] imgPlayer;

	private Button addPlayer;
	private Button ok;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.findusers);
		
		numPlayers = 1;
		
		Bundle b = getIntent().getExtras();
		username = b.getString("username");
		
		txtPlayer = new TextView[3];
		editPlayer = new EditText[3];
		imgPlayer = new ImageButton[3];
		
		txtPlayer[0] = (TextView) findViewById(R.id.txtPlayer1);
		txtPlayer[1] = (TextView) findViewById(R.id.txtPlayer2);
		txtPlayer[2] = (TextView) findViewById(R.id.txtPlayer3);
		
		editPlayer[0] = (EditText) findViewById(R.id.editPlayer1);
		editPlayer[1] = (EditText) findViewById(R.id.editPlayer2);
		editPlayer[2] = (EditText) findViewById(R.id.editPlayer3);
		
		imgPlayer[0] = (ImageButton) findViewById(R.id.imgRemovePlayer1);
		imgPlayer[1] = (ImageButton) findViewById(R.id.imgRemovePlayer2);
		imgPlayer[2] = (ImageButton) findViewById(R.id.imgRemovePlayer3);
		
		addPlayer = (Button) findViewById(R.id.btnAddPlayers);
		ok = (Button) findViewById(R.id.btnOKPlayers);
		
		txtPlayer[1].setVisibility(TextView.INVISIBLE);
		txtPlayer[2].setVisibility(TextView.INVISIBLE);
		
		editPlayer[1].setVisibility(EditText.INVISIBLE);
		editPlayer[2].setVisibility(EditText.INVISIBLE);
		
		imgPlayer[0].setVisibility(ImageButton.INVISIBLE);
		imgPlayer[1].setVisibility(ImageButton.INVISIBLE);
		imgPlayer[2].setVisibility(ImageButton.INVISIBLE);
		
		addPlayer.setOnClickListener(this);
		ok.setOnClickListener(this);
		
		imgPlayer[0].setOnClickListener(this);
		imgPlayer[1].setOnClickListener(this);
		imgPlayer[2].setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
		case R.id.btnAddPlayers:
			if (numPlayers < 3) {
				if (numPlayers == 1) imgPlayer[0].setVisibility(ImageButton.VISIBLE);
				
				numPlayers++;
				txtPlayer[numPlayers - 1].setVisibility(TextView.VISIBLE);
				editPlayer[numPlayers - 1].setVisibility(EditText.VISIBLE);
				imgPlayer[numPlayers - 1].setVisibility(ImageButton.VISIBLE);
				
				if (numPlayers == 3) addPlayer.setVisibility(Button.INVISIBLE);
			}
			
			break;
		case R.id.btnOKPlayers:
			String[] players = new String[numPlayers];
			for (int i = 0; i < numPlayers; i++) {
				players[i] = editPlayer[i].getText().toString();
				if (players[i].equals(username)) {
					// User cannot invite himself to the game
					Popup popup = new Popup(FindPlayersActivity.this);
					popup.show("Players", "You cannot invite yourself to the game");
					return;
				}
				
				else if (players[i].equals("")) {
					return;
				}
				
				else {
					for (int j = 0; j < i; j++) {
						if (players[i].equals(players[j])) {
							Popup popup = new Popup(FindPlayersActivity.this);
							popup.show("Players", "You cannot invite the same player twice");
							return;
						}
					}
				}
			}
			
			new CheckPlayersTask().execute(players);
			break;
		default:
			int i = 0;
			if (v.getId() == R.id.imgRemovePlayer2) i = 1;
			else if (v.getId() == R.id.imgRemovePlayer3) i = 2;
			
			for (; i < numPlayers - 1; i++) {
				editPlayer[i].setText(editPlayer[i + 1].getText().toString());	
			}
			
			if (numPlayers > 1) {
				txtPlayer[numPlayers - 1].setVisibility(TextView.INVISIBLE);
				editPlayer[numPlayers - 1].setVisibility(EditText.INVISIBLE);
				imgPlayer[numPlayers - 1].setVisibility(ImageButton.INVISIBLE);
				numPlayers--;
				
				if (numPlayers == 1) imgPlayer[0].setVisibility(ImageButton.INVISIBLE);
				addPlayer.setVisibility(Button.VISIBLE);
			}
			
			break;
		}
	}
	
	// Asynchronous tasks
	private class CheckPlayersTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			String packet = "checkPlayers";
			int len = arg0.length;
			
			for (int i = 0; i < len; i++) {
				packet = packet + "#" + arg0[i];
			}
			
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, packet);
				String[] response = DataSocket.readLine(input).split("#");
				if (response[0].equals("checkPlayers") && response[1].equals("OK")) {
					return null;
				}
				
				else {
					return response[2];
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String result) {
			if (result == null) {
				// Players names are OK so let's get ready to return the result
				Intent i = new Intent();
				i.putExtra("numPlayers", numPlayers);
				for (int j = 0; j < numPlayers; j++) {
					i.putExtra("player" + (j + 1), editPlayer[j].getText().toString());
				}
				
		        setResult(RESULT_OK, i);
		        finish();
			}
			
			else {
				// Wrong players so pop up an alert
				Popup popup = new Popup(FindPlayersActivity.this);
				popup.show("Players", "Player " + result + " does not exist");
			}
		}
	}
}

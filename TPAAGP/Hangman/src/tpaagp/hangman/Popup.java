package tpaagp.hangman;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Popup extends Dialog implements OnClickListener {
	private TextView info;
	private Button ok;
	
	public Popup(Context context) {
		super(context);
		setContentView(R.layout.popup);
		
		info = (TextView) findViewById(R.id.txtInfo);
		ok = (Button) findViewById(R.id.btnOK);
		ok.setOnClickListener(this);
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		this.dismiss();
	}

	public void onBackPressed() {
		this.dismiss();
	}
	
	public void show(String title, String text) {
		this.setTitle(title);
		info.setText(text);
		this.show();
	}
}

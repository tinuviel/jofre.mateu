package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class GameOwnerActivity extends Activity implements OnClickListener {
	private Socket s;
	private int gameId;
	
	private Button right;
	private Button wrong;
	private Button ok;
	
	private TextView word;
	private TextView missedLetters;
	private TextView lastLetter;
	
	private String lastLetterChar;
	private DrawView draw;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gameowner);
		
		Bundle b = getIntent().getExtras();
		gameId = b.getInt("game_id");
		
		lastLetterChar = null;
		
		right = (Button) findViewById(R.id.btnLetterRight);
		wrong = (Button) findViewById(R.id.btnLetterWrong);
		ok = (Button) findViewById(R.id.goOK);
		
		word = (TextView) findViewById(R.id.txtWord);
		missedLetters = (TextView) findViewById(R.id.txtListWrong);
		lastLetter = (TextView) findViewById(R.id.txtPlayerLetter);
		draw = (DrawView) findViewById(R.id.drawView);
		ok.setVisibility(4);
		draw.setTouchEnabled(false);
		
		right.setOnClickListener(this);
		wrong.setOnClickListener(this);
		ok.setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
		
		new ShowGameOwnerTask().execute();
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Boolean hit;
		int vId = arg0.getId();
		
		if(vId == ok.getId()) {
			ArrayList<ArrayList<Point>> lines = draw.getPendingLines();
			right.setVisibility(0);
			wrong.setVisibility(0);
			ok.setVisibility(4);
			draw.setTouchEnabled(false);
			/*Popup popup = new Popup(GameOwnerActivity.this);
			popup.show("Game", "Drawed!");*/
			new sendDrawTask().execute(lines);
		}
		else {
			if (lastLetterChar != null) {
				if (vId == R.id.btnLetterRight) {
					if (!word.getText().toString().contains(lastLetterChar)) {
						Popup popup = new Popup(GameOwnerActivity.this);
						popup.show("Game", "Do not lie!");
						return;
					}
					
					hit = true;
				}
				else {
					if (word.getText().toString().contains(lastLetterChar)) {
						Popup popup = new Popup(GameOwnerActivity.this);
						popup.show("Game", "Do not lie!");
						return;
					}
					right.setVisibility(4);
					wrong.setVisibility(4);
					ok.setVisibility(0);
					draw.setTouchEnabled(true);
					hit = false;
				}
				new MarkLetterTask().execute(hit);
			}
			
			
		}
	}
	
	// Asynchronous tasks
	private class ShowGameOwnerTask extends AsyncTask<Void, Void, String> {
		// showGameOwner#gameId
		
		@Override
		protected String doInBackground(Void... arg0) {
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "showGameOwner#" + gameId);
				String response = DataSocket.readLine(input);
				
				return response;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String result) {	
			// showGameOwner#WORD#lastPlayer#lastLetter#HIT/hit1/hit2/...#MISS/miss1/miss2/...#draws
			if (result != null) {
				System.out.println(result);
				String[] cmd = result.split("#");
				
				String sWord = cmd[1];
				String sPlayer = cmd[2];
				String sLetter = cmd[3];
				
				if (!sLetter.equals("null")) {
					lastLetter.setText("Player " + sPlayer + " said " + sLetter);
					lastLetterChar = sLetter;
				}
				
				else {
					lastLetter.setText(" ");
					right.setEnabled(false);
					wrong.setEnabled(false);
				}
				
				String[] sHits = cmd[4].split("/");
				String[] sMiss = cmd[5].split("/");
				
				String html = "";
				int wordLen = sWord.length();
				for (int i = 0; i < wordLen; i++) {
					// We check if its hit
					boolean isHit = false;
					for (int j = 1; j < sHits.length && !isHit; j++) {
						if (sWord.charAt(i) == sHits[j].charAt(0)) {
							html = html + "<font color=#00FF00>" + sWord.charAt(i) + "</font>";
							isHit = true;
						}
					}
					
					if (!isHit) {
						html = html + "<font color=#FF0000>" + sWord.charAt(i) + "</font>";
					}
				}
				
				word.setText(Html.fromHtml(html));
				
				String sLetters = "";
				Integer len = sMiss.length;
				if (len > 1) {
					sLetters = "Other missed letters are " + sMiss[1];
					for (int i = 2; i < len; i++) {
						sLetters = sLetters + ", " + sMiss[i];
					}
				}

				missedLetters.setText(sLetters);
				System.out.println("Parse de les linies de dibuix");
				// Draw
				ArrayList<ArrayList<Point>> lines = new  ArrayList<ArrayList<Point>>();
				
				len = cmd.length;
				System.out.println("Longitud del vector cmd = " + len.toString());
				System.out.println(cmd[5]);
				for(int i = 6; i < len; i++) {
					System.out.println(cmd[i]);
					String[] line = cmd[i].split("/");
					ArrayList<Point> points = new ArrayList<Point>();
					for(int j = 0; j < line.length; j++) {
						points.add(new Point(line[j]));
					}
					lines.add(points);
				}
				
				draw.setLines(lines);
			}
		}
	}
	
	private class MarkLetterTask extends AsyncTask<Boolean, Void, Boolean> {
		// showGameOwner#gameId
		
		@Override
		protected Boolean doInBackground(Boolean... arg0) {
			try {
				String mark;
				if (arg0[0]) mark = "hit";
				else mark = "false";
				
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "markLetter#" + gameId + "#" + mark);
				String[] response = DataSocket.readLine(input).split("#");
				
				if (response[1].equals("OK")) {
					return true;
				}
				
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
		}
		
		protected void onPostExecute(Boolean b) {
			if (!b) {
				Popup popup = new Popup(GameOwnerActivity.this);
				popup.show("Game", "An error occured at marking the letter. Please try again");
			}
			
			else {
				Popup popup = new Popup(GameOwnerActivity.this);
				popup.show("Game", "Letter marked succesfully");
				
				right.setEnabled(false);
				wrong.setEnabled(false);
			}
		}
	}
	
	private class sendDrawTask extends AsyncTask<ArrayList<ArrayList<Point>>, Void, Boolean> {

		@Override
		protected Boolean doInBackground(ArrayList<ArrayList<Point>>... arg0) {
			ArrayList<ArrayList<Point>> lines = arg0[0];
			
			try {
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				String packet = "drawLines#" + gameId;
				for (int i = 0; i < lines.size(); i++) {
					packet = packet + "#" + lines.get(i).get(0).toString();
					for (int j = 1; j < lines.get(i).size(); j++) {
						packet = packet + "/" + lines.get(i).get(j).toString();
					}
				}
				
				DataSocket.writeLine(output, packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			return null;
		}
		
	}
}

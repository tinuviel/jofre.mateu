package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class NewGameActivity extends Activity implements OnClickListener {
	private final int reqCode = 13; 
	
	private final int CAT = 0;
	private final int SPA = 1;
	private final int ENG = 2;
	
	private Socket s;
	
	private String username;
	
	private int numPlayers;
	private String[] playerNames;
	
	private boolean[] lang;
	
	private TextView players;
	
	private ImageButton cat;
	private ImageButton spa;
	private ImageButton eng;
	
	private Button findPlayers;
	private Button random;
	private Button play;
	
	private EditText word;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newgame);
		
		Bundle b = getIntent().getExtras();
		boolean pvp = b.getBoolean("pvp");
		username = b.getString("username");
		
		numPlayers = 0;
		
		lang = new boolean[3];
		lang[0] = true;
		lang[1] = false;
		lang[2] = false;
		
		players = (TextView) findViewById(R.id.txtPlayers);
		
		cat = (ImageButton) findViewById(R.id.imgCatalan);
		spa = (ImageButton) findViewById(R.id.imgSpanish);
		eng = (ImageButton) findViewById(R.id.imgBritish);
		
		findPlayers = (Button) findViewById(R.id.btnFindUsers);
		random = (Button) findViewById(R.id.btnRandom);
		play = (Button) findViewById(R.id.btnPlay);
		
		word = (EditText) findViewById(R.id.editWord);
		
		if (!pvp) {
			findPlayers.setVisibility(Button.INVISIBLE);
			random.setVisibility(Button.INVISIBLE);
			players.setVisibility(TextView.INVISIBLE);
		}
		
		cat.setImageResource(R.drawable.catalan_ok);
		
		cat.setOnClickListener(this);
		spa.setOnClickListener(this);
		eng.setOnClickListener(this);
		
		findPlayers.setOnClickListener(this);
		random.setOnClickListener(this);
		play.setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.imgCatalan:
			lang[CAT] = !lang[CAT];
			if (lang[CAT]) cat.setImageResource(R.drawable.catalan_ok);
			else cat.setImageResource(R.drawable.catalan);
			break;
		case R.id.imgSpanish:
			lang[SPA] = !lang[SPA];
			if (lang[SPA]) spa.setImageResource(R.drawable.spanish_ok);
			else spa.setImageResource(R.drawable.spanish);
			break;
		case R.id.imgBritish:
			lang[ENG] = !lang[ENG];
			if (lang[ENG]) eng.setImageResource(R.drawable.british_ok);
			else eng.setImageResource(R.drawable.british);
			break;
		case R.id.btnFindUsers:
			// This will start activity for result
			Intent i = new Intent(this, FindPlayersActivity.class);
			i.putExtra("username", username);
			startActivityForResult(i, reqCode);
			break;
		case R.id.btnRandom:
			new RandomUsersTask().execute();
			break;
		case R.id.btnPlay:
			String[] info = new String[numPlayers + 2];
			if (numPlayers < 1 || playerNames == null) {
				Popup popup = new Popup(NewGameActivity.this);
				popup.show("New game", "You must choose at least one player to play against");
				return;
			}
			
			else if (word.getText().toString().equals("")) {
				Popup popup = new Popup(NewGameActivity.this);
				popup.show("New game", "You must type a word to be guessed");
				return;
			}
			
			else {
				info[0] = word.getText().toString(); // This is the word chosen
				info[1] = username;
				for (int j = 0; j < numPlayers; j++) {
					info[j + 2] = playerNames[j];
				}
				
				new StartGameTask().execute(info);
			}
			
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			Bundle b = data.getExtras();
			numPlayers = b.getInt("numPlayers");
			playerNames = new String[numPlayers];
			String pla = "";
			for (int j = 1; j <= numPlayers; j++) {
				pla = pla + b.getString("player" + j) + "\n";
				playerNames[j - 1] = b.getString("player" + j);
			}
			
			players.setText(pla);
		}
	}
	
	// Asynchronous tasks
	private class StartGameTask extends AsyncTask<String, Void, Boolean> {
		ProgressDialog pDialog;
		
		@Override
		protected Boolean doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			publishProgress();
			String packet = "newGame";
			int len = arg0.length;
			
			for (int i = 0; i < len; i++) {
				packet = packet + "#" + arg0[i];
			}
			
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, packet);
				String[] response = DataSocket.readLine(input).split("#");
				if (response[0].equals("newGame") && response[1].equals("OK")) {
					return true;
				}
				
				else {
					return false;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(NewGameActivity.this, "", "Creating game. Please wait...", true);
		}
		
		protected void onPostExecute(Boolean result) {
			pDialog.dismiss();
			
			if (result) {
				// Game correctly created so move back to the main menu activity
				Intent i = new Intent(NewGameActivity.this, MainMenuActivity.class);
				i.putExtra("username", username);
				startActivity(i);
			}
			
			else {
				// Error at creating game so pop up an alert
				Popup popup = new Popup(NewGameActivity.this);
				popup.show("New game", "An unexpected error occured while creating the game. Please try again later");
			}
		}
	}
	
	private class RandomUsersTask extends AsyncTask<Void, Void, String> {
		ProgressDialog pDialog;
		
		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			publishProgress();
			
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "randomUsers");
				String response = DataSocket.readLine(input);
				
				return response;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(NewGameActivity.this, "", "Searching for users. Please wait...", true);
		}
		
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			
			if (result != null) {
				String[] cmd = result.split("#");
				int num = Integer.parseInt(cmd[1]);
				String text = "";
				
				playerNames = new String[num];
				numPlayers = num;
				
				for (int i = 0; i < num; i++) {
					text = text + cmd[2 + i] + "\n";
					playerNames[i] = cmd[2 + i];
				}
				
				players.setText(text);
			}
		}
	}
}

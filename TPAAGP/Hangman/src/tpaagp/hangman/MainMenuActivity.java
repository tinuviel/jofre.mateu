package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Button;

public class MainMenuActivity extends Activity implements OnClickListener {
	private String username;
	private int[] gameIds;
	private String[] ownerNames;
	private Socket s;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);
		
		Bundle b = getIntent().getExtras();
		username = b.getString("username");
		
		gameIds = null;
		ownerNames = null;
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
		
		new ShowGamesTask().execute();
	}
	
	public void onResume() {
		super.onResume();
		new ShowGamesTask().execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu1, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		Intent i;
	    switch (item.getItemId()) {
	    case R.id.newGame:
	    	GameMode ng = new GameMode(MainMenuActivity.this);
	        ng.show("Choose a game mode");
	        return true;
	    case R.id.stats:
	    	i = new Intent(MainMenuActivity.this, StatisticsActivity.class);
	    	i.putExtra("username", username);
	    	startActivity(i);
	        return true;
	    case R.id.refresh:
	    	new ShowGamesTask().execute();
	    	return true;
	    case R.id.settings:
	    	i = new Intent(MainMenuActivity.this, settingsActivity.class);
	    	i.putExtra("username", username);
	    	startActivity(i);
	        return true;
	    case R.id.logout:
	    	new LogoutTask().execute();
	    	Intent i2 = new Intent(MainMenuActivity.this, HangmanActivity.class);
	    	startActivity(i2);
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (!username.equals(ownerNames[arg0.getId()])) {
			Intent i = new Intent(MainMenuActivity.this, GameActivity.class);
			i.putExtra("username", username);
			i.putExtra("game_id", gameIds[arg0.getId()]);
			startActivity(i);
		}
		
		else {
			Intent i = new Intent(MainMenuActivity.this, GameOwnerActivity.class);
			i.putExtra("username", username);
			i.putExtra("game_id", gameIds[arg0.getId()]);
			startActivity(i);
		}
	}
	
	// This class must be here in order to allow the new activity start right after game mode
	// selection, otherwise there are some synchronization problems
	private class GameMode extends Dialog implements OnClickListener {
		private Button pvp;
		private Button pvai;

		public GameMode(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
			
			setContentView(R.layout.gamemode);
			
			pvp = (Button) findViewById(R.id.btnPVP);
			pvai = (Button) findViewById(R.id.btnPVAI);
			
			pvp.setOnClickListener(this);
			pvai.setOnClickListener(this);
			
			pvai.setEnabled(false);
		}

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub	
			Intent i = new Intent(MainMenuActivity.this, NewGameActivity.class);
			i.putExtra("username", username);
			
			switch (arg0.getId()) {
			case R.id.btnPVP:
				i.putExtra("pvp", true);
				break;
			case R.id.btnPVAI:
				i.putExtra("pvp", false);
				break;
			default: 
				i.putExtra("pvp", false);
				break;
			}
			
			this.dismiss();
			startActivity(i);
		}
		
		public void show(String title) {
			setTitle(title);
			show();
		}
	}
	
	// Asynchronous tasks
	private class ShowGamesTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub			
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "showAllGames");
				String response = DataSocket.readLine(input);
				
				return response;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String result) {	
			//showAllGames#id1/admin1/curPlayer1/wordState1#id2/admin2
			if (result != null) {
				String[] games = result.split("#");
				int len = games.length;
				
				gameIds = new int[len - 1];
				ownerNames = new String[len - 1];
				
				LinearLayout rl_your = (LinearLayout) findViewById(R.id.layYourTurn);
				LinearLayout rl_their = (LinearLayout) findViewById(R.id.layTheirTurn);
				
				rl_your.removeAllViews();
				rl_their.removeAllViews();
				
				for (int i = 1; i < len; i++) {
					String[] info = games[i].split("/");
					gameIds[i - 1] = Integer.parseInt(info[0]);
					ownerNames[i - 1] = info[1];
					
					// A new button must be configured and added to its layout
					Button btn = new Button(MainMenuActivity.this);
					btn.setId(i - 1);
					btn.setBackgroundResource(R.drawable.customnotebutton);
					btn.setWidth(175);
					btn.setHeight(55);
					btn.setGravity(Gravity.CENTER);
					
					// Also its set on click function must be set
					btn.setOnClickListener(MainMenuActivity.this);
					btn.setTextSize(12);
					btn.setHeight(70);
					btn.setWidth(275);
					
					if (info[2].equals(username)) {
						btn.setText("Owner: " + info[1] + "\nWord: " + info[3] + "\n");
						rl_your.addView(btn); 
					}
					
					else {
						btn.setText("Owner: " + info[1] + "\nCurrent player: " + info[2] +
									"\nWord: " + info[3] + "\n");
						
						rl_their.addView(btn);
					}
				}
			}
		}
	}
	
	private class LogoutTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub			
			try {
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				DataSocket.writeLine(output, "close");
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
	}
}

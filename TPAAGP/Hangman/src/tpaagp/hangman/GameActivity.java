package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class GameActivity extends Activity implements OnClickListener {
	private String username;
	private int gameId;
	
	private Socket s;
	
	private Button submitLetter;
	private Button submitWord;
	private EditText letter;
	private EditText word;
	private DrawView draw;
	
	private TextView wordState;
	private TextView listLetters;
	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		
		Bundle b = getIntent().getExtras();
		username = b.getString("username");
		gameId = b.getInt("game_id");
		
		submitLetter = (Button) findViewById(R.id.btnSubmitLetter);
		submitWord = (Button) findViewById(R.id.btnSubmitWord);
		
		letter = (EditText) findViewById(R.id.editLetterGuessed);
		word = (EditText) findViewById(R.id.editWordGuessed);
		
		wordState = (TextView) findViewById(R.id.txtWordHide);
		listLetters = (TextView) findViewById(R.id.txtSaidLetters);
		
		draw = (DrawView) findViewById(R.id.drawView2);

		draw.setTouchEnabled(false);
		submitLetter.setOnClickListener(this);
		submitWord.setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
		
		new ShowGameTask().execute();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSubmitLetter:
			String chosen = letter.getText().toString();
			new SubmitLetterTask().execute(chosen);
			break;
		case R.id.btnSubmitWord:
			String word_chosen = word.getText().toString();
			new SubmitWordTask().execute(word_chosen);
			submitWord.setEnabled(false);
			break;
		}
	}
	
	private class SubmitLetterTask extends AsyncTask<String, Void, Boolean> {
		ProgressDialog pDialog;
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			char let = params[0].charAt(0);

			publishProgress();			
			//String address = ServerIP.address;
			//int port = ServerIP.port;
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				System.out.println("Abans del PlayerServer");
				DataSocket.writeLine(output, "submitLetter#" + let + "#" + gameId);
				String[] response = DataSocket.readLine(input).split("#");
				System.out.println("He tornat del PlayerServer");
				if (response[1].equals("OK")) {
					return true;
				}
				
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(GameActivity.this, "", "Submitting the letter. Please wait...", true);
		}
		
		protected void onPostExecute(Boolean b) {
			pDialog.dismiss();			
			if (!b) {
				Popup popup = new Popup(GameActivity.this);
				popup.show("Game", "An error occured when submitting the letter. Please try again");
				
			}
			
			else {
				Popup popup = new Popup(GameActivity.this);
				popup.show("Game", "Letter submitted succesfully");
				submitLetter.setEnabled(false);
			}
		}
	}
	
	private class SubmitWordTask extends AsyncTask<String, Void, Boolean> {
		ProgressDialog pDialog;
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			String wordSent = params[0];

			publishProgress();			

			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "submitWord#" + wordSent + "#" + gameId);
				String[] response = DataSocket.readLine(input).split("#");
				
				if (response[1].equals("OK")) {
					return true;
				}
				
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(GameActivity.this, "", "Submitting the word. Please wait...", true);
		}
		
		protected void onPostExecute(Boolean b) {
			pDialog.dismiss();			
			if (!b) {
				Popup popup = new Popup(GameActivity.this);
				popup.show("Game", "The word you submitted is not correct");
			}
			
			else {
				Popup popup = new Popup(GameActivity.this);
				popup.show("Game", "That's it! You win!");
			}
		}
	}
	
	private class ShowGameTask extends AsyncTask<Void, Void, String> {	
		// showGame#game_id
		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "showGame#" + gameId);
				String response = DataSocket.readLine(input);
				
				return response;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}

		protected void onPostExecute(String result) {
			// showGame#WORD#LETTERS/letter1/letter2/...#curPlayer#draws...
			if (result != null) {
				String[] cmd = result.split("#");
				
				String sWord = cmd[1];
				String[] sLetters = cmd[2].split("/");
				String curPlayer = cmd[3];
				
				wordState.setText(sWord);
				
				int len = sLetters.length;
				if (len > 1) {
					String list = "Letters said: " + sLetters[1];
					for (int i = 2; i < len; i++) {
						list = list + ", " + sLetters[i];
					}
					
					listLetters.setText(list);
				}
				
				else listLetters.setText("");
				
				if (!curPlayer.equals(username)) {
					letter.setEnabled(false);
					submitLetter.setEnabled(false);
				}
				
				// Draws
				ArrayList<ArrayList<Point>> lines = new  ArrayList<ArrayList<Point>>();
				
				len = cmd.length;
				for(int i = 4; i < len; i++) {
					System.out.println(cmd[i]);
					String[] line = cmd[i].split("/");
					ArrayList<Point> points = new ArrayList<Point>();
					for(int j = 0; j < line.length; j++) {
						points.add(new Point(line[j]));
					}
					lines.add(points);
				}
				draw.setLines(lines);
			}
		}
	}
}

		

	

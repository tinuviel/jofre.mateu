package tpaagp.hangman;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DrawView extends View implements OnTouchListener {
    ArrayList<Point> points = new ArrayList<Point>();
    ArrayList<ArrayList<Point>> pending = new ArrayList<ArrayList<Point>>();
    ArrayList<ArrayList<Point>> lines = new ArrayList<ArrayList<Point>>();
    Paint paint = new Paint();
    Boolean touch = true;
    Boolean draw = true;

    public DrawView(Context context) {
        super(context);
        setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }
    
    public DrawView(Context context, AttributeSet attrs) {  	 
    	super( context, attrs );
    	setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }
    
    public DrawView(Context context, AttributeSet attrs, int defStyle) {    	 
    	super( context, attrs, defStyle );
    	setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Canvas canvas) {
    	if(draw) {
	    	for(int t = 0; t < lines.size(); t++) {
	    		ArrayList<Point> pnts = lines.get(t);
		    	for(int i = 0; i < pnts.size() - 1; i++){
		        	canvas.drawLine(pnts.get(i).x, pnts.get(i).y, pnts.get(i+1).x, pnts.get(i+1).y, paint);
		        }
	    	}
	    	
	    	for(int i = 0; i < points.size() - 1; i++){
	    		canvas.drawLine(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y, paint);
	        }
    	}
    }

    public boolean onTouch(View view, MotionEvent event) {   
    	if(touch) {
	    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    		points = new ArrayList<Point>();
	    	}
	    	else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	    		Point point = new Point();
	            point.x = event.getX();
	            point.y = event.getY();
	            points.add(point);
	            invalidate();
	    	}
	    	else if (event.getAction() == MotionEvent.ACTION_UP) {
	    		lines.add(points);
	    		pending.add(points);
	    	}
    	}
        return true;
    }
    
    public boolean getTouchEnabled() {
    	return touch;
    }
    
    public void setTouchEnabled(Boolean e) {
    	touch = e;
    }
    
    public boolean getDrawEnabled() {
    	return draw;
    }
    
    public void setDrawEnabled(Boolean e) {
    	draw = e;
    }
    
    public ArrayList<ArrayList<Point>> getLines() {
    	return lines;
    }
    
    public ArrayList<ArrayList<Point>> getPendingLines() {
    	ArrayList<ArrayList<Point>> aux = new ArrayList<ArrayList<Point>>(pending);
    	pending.clear();
    	return aux;
    }
    
    public void setLines(ArrayList<ArrayList<Point>> draw) {
    	lines = draw;
    }
    
    public void setPendingLines(ArrayList<ArrayList<Point>> draw) {
    	pending = draw;
    	lines = draw;
    }
    
    public void setColor(int color) {
    	paint.setColor(color);
    }
    
    public int getColor() {
    	return paint.getColor();
    }
}

class Point {
    float x, y;
    
    public Point() {
    	
    }

    public Point(String p) {
    	String[] pos = p.split(",");
    	x = Float.valueOf(pos[0].trim()).floatValue();
    	y = Float.valueOf(pos[1].trim()).floatValue();
    }
    
    @Override
    public String toString() {
        return x + "," + y;
    }
    
}

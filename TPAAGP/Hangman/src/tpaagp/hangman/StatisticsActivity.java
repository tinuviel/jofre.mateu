package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class StatisticsActivity extends Activity {
	private Socket s;
	
	private TextView statsUser;
	private TextView statsScore;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stats);
		
		statsUser = (TextView) findViewById(R.id.txtStatsUsername);
		statsScore = (TextView) findViewById(R.id.txtStatsScore);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;
		
		new ShowStatsTask().execute();
	}
	
	private class ShowStatsTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub			
			try {
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "showStats");
				String response = DataSocket.readLine(input);
				
				return response;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String result) {	
			if (result != null) {
				String[] info = result.split("#");
				int len = info.length;
				
				String textUser = "";
				String textScore = "";
				
				for (int i = 1; i < len; i++) {
					String[] info2 = info[i].split("/");
					textUser = textUser + info2[0] + "\n"; 
					textScore = textScore + info2[1] + "\n"; 
				}
				
				statsUser.setText(textUser);
				statsScore.setText(textScore);
			}
		}
	}
}

package tpaagp.hangman;


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.ToggleButton;

public class settingsActivity extends Activity implements OnClickListener {
	private String username;
	private Socket s;
	private TextView user, learn, changePass;
	private ToggleButton btn;
	private AudioManager am;

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		
		
		am = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		
		Bundle b = getIntent().getExtras();
		username = b.getString("username");
		
		user = (TextView)findViewById(R.id.user);
		user.setText(username);
		
		learn = (TextView) findViewById(R.id.help);
		learn.setOnClickListener(this);
		
		btn = (ToggleButton) findViewById(R.id.onoff);
		btn.setOnClickListener(this);
		btn.setChecked(true);
		
		changePass = (TextView) findViewById(R.id.pass);
		changePass.setOnClickListener(this);
		
		// Keep dragging the main socket connection
		SocketApplication sApp = (SocketApplication) getApplication();
		s = sApp.socket;

	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch(v.getId()) {
		case R.id.help:
			i = new Intent (settingsActivity.this, HelpActivity.class);
			startActivity(i);
			break;
		case R.id.pass: 
			i = new Intent (settingsActivity.this, ChangePassword.class);
			i.putExtra("username", username);
			startActivity(i);
			break;
		case R.id.onoff:
			if (btn.isChecked()) {
				am.setStreamMute(AudioManager.STREAM_MUSIC, false);
			}
			else {
				am.setStreamMute(AudioManager.STREAM_MUSIC, true);
			}
		}
	}
	
	
}

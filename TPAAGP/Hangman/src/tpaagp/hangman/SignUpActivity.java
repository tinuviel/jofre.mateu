package tpaagp.hangman;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SignUpActivity extends Activity implements OnClickListener {
	private EditText username;
	private EditText password;
	private EditText age;
	private Spinner country;
	private Button signIn;
	private Socket s;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		
		username = (EditText) findViewById(R.id.editUsernameSignIn);
		password = (EditText) findViewById(R.id.editPasswordSignIn);
		age = (EditText) findViewById(R.id.editAgeSignIn);
		
        username.setHint("Username");
        password.setHint("Password");
        age.setHint("Age");
		
		country = (Spinner) findViewById(R.id.spnCountrySignIn);
		
		
		
	    ArrayAdapter<CharSequence> listCountries = ArrayAdapter.createFromResource(
									    				this, 
									    				R.array.countries, 
									    				android.R.layout.simple_spinner_item);
	    
	    listCountries.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    country.setAdapter(listCountries);
	    
	    signIn = (Button) findViewById(R.id.btnSignIn);
	    signIn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		boolean okFields = checkFields();
		if (okFields) {
			String user = username.getText().toString();
			String pwd = password.getText().toString();
			String ageString = age.getText().toString();
			String cty = (String) country.getSelectedItem();
			
			new SignUpTask().execute(user, pwd, cty, ageString);
		}
	}
	
	// Asynchronous tasks in order to avoid blocking UI-Thread
	private class SignUpTask extends AsyncTask<String, Void, Boolean> {
		ProgressDialog pDialog;
		
		@Override
		protected Boolean doInBackground(String... info) {
			// TODO Auto-generated method stub
			String username = info[0];
			String password = info[1];
			String country = info[2];
			String age = info[3];
			
			try {
				publishProgress();			
				String address = ServerIP.address;
				int port = ServerIP.port;
				s = new Socket(InetAddress.getByName(address), port);
				InputStreamReader input = new InputStreamReader(s.getInputStream());
				OutputStreamWriter output = new OutputStreamWriter(s.getOutputStream());
				
				DataSocket.writeLine(output, "signup#" + username + 
													"#" + password + 
													"#" + country +
													"#" + age);
				
				String[] response = DataSocket.readLine(input).split("#");
				if (response[0].equals("signup") && response[1].equals("OK")) {
					return true;
				}
				
				else {
					return false;
				}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				System.out.println("Unknown Host Exception");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("IO Exception");
				e.printStackTrace();
			} 
			
			return false;
		}
		
		protected void onProgressUpdate(Void... v) {
			pDialog = ProgressDialog.show(SignUpActivity.this, "", "Signing up. Please wait...", true);
		}
		
		protected void onPostExecute(Boolean result) {
			pDialog.dismiss();
			
			if (result) {
				// Login OK so move ahead to the main menu window
				Intent i = new Intent(SignUpActivity.this, MainMenuActivity.class);
				i.putExtra("username", username.getText().toString());
				SocketApplication sApp = (SocketApplication) getApplication();
				sApp.socket = s;
				startActivity(i);
			}
			
			else {
				// Wrong login so pop up an alert
				Popup popup = new Popup(SignUpActivity.this);
				popup.show("Login", "This username is not available. Please choose another one");
				
				try {
					s.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("IO Exception");
					e.printStackTrace();
				}
			}
		}
	}
	
	// Helper methods
	private boolean checkFields() {
		// Usernames must be composed by only numbers and letters,
		// and its length must be from 4 to 10
		String regExpression = "[a-zA-Z0-9]{4,10}";
		String user = username.getText().toString();
		String pwd = password.getText().toString();
		String ageString = age.getText().toString();
		String cty = (String) country.getSelectedItem();
		
		if (!user.matches(regExpression)) {
			Popup wrongUser = new Popup(this);
			wrongUser.show("Sign In", "Username must be composed by only numbers and letters " +
										"and its length cannot be either shorter than 4 " +
										"or larger than 10");
			return false;
		}
		
		else if (!pwd.matches(regExpression)) {
			Popup wrongPwd = new Popup(this);
			wrongPwd.show("Sign In", "Password must be composed by only numbers and letters " +
										"and its length cannot be either shorter than 4 " +
										"or larger than 10");
			return false;
		}
		
		else if (ageString.equals("")) {
			Popup wrongAge = new Popup(this);
			wrongAge.show("Sign In", "Age cannot be empty");
			return false;
		}
		
		else if (cty.equals("Select your country")) {
			Popup wrongCountry = new Popup(this);
			wrongCountry.show("Sign in", "You must select a valid country");
			return false;
		}
		
		else {
			int ageInt = Integer.parseInt(ageString);
			if (ageInt >= 150) {
				Popup wrongAge = new Popup(this);
				wrongAge.show("Sign In", "C'mon... No one trusts you are older than 150");
				return false;
			}
		}
		
		return true;
	}
}

#ifndef __MYMODUL2_H__
#define __MYMODUL2_H__

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm-i386/uaccess.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/cdev.h>

#define COUNT 1

int mod_open(struct inode *i, struct file *f);
int mod_release (struct inode *i, struct file *f);
int mod_ioctl(struct inode *i, struct file *f, unsigned int arg1, unsigned long arg2);
ssize_t mod_read(struct file *f, char __user *buffer, size_t s, loff_t *off);
static int __init Mymodule_init(void);
static void __exit Mymodule_exit(void);

#endif

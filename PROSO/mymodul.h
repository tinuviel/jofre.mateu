#ifndef __MYMODUL_H__
#define __MYMODUL_H__

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>

#define POS_WRITE 4
#define POS_CLOSE 6
#define POS_LSEEK 19
#define POS_OPEN 45
#define POS_CLONE 312
#define MY_WRITE 0
#define MY_CLOSE 1
#define MY_LSEEK 2
#define MY_OPEN 3
#define MY_CLONE 4
#define NUM_CRIDES 5

#define proso_rdtsc(low, high) \
__asm__ __volatile__("rdtsc" : "=a" (low), "=d" (high))

static inline unsigned long long proso_get_cycles (void) {
  unsigned long eax, edx;
  proso_rdtsc(eax, edx);
  return ((unsigned long long) edx << 32) + eax;
}

struct t_info{
  int n_ent;
  int n_sort_ok;
  int n_sort_err;
  unsigned long long durada_total;
};

struct estadistiques{
  int pid;
  struct t_info info_crida[NUM_CRIDES];
};

struct my_thread_info {
  struct thread_info info;
  struct estadistiques est;
};

extern void *sys_call_table[];
void *crides_old[NUM_CRIDES];

int inicialitzat(struct thread_info *proces);
void inicialitzar(struct thread_info *p);
void actualitzar(struct thread_info *p, int crida, unsigned long long temps, int ret);
void imprimir_crida(struct t_info * info);
void imprimir_est(struct my_thread_info *proces);
ssize_t my_sys_write(unsigned int fd, const char __user * buf,size_t count);
long my_sys_close(unsigned int fd);
off_t my_sys_lseek(unsigned int fd, off_t offset, unsigned int origin);
long my_sys_open(const char __user * filename, int flags, int mode);
int my_sys_clone(struct pt_regs regs);
static int __init Mymodule_init(void);
static void __exit Mymodule_exit(void);
void activar_syscall(int crida);
void desactivar_syscall(int crida);
void mod1_setPID(int p);
struct t_info informacio(struct thread_info *p, int crida);


#endif

#ifndef UTILS_H
#define UTILS_H 

#define O_EXCL 0x8
#define O_CREAT 0x4
#define O_RDWR 0x3
#define O_WRONLY 0x2
#define O_RDONLY 0x1

void copy_data(void *start, void *dest, int size);
int copy_from_user(void *start, void *dest, int size);
int copy_to_user(void *start, void *dest, int size);
void itoa (int num, char *buffer);
int leng(int num);
int comprova_fd(int fd, int operacio);
int access_ok(int type, const void * addr, unsigned long size);
void copy_string(char *buffer1,char *buffer2);
int compare_string(char *buffer1,char *buffer2);
int tamany_string(char *buffer1);
int comprova_flags(int f1, int f2);
int copia_segura(char *buffer1,char *buffer2);
#endif

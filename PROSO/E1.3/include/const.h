#ifndef CONST_H__
#define CONST_H__ 

#define NR_TASKS 10
#define NR_CH 10
#define NR_SEM 10
#define KERNEL_STACK_SIZE 1024
#define QUANTUM 2

#endif /* CONST_H__*/

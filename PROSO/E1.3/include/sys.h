#ifndef __SYS_H__
#define __SYS_H__
#define EAXX	 (KERNEL_STACK_SIZE-10)
#define MAX_BUFFER 512

int sys_write(int fd,char *buffer, int size);
int sys_getpid();
int sys_fork();
void sys_exit();
int sys_ni_syscall();
int sys_open(const char *path, int flags);
int sys_read(int fd,char *buffer, int size);
int sys_close(int fd);
int sys_dup(int fd);
int sys_unlink(const char *path);

#endif  /* __LIBC_H__ */

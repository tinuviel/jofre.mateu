#ifndef STATS_H
#define STATS_H 

/* Structure used by 'get_stats' function */
struct stats
{
	unsigned int total_tics;
	unsigned int total_trans; /* Number of times the process has got the CPU: READY->RUN transitions */
  unsigned int remaining_tics;
};
#endif /* !STATS_H */

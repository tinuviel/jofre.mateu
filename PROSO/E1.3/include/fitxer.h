#ifndef FITXER_H__
#define FITXER_H__ 

#include <const.h>

#define NR_FTX (NR_TASKS*NR_CH)
#define FILE_NAME_SIZE 10
#define MAX_DIR	10

struct fo{
  int numref;
  int prw;
  int rw;
  struct inode* tin;
};

struct inode{
  int numref;
  int size;//nomes fitxers
  int rw;
  int ini;//nomes fitxers
  struct file_operations* op;
  struct dir *td;
};

struct dir{
  char nom[FILE_NAME_SIZE];
  struct inode* in;
};

extern struct fo tfo[NR_FTX];
extern struct inode ti[NR_FTX];
extern struct dir direct[MAX_DIR];

int entrada_tfo_lliure();
int entrada_dir_lliure();
int entrada_inode_lliure();
int entrada_dir_existeix(char *path);
int entrada_inode_existent(char *path);

#endif /* DEVICES_H__*/

/* 
 * libc.h - macros per fer els traps amb diferents arguments
 *          definició de les crides a sistema
 */
 
#ifndef __LIBC_H__
#define __LIBC_H__

#include <stats.h>

#define O_EXCL 0x8
#define O_CREAT 0x4
#define O_RDWR 0x3
#define O_WRONLY 0x2
#define O_RDONLY 0x1

int write(int fd,char *buffer,int size);
int getpid();
int fork();
void exit();
void perror();
int nice(int quantum);
int sem_init(int n_sem, unsigned int value);
int sem_wait(int n_sem);
int sem_signal(int n_sem);
int sem_destroy(int n_sem);
int get_stats(int pid, struct stats *st);
int open(const char *path, int flags);
int read(int fd,char *buffer,int size);
int dup(int fd);
int close(int fd);
int unlink(const char *path);

#endif  /* __LIBC_H__ */

#ifndef FAT_H__
#define  FAT_H__ 

#define MAX_BLOCKS	100
#define TAM_BLOCKS	256
#define EOF -1

struct block {
  char mem[TAM_BLOCKS];
};

extern struct block HD[MAX_BLOCKS];

extern int fat[MAX_BLOCKS];

extern int lliure;

int bloc_lliure_inicial();

void contingut_fat();
void llista_lliures();
void allibera_blocs(int ini);

#endif /* FAT_H__*/

/* 
 * sched.h - Estructures i macros pel tractament de processos
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <list.h>
#include <mm_address.h>
#include <stats.h>
#include <fat.h>
#include <fitxer.h>
#include <utils.h>
#include <devices.h>
#include <const.h>

struct ch{
  int fd;
  struct fo *tf;
};

extern int qr;

extern struct list_head runqueue, freequeue;

struct sem_struct {
  struct list_head node;
  int prop;
  int n_sem;
  struct list_head ini;
  int cont;  
};

extern struct sem_struct semlist[NR_SEM];

struct task_struct {
  int pid;
  int quantum;
  struct list_head node;
  unsigned int pags[NUM_PAG_DATA]; //aixo conte punters que apunten a les pagines associades al proces
  struct stats status;
  // SUBSTITUIU EL CAMP dummy PER QUE CONSIDEREU QUE HA DE TENIR EL TASK_STRUCT
  struct list_head sem;
  struct ch tch[NR_CH];
  int ch_pend;
  char *buffer;
};

union task_union {
  struct task_struct task;
  unsigned long stack[KERNEL_STACK_SIZE];    /* pila de sistema, per procés */
};

struct protected_task_struct {
  unsigned long task_protect[KERNEL_STACK_SIZE];  /* This field protects the different task_structs, so any acces to this field will generate a PAGE FAULT exeption */
  union task_union t;
};

extern struct protected_task_struct task[NR_TASKS];

#define KERNEL_ESP       (DWord) &task[0].t.stack[KERNEL_STACK_SIZE]

/* Inicialitza les dades del proces inicial */
void init_task0(void);
struct task_struct* current();
struct task_struct *list_head_to_task_struct (struct list_head *l);

void task_switch(union task_union *t);
inline void canvillista(struct list_head *l1, struct list_head *l2);
inline void bloquejarproces(struct list_head *l1, struct list_head *l2);
inline void round_robin();
int entrada_canal_lliure();

#endif  /* __SCHED_H__ */

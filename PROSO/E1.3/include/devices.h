#ifndef DEVICES_H__
#define  DEVICES_H__

#define DEV 3
#define TECLAT	0
#define PANTALLA  1
#define FITXER	2

#include <fitxer.h>
#include <fat.h>
#include <sched.h>

struct file_operations{
  int (*open)(const char *path, int flags); //retorn (*open)(parametres)
  int (*read)(struct fo *fito, char *buffer, int size);
  int (*close)();
  int (*write)(struct fo *fito, char *buffer, int size);
};

extern struct file_operations keyboard_op;
extern struct file_operations display_op;
extern struct file_operations file_op;

int sys_write_console(struct fo *fito, char *buffer,int size);
int sys_write_file(struct fo *fito, char *buffer,int size);

#endif /* DEVICES_H__*/

/* 
 * system.h - Capçalera del mòdul principal del sistema operatiu
 */

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include <types.h>


extern TSS         tss;
extern Descriptor* gdt;

inline void ini_tfo();
inline void ini_ti();
inline void ini_sem();
inline void ini_dir();
inline void initZeOSFat();

#endif  /* __SYSTEM_H__ */

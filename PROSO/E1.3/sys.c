/* 
 * sys.c - Syscalls implementation
 */
 
#include <devices.h>
#include <errno.h>
#include <utils.h>
#include <types.h>
#include <sched.h>
#include <list.h>
#include <mm.h>
#include <list.h>
#include <io.h>
#include <fitxer.h>
#include <sys.h>


#define TOP_PCB(punter)	((unsigned long)punter & 0xFFFFF000)

int PID = 1;

int sys_write(int fd,char *buffer, int size)
{
  char buf[MAX_BUFFER];
  int mida = size;
  int res;
  if((access_ok(O_RDONLY, buffer, size)) == 0) return -EFAULT;
  if(comprova_fd(fd, O_WRONLY) != 0) return comprova_fd(fd, O_WRONLY); 
  else if(size < 0) return -EINVAL;
  else if(buffer != NULL) {
    while (mida > 0) {
      if(mida > MAX_BUFFER) {
        if(copy_from_user(buffer, buf, MAX_BUFFER) == -1) return -EFAULT;
        res = current()->tch[fd].tf->tin->op->write(current()->tch[fd].tf, buf, MAX_BUFFER);
        if(res < 0) return res;
        buffer += MAX_BUFFER;
        mida -= MAX_BUFFER;
      }
      else {
        if(copy_from_user(buffer, buf, mida) == -1) return -EFAULT;
        res = current()->tch[fd].tf->tin->op->write(current()->tch[fd].tf, buf, mida);
        if(res < 0) return res;
        mida = 0;
      }
    }
    return size; 
  }
  return -EFAULT;
}

int sys_getpid()
{
  struct task_struct *task = current();
  return task->pid;
}

int sys_fork()
{
  struct task_struct *fill;
  struct list_head *nou;
  int fisic;
  int cont = 0;
  int i;
  if(list_empty(&freequeue)) return -ENOMEM;
  else {
    	nou = list_first(&freequeue);
    	list_del(nou);
    	fill = list_head_to_task_struct(nou);
    	copy_data(current(), fill, PAGE_SIZE);
    	list_add_tail(nou, &runqueue);
    	fisic = alloc_frame();
    	while(cont < NUM_PAG_DATA && fisic != -1) {
      	  set_ss_pag(PAG_LOG_FINAL_DATA+cont, fisic);
          fill->pags[cont] = fisic;
      	  copy_data((void *)((PAG_LOG_INIT_DATA_P0+cont)<<12), (void *)((PAG_LOG_FINAL_DATA+cont)<<12), PAGE_SIZE);
          del_ss_pag(PAG_LOG_FINAL_DATA+cont);
          ++cont;
          if(cont < NUM_PAG_DATA) fisic = alloc_frame();
        }
    //Fallo per falta de físiques
    if (cont < NUM_PAG_DATA || fisic == -1) {
        while(cont >= 0) {
          free_frame(fill->pags[cont]); 
          --cont;
        }
        list_del(nou);
        list_add_tail(nou, &freequeue);
        set_cr3();
        return -EAGAIN;
    }
    
    set_cr3();
    INIT_LIST_HEAD(&fill->sem);
    union task_union *t = (union task_union *)fill; 
    t->stack[EAXX] = 0;
    fill->pid = PID++;
    fill->status.total_tics = 0;
    fill->status.total_trans = 0;
    fill->status.remaining_tics = fill->quantum;
    for(i = 0; i < NR_CH; ++i) {
      if(fill->tch[i].tf != NULL) fill->tch[i].tf->numref++;
    }
    return fill->pid;
  }
}

int sys_sem_destroy(int n_sem) {
  struct task_struct *act;
  struct list_head *pos;
  union task_union *t;
  if(n_sem >= NR_SEM || n_sem < 0) return -EINVAL;
  if(semlist[n_sem].prop == -1) return -EINVAL;
  if(semlist[n_sem].prop != sys_getpid()) return -EPERM;
  list_for_each(pos, &semlist[n_sem].ini) {
    act = list_head_to_task_struct(pos);
    t = (union task_union *)act;
    t->stack[EAXX] = -1;
  }
  while(!(list_empty(&semlist[n_sem].ini))) canvillista(&semlist[n_sem].ini, &runqueue);
  semlist[n_sem].prop = -1;
  list_del(&semlist[n_sem].node);
  return 0;
}

int sys_nice(int quantum) {
  int valant;
  struct task_struct *task = current();
  if(quantum <= 0) return -EINVAL;
  valant = task->quantum;
  task->quantum = quantum;
  return valant;
}

int sys_sem_init(int n_sem, unsigned int value) {
  if(n_sem >= NR_SEM || n_sem < 0) return -EINVAL;
  if(semlist[n_sem].prop != -1) return -EBUSY;
  semlist[n_sem].prop = sys_getpid();
  semlist[n_sem].cont = value;
  INIT_LIST_HEAD(&semlist[n_sem].ini); 
  list_add(&semlist[n_sem].node, &(current()->sem));
  return 0;
}

int sys_sem_wait(int n_sem) {
  struct task_struct *seg;
  struct list_head *act;
  if(sys_getpid() == 0) return -EPERM;
  if(n_sem >= NR_SEM || n_sem < 0) return -EINVAL;
  if(semlist[n_sem].prop == -1) return -EINVAL;
  if(semlist[n_sem].cont > 0) --semlist[n_sem].cont;
  else if(semlist[n_sem].cont <= 0) {
    current()->status.total_tics += current()->quantum - qr;
    canvillista(&runqueue,&semlist[n_sem].ini);
    act = list_first(&runqueue);
    seg = list_head_to_task_struct(act);
    task_switch((union task_union *) seg);
    return -1;
  }
  return 0;
}

int sys_sem_signal(int n_sem) {
  if(n_sem >= NR_SEM || n_sem < 0) return -EINVAL;
  if(semlist[n_sem].prop == -1) return -EINVAL; 
  if(list_empty(&semlist[n_sem].ini)) ++semlist[n_sem].cont;
  else {
    union task_union *primer = (union task_union *)list_head_to_task_struct(list_first(&semlist[n_sem].ini));
    primer->stack[EAXX] = 0;
    canvillista(&semlist[n_sem].ini,&runqueue);
  }
  return 0;
}

int sys_get_stats(int pid, struct stats *st) {
  int i;
  struct list_head *pos;
  struct task_struct *act;
  if(pid < 0) return -EINVAL;
  if(st == NULL) return -EFAULT;
  if((access_ok(O_WRONLY, st, sizeof(struct stats))) == 0) return -EFAULT;
  if(pid == current()->pid) {
    struct stats estat = current()->status;
    estat.total_tics += current()->quantum - qr;
    estat.remaining_tics = qr;
    if(copy_to_user(&estat, st, sizeof(struct stats)) == -1) return -EFAULT;
    return 0;
  }
  list_for_each(pos, &runqueue) {
    act = list_head_to_task_struct(pos);
    if(act->pid == pid) {
      if(copy_to_user(&(act->status), st, sizeof(struct stats)) == -1) return -EFAULT;
      return 0;
    }
  }
  for(i = 0; i < NR_SEM ; ++i) {
    if(semlist[i].prop != -1) {
      list_for_each(pos, &semlist[i].ini) {
        act = list_head_to_task_struct(pos);
        if(act->pid == pid) {
          if(copy_to_user(&(act->status), st, sizeof(struct stats)) == -1) return -EFAULT;
          return 0;
        }
      }
    }
  }
  return -ESRCH;
}

int sys_open(const char *path, int flags) {
  char path2[FILE_NAME_SIZE];
  int d, can, fitx, ind, esta_creat, copia;
  copia = copia_segura((char*)path, path2);
  if(copia < 0) return copia;
  if(flags > 15 || flags <= 0) return -EINVAL; //Flags incorrectes
  esta_creat = entrada_dir_existeix(path2);
  if(flags & O_CREAT) {
    if(esta_creat != -1 && (flags & O_EXCL)) return -EEXIST;
    else if((flags & O_RDWR) == 0) return -EINVAL;
    else if(esta_creat == -1) { 
      //crear fitxer
      d = entrada_dir_lliure();
      ind = entrada_inode_lliure();
      if(d == -1 || ind == -1) return -ENOSPC; //Falta d'entrades
      copy_string(path2,direct[d].nom);
      direct[d].in = &ti[ind];
      ti[ind].td = &direct[d];
      ti[ind].rw = flags & O_RDWR; //suposarem que funciona
      ti[ind].size = 0;
      ti[ind].numref = 0;
      ti[ind].op = &file_op; //Suposarem que només es pot fer O_CREAT de fitxers
      ti[ind].ini = bloc_lliure_inicial();//Idem
      esta_creat = d;
    }
  }
  if(esta_creat != -1) {
    fitx = entrada_tfo_lliure();
    can = entrada_canal_lliure();
    ind = entrada_inode_existent(path2);
    if (fitx == -1 || can == -1) return -EMFILE; //Espai no disponible
    if(!comprova_flags(flags & O_RDWR, ti[ind].rw)) return -EACCES; //Flags incorrectes

    //Actualitzar inode
    ti[ind].numref++;

    //Inicialitzar entrada tfo
    tfo[fitx].tin = &ti[ind];
    tfo[fitx].rw = flags & O_RDWR;
    tfo[fitx].numref = 1;
    tfo[fitx].prw = 0;

    //Inicialitzar canal
    current()->tch[can].tf = &tfo[fitx];
    return can;    
  }
  return -ENOENT; //No existeix;
}

int sys_read(int fd, char *buffer, int size) {
  if(access_ok(O_WRONLY, buffer, size) == 0) return -EFAULT;
  if(comprova_fd(fd, O_RDONLY) != 0) return comprova_fd(fd, O_RDONLY);
  else if(size < 0) return -EINVAL;
  else if(buffer != NULL) return current()->tch[fd].tf->tin->op->read(current()->tch[fd].tf, buffer, size);
  return -EFAULT;
}

int sys_dup(int fd) {
  int can;
  if(fd >= 10 || fd < 0) return -EBADF;
  can = entrada_canal_lliure();
  if(can == -1) return -EMFILE;
  if(current()->tch[fd].tf == NULL) return -EBADF;
  current()->tch[can].tf = current()->tch[fd].tf; 
  current()->tch[can].tf->numref++;
  return can;
}

int sys_close(int fd) {
  if(fd >= 10 || fd < 0) return -EBADF;
  if(current()->tch[fd].tf == NULL) return -EBADF;
  current()->tch[fd].tf->numref--;
  if(current()->tch[fd].tf->numref == 0) {
    current()->tch[fd].tf->tin->numref--;
    current()->tch[fd].tf->tin = NULL;
  }
  current()->tch[fd].tf = NULL;
  return 0;
}

int sys_unlink(const char *path) {
  char path2[FILE_NAME_SIZE];
  int esta_creat, copia;
  copia = copia_segura((char*)path, path2);
  if(copia < 0) return copia;
  if(path == NULL) return -EFAULT; //Path inexistent
  esta_creat = entrada_dir_existeix(path2);
  if(esta_creat == -1) return -ENOENT;
  if(direct[esta_creat].in->numref != 0) return -EBUSY;
  direct[esta_creat].in->td = NULL;
  allibera_blocs(direct[esta_creat].in->ini);
  direct[esta_creat].in = NULL;
  return 0;
}

void sys_exit() {
    int i = 0;
    struct task_struct *act, *seg;
    struct list_head *nou;
    act = current();
    struct list_head *pos;
    list_for_each(pos, &current()->sem) sys_sem_destroy(((struct sem_struct *)pos)->n_sem);
    if(act->pid != 0) {
      for(i = 0; i < NUM_PAG_DATA; ++i) free_frame(act->pags[i]);
      for(i = 0; i < NR_CH; ++i) sys_close(i);
      nou = list_first(&runqueue);
      list_del(nou);
      list_add_tail(nou, &freequeue);
      nou = list_first(&runqueue);
      seg = list_head_to_task_struct(nou);
      task_switch((union task_union *) seg);
    }
}

int sys_ni_syscall() 
{
  return -ENOSYS;
}

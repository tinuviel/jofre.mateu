#include <fitxer.h>
#include <types.h>
#include <sched.h>


struct fo tfo[NR_FTX];
struct inode ti[NR_FTX];
struct dir direct[MAX_DIR];

int entrada_tfo_lliure() {
  int i = 0;
  for(i = 0; i < NR_FTX; ++i) {
    if(tfo[i].numref == 0) return i;
  }
  return -1;
}

int entrada_dir_lliure() {
  int i = 0;
  for(i = 0; i < MAX_DIR; ++i) {
    if(direct[i].in == NULL) return i;
  }
  return -1;
}

int entrada_dir_existeix(char *path) {
  int i = 0;
  for(i = 0; i < MAX_DIR; ++i) {
    if(direct[i].in != NULL && compare_string(direct[i].nom, path) == 1) return i;
  }
  return -1;
}

int entrada_inode_lliure() {
  int i = 0;
  for(i = 0; i < NR_FTX; ++i) {
    if(ti[i].td == NULL) return i;
  }
  return -1;
}

int entrada_inode_existent(char *path) {
  int i = 0;
  for(i = 0; i < NR_FTX; ++i) {
    if(ti[i].td->nom != NULL && compare_string(path, ti[i].td->nom) != 0) return i;
  }
  return -1;
}

int entrada_tfo_existent(struct inode* in) {
  int i = 0;
  for(i = 0; i < NR_FTX; ++i) {
    if(tfo[i].tin == in) return i;
  }
  return -1;
}

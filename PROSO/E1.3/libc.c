/*
 * libc.c 
 */

#include <libc.h>
#include <errno.h>

int errno;

/* Wrapper of  write system call*/
int write(int fd,char *buffer,int size)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl 12(%%ebp), %%ecx\n"
    "movl 16(%%ebp), %%edx\n"
    "movl $4, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

/* Wrapper of getpid*/
int getpid()
{
  int aux;
  __asm__(
    "movl $20, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    : "=r"(aux)
    :
  );
  return aux;
}
/* Wrapper of fork*/
int fork()
{
  int aux;
  __asm__(
    "movl $2, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

void exit()
{
  __asm__(
    "movl $1, %eax\n"
    "int $0x80\n"
  );
}

int nice(int quantum)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $34, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int sem_init(int n_sem, unsigned int value){
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl 12(%%ebp), %%ecx\n"
    "movl $21, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int sem_wait(int n_sem) {
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $22, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int sem_signal(int n_sem) {
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $23, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int sem_destroy(int n_sem) {
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $24, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int get_stats(int pid, struct stats *st) {
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl 12(%%ebp), %%ecx\n"
    "movl $35, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;}

int open(const char *path, int flags) {
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl 12(%%ebp), %%ecx\n"
    "movl $5, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int read(int fd,char *buffer,int size)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl 12(%%ebp), %%ecx\n"
    "movl 16(%%ebp), %%edx\n"
    "movl $3, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int dup(int fd)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $41, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int close(int fd)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $6, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );
  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

int unlink(const char *path)
{
  int aux;
  __asm__(
    "pushl %%ebx\n"
    "movl 8(%%ebp), %%ebx\n"
    "movl $10, %%eax\n"
    "int $0x80\n"
    "movl %%eax, %0\n"
    "popl %%ebx\n"
    : "=r"(aux)
    :
  );

  if(aux < 0) {
    errno = -aux;
    return -1;
  }
  return aux;
}

void perror()
{ 
  switch(errno) {
    case ENAMETOOLONG:
      write(1,"ENAMETOOLONG: 36-AFile name too long\n",37);
      break;
    case ENOSPC:
      write(1,"ENOSPC: 28-No space left on device\n",35);
      break;
    case EMFILE:
      write(1,"EMFILE: 24-Too many open files\n",31);
      break;
    case EACCES:
      write(1,"EACCES: 13-Permission denied\n",29);
      break;
    case ENOENT:
      write(1,"ENOENT: 2-No such file or directory\n",37);
      break;
    case EBUSY:
      write(1,"EBUSY: 16-Device or resource busy\n",34);
      break;
    case EPERM:
      write(1,"EPERM: 1-Operation not permitted\n",33);
      break;
    case ESRCH:
      write(1,"ESRCH: 3-No such process\n",25);
      break;
    case EBADF:
      write(1,"EBADF: 9-Bad file number\n",25);
      break;
    case EFAULT:
      write(1,"EFAULT: 14-Bad address\n",23);
      break;
    case EINVAL:
      write(1,"EINVAL: 22-Invalid argument\n",28);
      break;
    case ENOSYS:
      write(1,"ENOSYS: 38-Function not implemented\n",36);
      break;
    case EAGAIN:
      write(1,"EAGAIN: 11-Try again\n",21);
      break;
    case ENOMEM:
      write(1,"ENOMEM: 12-Out of memory\n",25);
      break;
    case EEXIST:
      write(1,"EEXIST: 17-File exists\n",23);
    default:
      write(1,"Falta implementar\n",18);
      break;
  }
}

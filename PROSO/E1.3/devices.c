#include <io.h>
#include <devices.h>
#include <errno.h>
#include <sys.h>
#include <interrupt.h>


int sys_write_console(struct fo* fito, char *buffer,int size)
{
  int i;
  for (i = 0; i < size; i++) printc(buffer[i]);
  return size;
 }

int sys_write_file(struct fo* fito, char *buffer,int size) {
  int i,novapos;
  int mida = 0;
  int indx = fito->tin->ini;
  int numblck = fito->prw/TAM_BLOCKS;
  int posblck = fito->prw%TAM_BLOCKS; 
  for (i = 0; i < numblck; i++) {
    indx = fat[indx];
    if(indx == EOF) return -ENOSPC;
  }
  i = 0;
  while(mida < size) {
    while(posblck < TAM_BLOCKS && mida < size) {
      HD[indx].mem[posblck] = buffer[i++];
      ++mida;
      ++posblck;
      fito->prw++;
      if(fito->prw > fito->tin->size) fito->tin->size++;
    }
    if(posblck == TAM_BLOCKS) {
      if(fat[indx] == EOF) {
        novapos = bloc_lliure_inicial();
        if(novapos == EOF) return mida;
        fat[indx] = novapos;
      }
      indx = fat[indx];
      posblck = 0;
    }
  }
  return mida;
}


int sys_read_file_aux(struct fo* fito, char *buffer,int size) {
  int i;
  int mida = 0;
  int indx = fito->tin->ini;
  int numblck = fito->prw/TAM_BLOCKS;
  int posblck = fito->prw%TAM_BLOCKS;
  int mida_fitxer = fito->tin->size;
  for (i = 0; i < numblck; i++) {
    indx = fat[indx];
    if (indx == EOF) return -ENOSPC;
  }
  i = 0;
  while(mida < size) {
    while(posblck < TAM_BLOCKS && mida < size && fito->prw < mida_fitxer) {
      buffer[i++] = HD[indx].mem[posblck];
      ++mida;
      ++posblck;
      fito->prw++;
    }
    if(posblck == TAM_BLOCKS) {
      if(fat[indx] == EOF) {
        return mida;
      }
      indx = fat[indx];
      posblck = 0;
    }
    else if(fito->prw == mida_fitxer) {
      return mida;
    }
  }
  return mida;
}



int sys_read_file(struct fo* fito, char *buffer,int size) {
  int mida = 0;
  int llegit = 0;
  char buf[MAX_BUFFER];
  while (mida < size) {
    if(size-mida > MAX_BUFFER) {
      llegit = sys_read_file_aux(fito, buf, MAX_BUFFER);
      if(llegit < 0) return llegit;
      if(copy_to_user(buf, buffer, llegit) == -1) return -EFAULT;
      buffer += llegit;
      mida += llegit;
      if(llegit < MAX_BUFFER) {
        return mida;
      }
    }
    else {
      llegit = sys_read_file_aux(fito, buf, size-mida);
      if(llegit < 0) return llegit;
      if(copy_to_user(buf, buffer, llegit) == -1) return -EFAULT;
      mida += llegit;
      return mida;
    }
  }  
  return size;
}

int sys_read_keyboard(struct fo* fito, char *buffer,int size) {
  if(current()->pid == 0) return -EPERM;
  if(size > disponibles()) {
    current()->buffer = buffer;
    current()->ch_pend = size;
    bloquejarproces(&runqueue,&keyboardqueue);
  }
  else {
    return read_sense_bloqueig(buffer, size);
  }
  return -EPERM;
}

struct file_operations display_op = {
   NULL, NULL, NULL, sys_write_console
};

struct file_operations keyboard_op = {
   NULL, sys_read_keyboard, NULL, NULL
};
struct file_operations file_op = { 
   NULL, sys_read_file, NULL, sys_write_file
};

#include <utils.h>
#include <types.h>
#include <errno.h>
#include <mm_address.h>
#include <mm.h>
#include <fitxer.h>
void copy_data(void *start, void *dest, int size)
{ 
  DWord *p = start, *q = dest;
  Byte *p1, *q1;
  while(size > 4) {
    *q++ = *p++;
    size -= 4;
  }
  p1=(Byte*)p;
  q1=(Byte*)q;
  while(size > 0) {
    *q1++ = *p1++;
    size --;
  }
}
/* Copia de espacio de usuario a espacio de kernel, devuelve 0 si ok y -1 si error*/
int copy_from_user(void *start, void *dest, int size)
{
  DWord *p = start, *q = dest;
  Byte *p1, *q1;
  while(size > 4) {
    *q++ = *p++;
    size -= 4;
  }
  p1=(Byte*)p;
  q1=(Byte*)q;
  while(size > 0) {
    *q1++ = *p1++;
    size --;
  }
  return 0;
}
/* Copia de espacio de kernel a espacio de usuario, devuelve 0 si ok y -1 si error*/
int copy_to_user(void *start, void *dest, int size)
{
  DWord *p = start, *q = dest;
  Byte *p1, *q1;
  while(size > 4) {
    *q++ = *p++;
    size -= 4;
  }
  p1=(Byte*)p;
  q1=(Byte*)q;
  while(size > 0) {
    *q1++ = *p1++;
    size --;
  }
  return 0;
}

/* access_ok: Checks if a user space pointer is valid
 * @type:  Type of access: %VERIFY_READ or %VERIFY_WRITE. Note that
 *         %VERIFY_WRITE is a superset of %VERIFY_READ: if it is safe
 *         to write to a block, it is always safe to read from it
 * @addr:  User space pointer to start of block to check
 * @size:  Size of block to check
 * Returns true (nonzero) if the memory block may be valid,
 *         false (zero) if it is definitely invalid
 */
int access_ok(int type, const void * addr, unsigned long size)
{
    if (type == O_RDONLY) {
        if ((unsigned int)addr >= L_USER_START && (unsigned int)addr+size <= PAG_LOG_FINAL_DATA<<12)
            return 1;
    }
    else if (type == O_WRONLY) {
        if ((unsigned int)addr >= PAG_LOG_INIT_DATA_P0<<12 && (unsigned int)addr+size <= PAG_LOG_FINAL_DATA<<12)
            return 1;
    }
    return 0;

}

int leng(int num) {
  int i = 0;
	if(num == 0) i = 1;
  while (num > 0)
    {
      i++;
      num /= 10;
    }
  return i;
}

void itoa (int num, char *buffer)
{
  int i = leng(num);
  buffer[i] = '\0';
  while (i > 0)
    {
      buffer[i - 1] = (num % 10) + '0';
      num /= 10;
      i--;
    }
}

int comprova_fd(int fd, int operacio)
{
  if(fd >= 10 || fd < 0) return -EBADF;
  if(current()->tch[fd].tf == NULL) return -EBADF;
  if(current()->tch[fd].tf->rw != operacio && current()->tch[fd].tf->rw != O_RDWR) {

    return -EBADF;
  }
  return 0;
}

void copy_string(char *buffer1,char *buffer2) {
  int i = 0;
  while(buffer1[i] != '\0') {
    buffer2[i] = buffer1[i];
    ++i;
  }
  buffer2[i] = '\0';
}

int tamany_string(char *buffer1) {
  int i = 0;
  while(buffer1[i++] != '\0');
  return i;
}

int compare_string(char *buffer1,char *buffer2) {
  int i = 0;
  if(tamany_string(buffer1) != tamany_string(buffer2)) return 0;
  while(buffer1[i] != '\0' && buffer2[i] != '\0') {
    if(buffer1[i] != buffer2[i]) return 0;
    ++i;
  }
  return 1;
}

int comprova_flags(int f1, int f2) {
  return (f2 == O_RDWR || f1 == f2);
}

int copia_segura(char *buffer1,char *buffer2) {
  int i = 0;
  if(buffer1 == NULL) return -EFAULT;
  if(access_ok(O_RDONLY, &buffer1[i], sizeof(char)) == 0) return -EFAULT;
  while(buffer1[i] != '\0' && i < FILE_NAME_SIZE) {
    if(access_ok(O_RDONLY, &buffer1[i], sizeof(char)) == 0) return -EFAULT;
    copy_from_user(&buffer1[i], &buffer2[i], sizeof(char));
    i++;
  }
  if(i >= FILE_NAME_SIZE) return -ENAMETOOLONG;
  if(access_ok(O_RDONLY, &buffer1[i], sizeof(char)) == 0) return -EFAULT;
  buffer2[i] = '\0';
  return 0;
}

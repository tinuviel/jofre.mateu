/*
 * system.c -  
 */ 

#include <segment.h>
#include <types.h>
#include <interrupt.h>
#include <hardware.h>
#include <system.h>
#include <sched.h>
#include <mm.h>
#include <io.h>
#include <utils.h>
#include <fitxer.h>
#include <fat.h>
#include <devices.h>

int (*usr_main)(void) = (void *) PH_USER_START;
unsigned int *p_sys_size = (unsigned int *) KERNEL_START;
unsigned int *p_usr_size = (unsigned int *) KERNEL_START+1;
unsigned int *p_rdtr = (unsigned int *) KERNEL_START+2;

/************************/
/** Auxiliar functions **/
/************************/
/**************************
 ** setSegmentRegisters ***
 **************************
 * Set properly all the registers, used
 * at initialization code.
 *   DS, ES, FS, GS <- DS
 *   SS:ESP <- DS:DATA_SEGMENT_SIZE
 *         (the stacks grows towards 0)
 *
 * cld -> gcc2 wants DF (Direction Flag (eFlags.df))
 *        always clear.
 */

/*
 * This function MUST be 'inline' because it modifies the %esp 
 */
inline void set_seg_regs(Word data_sel, Word stack_sel, DWord esp)
{
      esp = esp - 5*sizeof(DWord); /* To avoid overwriting task 1 */
	  __asm__ __volatile__(
		"cld\n\t"
		"mov %0,%%ds\n\t"
		"mov %0,%%es\n\t"
		"mov %0,%%fs\n\t"
		"mov %0,%%gs\n\t"
		"mov %1,%%ss\n\t"
		"mov %2,%%esp"
		: /* no output */
		: "r" (data_sel), "r" (stack_sel), "g" (esp) );

}

/*
 *   Main entry point to ZEOS Operatin System 
 */

int __attribute__((__section__(".text.main"))) 
  main(void) 
{

  set_eflags();
  /* Define the kernel segment registers */
  set_seg_regs(__KERNEL_DS, __KERNEL_DS, KERNEL_ESP);


  printk("Kernel Loaded!    "); 
  
  /* Initialize hardware data */
  setGdt(); /* Definicio de la taula de segments de memoria */
  setIdt(); /* Definicio del vector de interrupcions */
  setTSS(); /* Definicio de la TSS */
  
  /* Initialize Memory */
  init_mm();

  /*Initialize Structs*/
  ini_tfo();
  ini_ti();
  ini_sem();
  ini_dir();
  initZeOSFat();

  /*Initialize Devices*/
  copy_string("DISPLAY", direct[0].nom);
  direct[0].in = &ti[0];
  ti[0].numref = 1;
  ti[0].rw = O_WRONLY;
  ti[0].op = &display_op;
  ti[0].td = &direct[0];

  copy_string("KEYBOARD", direct[1].nom);
  direct[1].in = &ti[1];
  ti[1].numref = 1;
  ti[1].rw = O_RDONLY;
  ti[1].op = &keyboard_op;
  ti[1].td = &direct[1];

  /* Initialize task 0 data */
  init_task0();

  
  /* Move user code/data now (after the page table initialization) */
  copy_data((void *) KERNEL_START + *p_sys_size, usr_main, *p_usr_size);

  
  printk("Entering user mode..."); 

  /*
   * We return from a 'theorical' call to a 'call gate' to reduce our privileges
   * and going to execute 'magically' at 'usr_main'...
   */
  enable_int();
  return_gate(__USER_DS, __USER_DS, USER_ESP, __USER_CS, L_USER_START);

  /* The execution never arrives to this point */
  return 0;
}

inline void ini_tfo() {
  //INICIALITZACIÓ TFO
  int i = 0;
  for(i = 0; i < NR_FTX; ++i) {
    tfo[i].numref = 0;
  }
}

inline void ini_ti() {
  int i = 0;
  //INICIALITZACIÓ TINODES
  for(i = 0; i < NR_FTX; ++i) {
    ti[i].numref = 0;
    ti[i].td = NULL;
  }
}

inline void ini_sem() {
  int i = 0;
  //INICIALITZACIÓ SEMÀFORS
  for(i = 0; i < NR_SEM; ++i) {
    semlist[i].prop = -1;
    semlist[i].n_sem = i;
  }
}

inline void ini_dir() {
  int i = 0;
  //INICIALITZACIÓ DIRECTORI
  for(i = 0; i < MAX_DIR; ++i) {
    direct[i].in = NULL;
  }
}

inline void initZeOSFat() {
  int i = 0;
  //INICIALITZACIÓ FAT
  for(i = 0; i < MAX_BLOCKS; ++i) {
    fat[i] = i+1;
  }
  fat[MAX_BLOCKS-1] = EOF;
}

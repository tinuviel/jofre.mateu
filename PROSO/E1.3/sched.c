/*
 * sched.c - initializes struct for task 0
 */
 
#include <sched.h>
#include <mm.h>
#include <io.h>
#include <fat.h>
#include <fitxer.h>
#include <utils.h>
#include <devices.h>
#include <sys.h>

int interrupt = 0;
LIST_HEAD(runqueue);
LIST_HEAD(freequeue);

int qr;

struct sem_struct semlist[NR_SEM];

struct protected_task_struct task[NR_TASKS]
  __attribute__((__section__(".data.task")));


void init_task0(void)
{
  int i;
  
  //INICIALITZACIÓ PCB P0
  task[0].t.task.pid = 0;
  task[0].t.task.quantum = QUANTUM;
  task[0].t.task.status.total_tics = 0;
  task[0].t.task.status.total_trans = 0;
  task[0].t.task.status.remaining_tics = QUANTUM;
  qr = task[0].t.task.quantum;
  list_add(&task[0].t.task.node,&runqueue);
  INIT_LIST_HEAD(&task[0].t.task.sem);
  for(i = 0; i < NR_CH; ++i) {
    task[0].t.task.tch[i].tf = NULL;
  }

//open manual de pantalla 
  task[0].t.task.tch[1].tf = &tfo[0];
  tfo[0].numref++;
  tfo[0].rw = O_WRONLY;
  tfo[0].tin = &ti[0];
  tfo[0].tin->numref++;
//open manual de teclat 
  task[0].t.task.tch[0].tf = &tfo[1];
  tfo[1].numref++;
  tfo[1].rw = O_RDONLY;
  tfo[1].tin = &ti[1];
  tfo[1].tin->numref++;
//FREE QUEUE
  for(i = 1; i < NR_TASKS; ++i) list_add(&task[i].t.task.node,&freequeue);
/* Initializes paging for the process 0 adress space */
  set_user_pages(&task[0].t.task);
  set_cr3();
}

struct task_struct* current() {
  struct task_struct *inipag;
  __asm__ (
    "movl $0xFFFFF000, %%eax\n"
    "andl %%esp, %%eax\n"
    "movl %%eax, %0"
    :"=r"(inipag)
  );
  return inipag;
}

struct task_struct *list_head_to_task_struct (struct list_head *l) {
  return list_entry (l, struct task_struct, node);
}

void task_switch(union task_union *t) {
  unsigned int pag,x;
  t->task.status.total_trans++;
  t->task.status.remaining_tics = t->task.quantum;
  qr = t->task.quantum;
  tss.esp0 = (unsigned int) &(t->stack[KERNEL_STACK_SIZE]);
  x = tss.esp0; 
  for (pag = 0; pag < NUM_PAG_DATA; pag++) set_ss_pag(PAG_LOG_INIT_DATA_P0 + pag, t->task.pags[pag]);
  set_cr3();
  if(interrupt == 1) {
    interrupt = 0;
    __asm__ (
      "MOVB $0x20, %al\n"
      "OUTB %al, $0x20\n"
    );   
  }
  __asm__ (
    "movl %0, %%esp\n"
    "subl $0x40, %%esp\n"
    "popl %%ebx\n"
    "popl %%ecx\n"
    "popl %%edx\n"
    "popl %%esi\n"
    "popl %%edi\n"
    "popl %%ebp\n"
    "popl %%eax\n"
    "popl %%ds\n"
    "popl %%es\n"
    "popl %%fs\n"
    "popl %%gs\n"
    "iret \n"
    :
    :"g"(x)
  );
}

inline void canvillista(struct list_head *l1, struct list_head *l2) {
  struct list_head *act= list_first(l1);
  list_del(act);
  list_add_tail(act, l2);
}

inline void bloquejarproces(struct list_head *l1, struct list_head *l2) {
  struct list_head *act;
  struct task_struct *t;
  canvillista(l1, l2);
  act = list_first(l1);
  t = list_head_to_task_struct(act);
  task_switch((union task_union *)t);
}

inline void round_robin() {
  struct task_struct *seg;
  struct list_head *act;
  current()->status.remaining_tics = --qr;
  if(qr == 0 && !list_is_last(&current()->node, &runqueue)){
    interrupt = 1;
    current()->status.total_tics += current()->quantum;
    canvillista(&runqueue,&runqueue);
    act = list_first(&runqueue);
    seg = list_head_to_task_struct(act);
    task_switch((union task_union *) seg);
  }
  else if (qr == 0){
    current()->status.total_tics += current()->quantum;
    qr = current()->quantum;
  }
}

int entrada_canal_lliure() {
  int i = 0;
  for(i = 0; i < NR_CH; ++i) {
    if(current()->tch[i].tf == NULL) return i;
  }
  return -1;
}

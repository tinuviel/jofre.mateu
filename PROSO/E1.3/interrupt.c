/* 
 * interrupt.c -
 */ 
#include <types.h>
#include <interrupt.h>
#include <segment.h>
#include <hardware.h>
#include <entry.h>
#include <io.h>
#include <utils.h>
#include <sys.h>
#include <sched.h>
#include <mm.h>

Gate idt[IDT_ENTRIES];
Register    idtR;
int tics = 0;
int seg = 0;
int min = 0;
int lectura = 0;
int escriptura = 0;
int acumulat = 0;
char buffer[256], keyboard[MAX_KEY_BUFFER];
char char_map[] =
{
  '\0','\0','1','2','3','4','5','6',
  '7','8','9','0','\'','�','\0','\0',
  'q','w','e','r','t','y','u','i',
  'o','p','`','+','\0','\0','a','s',
  'd','f','g','h','j','k','l','�',
  '\0','�','\0','�','z','x','c','v',
  'b','n','m',',','.','-','\0','*',
  '\0','\0','\0','\0','\0','\0','\0','\0',
  '\0','\0','\0','\0','\0','\0','\0','7',
  '8','9','-','4','5','6','+','1',
  '2','3','0','\0','\0','\0','<','\0',
  '\0','\0','\0','\0','\0','\0','\0','\0',
  '\0','\0'
};

LIST_HEAD(keyboardqueue);

void setInterruptHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
  /***********************************************************************/
  /* THE INTERRUPTION GATE FLAGS:                          R1: pg. 5-11  */
  /* ***************************                                         */
  /* flags = x xx 0x110 000 ?????                                        */
  /*         |  |  |                                                     */
  /*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
  /*         |   \ DPL = Num. higher PL from which it is accessible      */
  /*          \ P = Segment Present bit                                  */
  /***********************************************************************/
  Word flags = (Word)(maxAccessibleFromPL << 13);
  flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

  idt[vector].lowOffset       = lowWord((DWord)handler);
  idt[vector].segmentSelector = __KERNEL_CS;
  idt[vector].flags           = flags;
  idt[vector].highOffset      = highWord((DWord)handler);
}

void setTrapHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
  /***********************************************************************/
  /* THE TRAP GATE FLAGS:                                  R1: pg. 5-11  */
  /* ********************                                                */
  /* flags = x xx 0x111 000 ?????                                        */
  /*         |  |  |                                                     */
  /*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
  /*         |   \ DPL = Num. higher PL from which it is accessible      */
  /*          \ P = Segment Present bit                                  */
  /***********************************************************************/
  Word flags = (Word)(maxAccessibleFromPL << 13);

  //flags |= 0x8F00;    /* P = 1, D = 1, Type = 1111 (Trap Gate) */
  /* Changed to 0x8e00 to convert it to an 'interrupt gate' and so
     the system calls will be thread-safe. */
  flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

  idt[vector].lowOffset       = lowWord((DWord)handler);
  idt[vector].segmentSelector = __KERNEL_CS;
  idt[vector].flags           = flags;
  idt[vector].highOffset      = highWord((DWord)handler);
}

void setIdt()
{
  /* Program interrups/exception service routines */
  idtR.base  = (DWord)idt;
  idtR.limit = IDT_ENTRIES * sizeof(Gate) - 1;
  /* ADD INITIALIZATION CODE FOR INTERRUPT VECTOR */
  setInterruptHandler(0, divide_error_handler,0);//canviar mode maxim execucio
  setInterruptHandler(1,debug_handler,0);
  setInterruptHandler(2,nm1_handler,0);
  setInterruptHandler(3,breakpoint_handler,3);
  setInterruptHandler(4,overflow_handler,3);
  setInterruptHandler(5,bounds_check_handler,3);
  setInterruptHandler(6,invalid_opcode_handler,0);
  setInterruptHandler(7,device_not_available_handler,0);
  setInterruptHandler(8,double_fault_handler,0);
  setInterruptHandler(9,coprocessor_segment_overrun_handler,0);
  setInterruptHandler(10,invalid_tss_handler,0);
  setInterruptHandler(11,segment_not_present_handler,0);
  setInterruptHandler(12,stack_exception_handler,0);
  setInterruptHandler(13,general_protection_handler,0);
  setInterruptHandler(14,page_fault_handler,0);
  setInterruptHandler(16,floatin_point_error_handler,0);
  setInterruptHandler(17,alignment_check_handler,0);
  setInterruptHandler(32,clock_handler,0);
  setInterruptHandler(33,keyboard_handler,0);
  setTrapHandler(128,system_call,3);
  set_idt_reg(&idtR);
}

void divide_error_routine() {
  printk("DIVIDE ERROR!\n");
  while(1);
}

void debug_routine() {
  printk("DEBUG!\n");
  while(1);
}

void nm1_routine() {
  printk("NM1!\n");
  while(1);
}

void breakpoint_routine() {
  printk("BREAKPOINT!\n");
  while(1);
}

void overflow_routine() {
  printk("OVERFLOW!\n");
  while(1);
}

void bounds_check_routine() {
  printk("BONUS CHECK!\n");
  while(1);
}

void invalid_opcode_routine() {
  printk("INVALID OPCODE!\n");
  while(1);
}

void device_not_available_routine() {
  printk("DEVICE NOT AVAIBLE!\n");
  while(1);
}

void double_fault_routine() {
  printk("DOUBLE FAULT!\n");
  while(1);
}

void coprocessor_segment_overrun_routine() {
  printk("COPROCESOR SEGMENT!\n");
  while(1);
}

void invalid_tss_routine() {
  printk("INVALID TSS!\n");
  while(1);
}

void segment_not_present_routine() {
  printk("SEGMENT NOT PRESENT!\n");
  while(1);
}

void stack_exception_routine() {
  printk("STACK EXCEPTION!\n");
  while(1);
}

void general_protection_routine() {
  printk("GENERAL PROTECTION!\n");
  while(1);
}

void page_fault_routine() {
  int pid, i;
  char buf[10];
  pid = sys_getpid();
  itoa(pid, buf);
  printk("Excepcio de fallada de pagina al proces ");
  for(i = 0; i < leng(pid); i++) printc(buf[i]);
  printk("\n");

  if (pid == 0) while(1);
  else sys_exit();
}

void floatin_point_error_routine() {
  printk("FLOATIN POINT ERROR!\n");
  while(1);
}

void alignment_check_routine() {
  printk("ALIGNMENT CHECK!\n");
  while(1);
}

void clock_routine() {
  ++tics;
  if (tics%18 == 0) {
    seg++;
    if(seg == 60) {
      seg = 0;
      min++;
    }
  }
  itoa(min, buffer);
  int i = 0;
  int xx = 77 - leng(min);
  if (leng(min) == 1) xx--;
  int yy = 0;
  while(i < leng(min)) {
    if(leng(min) == 1) printc_xy(xx++, yy, '0');
    printc_xy(xx++, yy, buffer[i++]);
  }
  i = 0;
  printc_xy(xx++, yy, ':');
  itoa(seg, buffer);
  while(i < leng(seg)) {
    if(leng(seg) == 1) printc_xy(xx++, yy, '0');
    printc_xy(xx++, yy, buffer[i++]);
  }
  current()->status.total_tics++;
  round_robin();
}

int apte(int size) {
  if (list_empty(&keyboardqueue)) return 0;
  if ((lectura < escriptura)&&(escriptura-lectura >= size)) {
    return 1;
  }
  if ((lectura > escriptura)&&((MAX_KEY_BUFFER)-lectura+escriptura-1 >= size)) return 1;
  return 0;
}

void buida_buffer(union task_union *task, char *key_buffer, int size) {
  int i;
  //carrega p�gines1
  for (i = 0; i < NUM_PAG_DATA; i++) set_ss_pag(PAG_LOG_FINAL_DATA + i, task->task.pags[i]);
  i = 0;
  while(i < size) {
    copy_to_user(&keyboard[lectura], &key_buffer[i++]+(NUM_PAG_DATA<<12), sizeof(char));
    lectura = (lectura+1)%MAX_KEY_BUFFER;
  }
  //esborra pagines
  for (i = 0; i < NUM_PAG_DATA; i++) del_ss_pag(PAG_LOG_FINAL_DATA+i);
  //flush
  set_cr3();
}


void keyboard_routine() {
  char c = inb(0x60);
  char c2;
  char buffer[7] = { ' ',' ',' ',' ',' ',' ',' '}; 
  char *key_buffer = NULL;
  int size = MAX_KEY_BUFFER;
  struct task_struct *pcb = NULL;
  union task_union *task;


  if (c>>7 == 0) {
    if (char_map[(unsigned int)c] != '\0') {
       c2 = char_map[(unsigned int)c];
       buffer[6] = c2;
       printk_xy(73, 24, buffer);

      if(!list_empty(&keyboardqueue)) {
        pcb = list_head_to_task_struct(list_first(&keyboardqueue));
        size = pcb->ch_pend;
        key_buffer = pcb->buffer;
      }

    //----------------CAPTACIO DE CARACTERS-------------------------
      if(((escriptura+1)%MAX_KEY_BUFFER) != lectura) { //podem escriure al buffer
        keyboard[escriptura] = c2;
        escriptura = (escriptura+1)%MAX_KEY_BUFFER;
        if (apte(size)) { //ja tenim prous caracters per retornar el proces
          task = (union task_union *)pcb;

          buida_buffer(task, key_buffer, size);

          task->stack[EAXX] = acumulat + size;
          acumulat = 0;
          canvillista(&keyboardqueue, &runqueue);
        }
        else if(escriptura +1 == lectura) { // buffer ple, copiem una part i continuem
          task = (union task_union *)pcb;
      
          buida_buffer(task, key_buffer, size);
          pcb->ch_pend -= (MAX_KEY_BUFFER -1);
          pcb->buffer = buffer + MAX_KEY_BUFFER -1;
          acumulat += MAX_KEY_BUFFER -1;
        }
      }
    }
    else printk_xy(73, 24, "CONTROL");
  }
}

int disponibles() {
  if (lectura <= escriptura) return escriptura-lectura; 
  else if (lectura > escriptura) return (MAX_KEY_BUFFER)-lectura+escriptura-1;
}

int read_sense_bloqueig(char *buf, int size) {
  int i = 0;
  while (i < size) {
    copy_to_user(&keyboard[lectura], &buf[i++], sizeof(char));
  }
  return i;
}

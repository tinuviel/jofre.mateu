#include <fat.h>

int lliure = 0;

struct block HD[MAX_BLOCKS];

int fat[MAX_BLOCKS];

int bloc_lliure_inicial() {
  //Cos de la funcio
  int i = lliure;
  if (lliure != EOF) lliure = fat[lliure];
  if (i != EOF) fat[i] = EOF;
  return i;
}

void allibera_blocs(int ini) {
  int seg, ant;
  ant =ini;
  seg = fat[ini];
  while(seg != EOF) {
    fat[ant] = lliure;
    lliure = ant;
    ant = seg;
    seg = fat[seg];
  }
  fat[ant] = lliure;
  lliure = ant;
}

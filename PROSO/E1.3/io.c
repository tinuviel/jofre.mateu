/* 
 * io.c - 
 */
 
#include <io.h>

/**************/
/** Screen  ***/
/**************/

#define NUM_COLUMNS 80
#define NUM_ROWS    25

Byte x, y=15; 

/* Read a byte from 'port' */
Byte inb (unsigned short port)
{
  Byte v;

  __asm__ __volatile__ ("inb %w1,%0":"=a" (v):"Nd" (port));
  return v;
}

void printc(char c)
{ 
  Word ch = (Word) (c & 0x00FF) | 0x0200;
  DWord screen = 0xb8000 + (y * NUM_COLUMNS + x) * 2;
   __asm__ __volatile__ ( "movb %0, %%al; outb $0xe9" ::"a"(c));
  if (++x >= NUM_COLUMNS || c == '\n')
  {
    x = 0;
    if (++y >= NUM_ROWS) {
      y = 0;
    }
  }
  asm("movw %0, (%1)" : : "g"(ch), "g"(screen));
}

void printk(char *string)
{
  int i;
  for (i = 0; string[i]; i++)
    printc(string[i]);
}

void printc_xy(int xx, int yy, char c) {
  Word ch = (Word) (c & 0x00FF) | 0x0200;
  DWord screen = 0xb8000 + (yy * NUM_COLUMNS + xx) * 2;
  asm("movw %0, (%1)" : : "g"(ch), "g"(screen));  
}

void printk_xy(int xx, int yy, char *buffer) {
  /*xx = xx + x;
  x = xx - x;
  xx = xx - x;
  yy = yy + y;
  y = yy - y;
  yy = yy - y;
  printk(buffer);
  xx = xx + x;
  x = xx - x;
  xx = xx - x;
  yy = yy + y;
  y = yy - y;
  yy = yy - y;*/
  int i;
  for(i = 0; buffer[i] != '\0'; i++) {
    printc_xy(xx, yy, buffer[i]);
    if (++xx >= NUM_COLUMNS || buffer[i] == '\n') {
      xx = 0;
      if (++yy >= NUM_ROWS) {
        yy = 0;
      }
    }
  }

 
}

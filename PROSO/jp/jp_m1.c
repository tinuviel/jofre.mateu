#include <stdio.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>

#define ERR_ROOT "El joc de proves no s'executa com a root\n"
#define module_path "module1.ko"
#define module_name "module1"
#define insmod "insmod"
#define rmmod "rmmod"
#define MAX_TRIES 10

/* Misc. */

inline int load(int pid, const char * path) {
    int ret, child;
    char pidarg[20];
    sprintf(pidarg,"pid=%d", pid);
    
    child = fork();
    if (!child) {
        close(2);
        execlp(insmod, insmod, path, pidarg, NULL);
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

inline int unload() {
    int ret, child;
    child = fork();
    if (!child) {
        close(2);
        execlp(rmmod, rmmod, module_name, NULL);
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

/* Jocs de proves */

int writes = 0; // tants com flushes: es produeixen quan \n | variable
int opens = 0;
int lseeks = 0;
int clones = 0;
int closes = 0;

void runjpopen() {
    int fd;

    printf("\nProvem open...\nOPEN amb path NULL...\t\t");
    writes++;

    fd = open(NULL, 0);
    opens++;
    
    if (fd < 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
    
    printf("OPEN O_RDONLY de README...\t");
    //writes++;
    fd = open("/dev/random", O_RDONLY);
    opens++;
    
    if (fd >= 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
}

void runjplseek() {
    printf("\nProvem lseek...\n");
    writes++;
    
    off_t offset;
    
    printf("LSEEK amb BADF...\t\t");
    //writes++;

    offset = lseek(4, 1, SEEK_CUR);
    lseeks++;

    if (offset < 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
    
    printf("LSEEK amb EOF...\t\t");
    //writes++;

    offset = lseek(3, 0, SEEK_END);
    lseeks++;

    if (offset >= 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
}

void runjpwrite() {
    printf("\nProvem write...\n");
    writes++;
    
    printf("WRITE amb buffer NULL...\t");
    //writes++;
    
    if (write(2, NULL, 100) < 0) printf("OK\n");
    else printf("ERROR\n");
    writes+=2;
    
    printf("WRITE amb BADF...\t\t");
    //writes++;
    
    if (write(4, module_path, 100) < 0) printf("OK\n");
    else printf("ERROR\n");
    writes+=2;
}

void runjpclone() {
    printf("\nProvem clone...\n");
    writes++;
    
    int pid = fork();
    clones++;
    
    if (!pid) exit(0);
    
    printf("CLONE amb fork()...\t\t");
    //writes++;
    
    if (pid >= 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
    
    // Si no es fa bé, no arriba a fer el clone
/*    printf("CLONE amb NULLs...\t\t");*/
/*    writes++;*/
/*    */
/*    pid = clone(dumb_sig, NULL, 0, NULL);*/
/*    clones++;*/
/*    */
/*    if (pid < 0) printf("OK\n");*/
/*    else printf("ERROR\n");*/
/*    writes++;*/
}

void runjpclose() {
    printf("\nProvem close...\n");
    writes++;
    
    printf("CLOSE amb BADF...\t\t");
    //writes++;
    
    if (close(4) < 0) printf("OK\n");
    else printf("ERROR\n");
    writes++;
    closes++;
    
    printf("CLOSE README...\t\t\t");
    //writes++;
    
    if (close(3) == 0) printf("OK\n");
    else printf("ERROR\n");
    writes++; closes++;
}

void printjpinfo() {
    writes += 7; // writes que anem a fer ara

    printf("\nNúmero d'entrades a funcions esperades pel proces %d:\n",
        getpid());
    printf("OPEN:\t%02d\n", opens);
    printf("CLOSE:\t%02d\n", closes);
    printf("WRITE:\t%02d\n", writes);
    printf("LSEEK:\t%02d\n", lseeks);
    printf("CLONE:\t%02d\n", clones);
}

void runjp() {
    printf("Inici del joc de proves.\n");
    writes++;
    
    runjpopen();
    runjplseek();
    runjpwrite();
    runjpclone();
    runjpclose();
    
    writes++; // Pel printf de fi de joc de proves
    printjpinfo();
    
    printf("\nFi del joc de proves.\n");
}

/* Main */

int main(int argc, char **argv) {
    if (geteuid() != 0) {
        fprintf(stderr,ERR_ROOT);
        return -1;
    }
    
    char * path = module_path;
    if (argc == 2) path = argv[1];
    
    sem_t * semparent;
    sem_t * semchild;
    
    
    semparent = sem_open("parent", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR, 0);
    
    if (semparent == SEM_FAILED) {
        perror("parent");
        return -1;
    }
    
    semchild = sem_open("child", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR, 0);
    if(semchild == SEM_FAILED) {
        perror("child");
        return -1;
    }
    
    int jppid = fork();
    if (!jppid) {
        int parent = getppid();
        
        // Lo hereda
        //semparent = sem_open("parent", O_RDWR);
        /*if (semparent == SEM_FAILED) {
            perror("parent");
            return -1;
        }*/
        
        //semchild = sem_open("child", O_RDWR);
        /*if(semchild == SEM_FAILED) {
            perror("child");
            return -1;
        }*/
        
        // Esperem a carregar el mòdul
        sem_wait(semparent);
        
        runjp();
        //sleep(1); // Per deixar temps al pause del pare
        
        // Esperem a que es descarregui el mòdul
        sem_post(semchild);
        sem_wait(semparent);
        
        // Ja hem acabat
        sem_close(semchild);
        sem_unlink("child");
        
        return 0;
    }

    printf("Pid del joc de proves: %d\n", jppid);
    printf("Prem enter per carregar el mòdul\n");
    
    getchar();
    
    printf("Loading module 1...\n");
    
    while (1) {
        if (!load(jppid, path)) {
            printf("Module loaded successfully!\n");
            break;
        }
        else {
            int i;
            //for (i = 0; unload() && i < MAX_TRIES; i++);
            while(unload());
        }
    }

    
    // Avisem que el modul està carregat
    // Ens esperem a que acabin els jocs de proves
    sem_post(semparent);
    sem_wait(semchild);
    sem_close(semchild);
    
    printf("Unloading module 1...\n");
    while(unload());

    // Avisem que ja hem descarregat el mòdul
    sem_post(semparent);
    sem_close(semparent);
    sem_unlink("parent");
    
    printf("Module unloaded successfully!\n");
    
}

#include <stdio.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>

#include <errno.h>

#define ERR_ROOT "El joc de proves no s'executa com a root\n"
#define module_path1 "module1.ko"
#define module_path2 "module2.ko"
#define module1_name "module1"
#define module2_name "module2"
#define insmod "insmod"
#define rmmod "rmmod"
#define mknod "mknod"
#define rm "rm"
#define device_name "fm2"
#define MAX_TRIES 10


struct t_info {
    int pid;
    int num_entrades;
    int num_sortides_ok;
    int num_sortides_error;
    unsigned long long durada_total;
};


/* Misc. */

inline int load(const char * path, const char * param) {
    int ret, child;
    
    child = fork();
    if (!child) {
        close(2);
        execlp(insmod, insmod, path, param, NULL);
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

inline int unload(const char * module_name) {
    int ret, child;
    child = fork();
    if (!child) {
        close(2);
        execlp(rmmod, rmmod, module_name, NULL);
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

inline int get_major_minor(int * major, int * minor) {
    const char *format = "[%*f]   -Major: %d\n[%*f]   -Minor: %d\n";

    system("dmesg | tail -n 2 > tmp");
    char str[100];
    int fd = open("tmp", O_RDONLY);
    if (read(fd, str, 100) < 0) perror("Error obtenint major i minor");
    
    sscanf(str, format, major, minor);
    close(fd);
    unlink("tmp");
    
    return 0;
}

int new_device() {
    int child, ret;
    child = fork();
    if (!child) {
        close(2);
        int major = 254, minor = 0;
        get_major_minor(&major, &minor);
        
        char smaj[5], smin[5];
        sprintf(smaj, "%d", major);
        sprintf(smin, "%d", minor);

        execlp(mknod, mknod, device_name, "c", smaj, smin, NULL);
    	perror("ERROR");
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

inline int delete_device() {
    int child, ret;
    child = fork();
    if (!child) {
        close(2);
        execlp(rm, rm, device_name, NULL);
        perror("ERROR");
    }
    waitpid(child, &ret, 0);
    return !(WIFEXITED(ret) && !WEXITSTATUS(ret));
}

/* Jocs de proves */

int writes = 0; // tants com flushes: es produeixen quan \n | variable
int opens = 0;
int lseeks = 0;
int clones = 0;    
int closes = 0;

void runOberturesMultiples() {
    printf("Provant multiples open\n");
    
    printf("  -Fem un open...");
    
    int fd = open(device_name, O_RDONLY);
    
    if (fd >= 0) printf("OK  device obert amb fd: %d\n", fd); 
    else perror("ERROR: ");
    
    printf("  -Tornant a obrir el device...\n");

    int fd2 = open(device_name, O_RDONLY);
    
    if (fd2 < 0) perror("     OK  Fitxer ja obert"); 
    else printf("ERROR: ha pogut tornar a obrir el device. fd: %d\n",fd);
    
    printf("  -tancant canal...");
    if (!close(fd)) printf("OK\n");
    else perror("ERROR al tancar");
}

void ObrintEnModeSudo() {
    printf("Provant privilegis\n");
    
    seteuid(50);
    
    printf("  -Obrint device en mode no sudo, el meu eUID ara es: %d...\n", geteuid());
    
    
    int fd = open(device_name, O_RDONLY);
    
    if (fd < 0) perror("    OK  device no es pot obrir en mode usuari\n"); 
    else printf("ERROR: ha pogut obrir el fitxer. fd = %d\n",fd);
    
    
    printf("  -Obrint amb mode sudo, ara el meu eUID es: %d...\n", geteuid());
    
    seteuid(0);
    
    fd = open(device_name, O_RDONLY);
    
    if (fd >= 0) printf("OK  device obert amb fd: %d\n", fd); 
    else perror("ERROR: no ha pogut obrir el fitxer en mode sudo");
    
    printf("Tancant canal...");
    
    fd = close(fd);
    
    if (fd < 0) perror ("ERROR");
    else printf("OK\n");
       
}

void PI_ioctl_read() {
    int accio;
    int param1;
    int param2;
    int pidaux;
    
    printf("Prova interactiva de ioctl i read, es requereix la interaccio de miss Yolanda\n");
    
    printf("   -Obrint el dispositiu...");
    int fd = open(device_name, O_RDONLY);
    if (fd >= 0) printf("OK\n");
    
    do {
        printf("------menu--------\n");
        printf("  0) sortir\n");
        printf("  1) ioctl\n");
        printf("  2) read\n");
        printf("  3) fes un fork\n");
        printf("  4) fes un open i un close\n");
        printf("  5) fes un lseek (open i close)\n");
        printf("elegeixi una acció: ");
        fscanf(stdin,"%d", &accio);

                                                                                            
        switch (accio) {
            case 0:
            break;
            case 1:
                printf(" -arg1: ");
                scanf("%d", &param1);
                switch (param1) {
                    case 0:
                        printf(" -arg2: ");
                        scanf("%d", &param2);
                        printf("Canvi de proces a: %d...", param2);
                        if (ioctl(fd, param1, &param2)<0) perror("ERROR");
                        else printf("OK\n");
                    break;
                    case 1:
                        printf(" -arg2: ");
                      //  fflush(stdin);
                        scanf("%d", &param2);
                        printf("\nCanvi consulta de estadistiques a: %d...", param2);
                        if (ioctl(fd, param1, param2)<0) perror("ERROR");
                        else printf("OK\n");
                    break;
                    case 2:
                        printf("Restejant estadistiques del proces acutal...");
                        if (ioctl(fd, param1, param2)<0) perror("ERROR");
                        else printf("OK\n");
                    break;
                    case 3:
                        printf("Resetejar totes les estadistiques de tots els programes");
                        if (ioctl(fd, param1, param2)<0) perror("ERROR");
                        else printf("OK\n");
                    break;
                    default:
                        printf("Operació invalida!! Estigues més atenta Yolanda!\n");
                    break;
                }
            break;
            case 2:
                printf(" -nombre de bytes a llegir: ");
                scanf("%d", &param1);
                struct t_info info;
                printf("llegint...");
                int read_bytes;
                if ((read_bytes = read(fd, &info, param1)) == sizeof(struct t_info)) {
                    printf("OK\n");
                    printf(" pid: %d \n num_entrades: %d\n num_sortides_ok:\
                        %d\n num_sortides_error: %d\n durada_total: %llu\n",
                        info.pid, info.num_entrades, info.num_sortides_ok,
                        info.num_sortides_error, info.durada_total);
                }else printf("s'han llegit: %d bytes. No son suficients per escriure el t_info\n", read_bytes);
            break;
            case 3:
               pidaux = fork();
               if (pidaux == 0) {
                    printf("  -Hola mon!!! soc un fillet i hem disposo a expirar,");
                    printf(" les meves ultimes paraules son pid: %d\n",getpid());
                    exit(0);
                }
                waitpid(pidaux, NULL, 0);
            break;
            case 4:
                printf("   -Obrint fitxer asdf.txt...");
                pidaux = open("asdf.txt", O_RDONLY|O_CREAT);
                if (pidaux >= 0) printf("OK\n");
                else printf("ERROR\n");
                printf("  -tancant el fitxer asdf.txt...");
                if (close(pidaux) >= 0) printf("OK\n");
                else printf("ERROR\n");
                unlink("asdf.txt");
            break;
            case 5:
                printf("   -Obrint fitxer asdf.txt...");
                pidaux = open("asdf.txt", O_RDONLY|O_CREAT);
                if (pidaux >= 0) printf("OK\n");
                else printf("ERROR\n");
                printf("  -executant lseek...");
                int offset = 0;
                if(lseek(pidaux, offset, SEEK_SET)!=-1) printf("OK\n");
                else printf("ERROR\n");
                
                
                printf("  -tancant el fitxer asdf.txt...");
                if (close(pidaux) >= 0) printf("OK\n");
                else printf("ERROR\n");
                unlink("asdf.txt");
            break;
            default:
                printf("no es una opcion\n");
        }
   } while(accio);
  printf("  -Tancant el canal...");//, fd);
  if(close(fd) < 0) perror("ERROR al tacar el canal");
  else printf("OK\n");
}


void runjp() {
    printf("Inici del joc de proves.\n");
    writes++;
    
    runOberturesMultiples();
    PI_ioctl_read();
    
    printf("\nFi del joc de proves.\n");
}

/* Main */

int main(int argc, char **argv) {
    if (geteuid() != 0) {
        fprintf(stderr,ERR_ROOT);
        return -1;
    }
    
    char * path_m1 = module_path1;
    char * path_m2 = module_path2;
    
    if (argc == 2) path_m1 = argv[1];
    else if (argc >= 2) {
        path_m1 = argv[1];
        path_m2 = argv[2];
    }
    
    sem_t * semparent;
    sem_t * semchild;
    
    
    semparent = sem_open("parent", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR, 0);
    
    if (semparent == SEM_FAILED) {
        perror("parent");
        return -1;
    }
    
    semchild = sem_open("child", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR, 0);
    if(semchild == SEM_FAILED) {
        perror("child");
        return -1;
    }
    
    int jppid = fork();
    if (!jppid) {
        int parent = getppid();
        
        // Esperem a carregar el mòdul
        sem_wait(semparent);
        
        runjp();
        
        // Esperem a que es descarregui el mòdul
        sem_post(semchild);
        sem_wait(semparent);
        
        // Ja hem acabat
        sem_close(semchild);
        sem_unlink("child");
        
        return 0;
    }

    printf("Pid del joc de proves: %d\n", jppid);
    printf("Prem enter per carregar el mòdul\n");
    
    getchar();
    
    printf("Loading module 1...\n");

    char pidarg[20];
    sprintf(pidarg,"pid=%d", jppid);
    
    while (1) {
        if (!load(path_m1, pidarg)) {
            printf("Module 1 loaded successfully!\n");
            while(1) {
                if (!load(path_m2, NULL)) {
                    printf("Module 2 loaded successfully!\n");
                    break;
                }
                else while(unload(module2_name));
            }
            break;
        }
        else {
            while(unload(module2_name));
            while(unload(module1_name));
        }
    }
    
    printf("Creant device...");
    
    if (!new_device()) printf("OK\n");
    else printf("ERROR creant device");
    
    

    
    // Avisem que el modul està carregat
    // Ens esperem a que acabin els jocs de proves
    sem_post(semparent);
    sem_wait(semchild);

    printf("Eliminant device...");

    if(!delete_device()) printf("OK\n");
    else printf("ERROR eliminant el device\n");
    
    printf("Unloading module 2...\n");    
    while(unload(module2_name));
    printf("Unloading module 1...\n");
    while(unload(module1_name));

    // Avisem que ja hem descarregat el mòdul
    sem_post(semparent);
    sem_close(semparent);
    sem_unlink("parent");
    
    printf("Module unloaded successfully!\n");
    
}

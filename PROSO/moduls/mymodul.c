#include "mymodul.h"

int pid = 1;
module_param (pid, int, 0);
MODULE_PARM_DESC (pid, "Proces id to monitor (default 1)");
MODULE_AUTHOR ("Josep Albert Vilaverda i Jofre Mateu corporation");
MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("ProSo driver, monitoritzador d'estadistiques");

EXPORT_SYMBOL(inicialitzar);
EXPORT_SYMBOL(activar_syscall);
EXPORT_SYMBOL(desactivar_syscall);
EXPORT_SYMBOL(mod1_setPID);
EXPORT_SYMBOL(informacio);

/*
* Inicialització del modul
*/
void *crides[NUM_CRIDES] = {my_sys_write, my_sys_close, my_sys_lseek, my_sys_open, my_sys_clone};

int inicialitzat(struct thread_info *proces) {
  int pid;
  printk("<7>NO INICIALITZAT\n");
  if(((struct my_thread_info *)proces)->info.task->pid != ((struct my_thread_info *)proces)->est.pid) {
	pid = ((struct my_thread_info *)proces)->info.task->pid;
	printk("<7>%d\n INICIALITZAT", pid);
	printk("<7>INICIALITZAT\n");
	return 1;
  }
  else return 0;
}

void inicialitzar(struct thread_info *p) {
  int i;
  struct my_thread_info *proces = (struct my_thread_info *) p;
  try_module_get(THIS_MODULE);
  printk("<7>INICIALITZAR\n");
  proces->est.pid = proces->info.task->pid;
  for(i = 0; i < NUM_CRIDES; i++) {
    proces->est.info_crida[i].n_ent = 0;
    proces->est.info_crida[i].n_sort_ok = 0;
    proces->est.info_crida[i].n_sort_err = 0;
    proces->est.info_crida[i].durada_total = 0;
  }
  module_put(THIS_MODULE);
}

void actualitzar(struct thread_info *p, int crida, unsigned long long temps, int ret) {
  struct my_thread_info *proces = (struct my_thread_info *) p;
  proces->est.info_crida[crida].n_ent++;
  proces->est.info_crida[crida].durada_total += temps;
  if(ret) proces->est.info_crida[crida].n_sort_ok++;
  else proces->est.info_crida[crida].n_sort_err++;
}

struct my_thread_info * cerca_proces(int pid) {
  struct task_struct * task;
  for_each_process(task) if(task->pid == pid) return (struct my_thread_info *) task->thread_info;
  return NULL;
}

void imprimir_crida(struct t_info * info) {
  printk("<7>Entrades a la crida:		%d\n", info->n_ent);
  printk("<7>Sortides correctes de la crida:	%d\n", info->n_sort_ok);
  printk("<7>Sortides incorrectes de la crida:	%d\n", info->n_sort_err);
  printk("<7>Temps executant la crida:		%lld\n", info->durada_total);
}

void imprimir_est(struct my_thread_info *proces) {
  int i;
  printk("<7>IMPRIMIM ESTADISTIQUES\n");
  for(i = 0; i < NUM_CRIDES; i++) {
    switch(i) {
    case MY_WRITE:
      printk("<7>\n\nEstadistiques de la crida write:\n");
      break;
    case MY_CLOSE:
      printk("<7>\n\nEstadistiques de la crida close:\n");
      break;
    case MY_LSEEK:
      printk("<7>\n\nEstadistiques de la crida lseek:\n");
      break;
    case MY_OPEN:
      printk("<7>\n\nEstadistiques de la crida open:\n");
      break;
    case MY_CLONE:
      printk("<7>\n\nEstadistiques de la crida clone:\n");
      break;
    }
    imprimir_crida(&proces->est.info_crida[i]);
  }
}

ssize_t my_sys_write(unsigned int fd, const char __user * buf,size_t count) {
  ssize_t (*funcio) (unsigned int fd, const char __user * buf,size_t count);
  ssize_t ret;
  unsigned long long abans, despres;
  struct thread_info *proces = current_thread_info();
  try_module_get(THIS_MODULE);
	printk("<7>WRITE-----------------------------------------------------------------\n");
 
  if(!inicialitzat(proces)) inicialitzar(proces);
  printk("<7>ESTRUCTURES INICIALITZADES\n");
  funcio = crides_old[MY_WRITE];
  //Mesurem el temps, cridem, i tornem a mesurar
  abans = proso_get_cycles();
  ret = funcio(fd, buf, count);
  despres = proso_get_cycles();
  actualitzar(proces, MY_WRITE, despres-abans, (ret >= 0));
  module_put(THIS_MODULE);
  return ret;
}

long my_sys_close(unsigned int fd){
  long (*funcio) (unsigned int fd);
  long ret;
  unsigned long long abans, despres;
  struct thread_info *proces = current_thread_info();
  try_module_get(THIS_MODULE);
	printk("<7>CLOSE-----------------------------------------------------------------\n");

  if(!inicialitzat(proces)) inicialitzar(proces);
  printk("<7>ESTRUCTURES INICIALITZADES\n");
  funcio = crides_old[MY_CLOSE];
  //Mesurem el temps, cridem, i tornem a mesurar
  abans = proso_get_cycles();
  ret = funcio(fd);
  despres = proso_get_cycles();
  actualitzar(proces, MY_CLOSE, despres-abans, (ret >= 0));
  module_put(THIS_MODULE);
  return ret;
}

off_t my_sys_lseek(unsigned int fd, off_t offset, unsigned int origin) {
  off_t (*funcio) (unsigned int fd, off_t offset, unsigned int origin);
  off_t ret;
  unsigned long long abans, despres;
  struct thread_info *proces = current_thread_info();
  try_module_get(THIS_MODULE);
	printk("<7>LSEEK-----------------------------------------------------------------\n");

  if(!inicialitzat(proces)) inicialitzar(proces);
  printk("<7>ESTRUCTURES INICIALITZADES\n");
  funcio = crides_old[MY_LSEEK];
  //Mesurem el temps, cridem, i tornem a mesurar
  abans = proso_get_cycles();
  ret = funcio(fd, offset, origin);
  despres = proso_get_cycles();
  actualitzar(proces, MY_LSEEK, despres-abans, (ret >= 0));
  module_put(THIS_MODULE);
  return ret;
}

long my_sys_open(const char __user * filename, int flags, int mode) {
  long (*funcio) (const char __user * filename, int flags, int mode);
  long ret;
  unsigned long long abans, despres;
  struct thread_info *proces = current_thread_info();
  try_module_get(THIS_MODULE);
	printk("<7>OPEN-----------------------------------------------------------------\n");

  if(!inicialitzat(proces)) inicialitzar(proces);
  printk("<7>ESTRUCTURES INICIALITZADES\n");
  funcio = crides_old[MY_OPEN];
  //Mesurem el temps, cridem, i tornem a mesurar
  abans = proso_get_cycles();
  ret = funcio(filename, flags, mode);
  despres = proso_get_cycles();
  actualitzar(proces, MY_OPEN, despres-abans, (ret >= 0));
  module_put(THIS_MODULE);
  return ret;
}

int my_sys_clone(struct pt_regs regs){
  int (*funcio) (struct pt_regs regs);
  int ret;
  unsigned long long abans, despres;
  struct thread_info *proces = current_thread_info();
  try_module_get(THIS_MODULE);
	printk("<7>CLONE-----------------------------------------------------------------\n");

  if(!inicialitzat(proces)) inicialitzar(proces);
  printk("<7>ESTRUCTURES INICIALITZADES\n");
  funcio = crides_old[MY_CLONE];
  //Mesurem el temps, cridem, i tornem a mesurar
  abans = proso_get_cycles();
  ret = funcio(regs);
  despres = proso_get_cycles();
  actualitzar(proces, MY_CLONE, despres-abans, (ret >= 0));
  module_put(THIS_MODULE);
  return ret;
}

static int __init Mymodule_init(void) {
	/* Codi d'inicialització */
	printk("<7>INIT-----------------------------------------------------\n");
	crides_old[MY_WRITE] = sys_call_table[POS_WRITE];
	crides_old[MY_CLOSE] = sys_call_table[POS_CLOSE];
	crides_old[MY_LSEEK] = sys_call_table[POS_LSEEK];
	crides_old[MY_OPEN] = sys_call_table[POS_OPEN];
	crides_old[MY_CLONE] = sys_call_table[POS_CLONE];
	sys_call_table[POS_WRITE] = crides[MY_WRITE];
	sys_call_table[POS_CLOSE] = crides[MY_CLOSE];
	sys_call_table[POS_LSEEK] = crides[MY_LSEEK];
	sys_call_table[POS_OPEN] = crides[MY_OPEN];
	sys_call_table[POS_CLONE] = crides[MY_CLONE];
	
	printk("<7>Mymodule carregat amb exit\n");
	return 0;
	/*Aquesta funció retorna 0 si tot va correctament i < 0 en cas d'error*/
}

static void __exit Mymodule_exit(void) {
        struct my_thread_info * proces;
	printk("<7>EXIT-----------------------------------------------------\n");
	/* Codi de finalització */
	sys_call_table[POS_WRITE] = crides_old[MY_WRITE];
	sys_call_table[POS_CLOSE] = crides_old[MY_CLOSE];
	sys_call_table[POS_LSEEK] = crides_old[MY_LSEEK];
	sys_call_table[POS_OPEN] = crides_old[MY_OPEN];
	sys_call_table[POS_CLONE] = crides_old[MY_CLONE];

        proces = cerca_proces(pid);
        if(proces == NULL) printk("<7>El proces no existeix\n");
        else if(!inicialitzat((struct thread_info *)proces)) {
	  printk("<7>El proces existeix pero no esta inicialitzat\n");
          inicialitzar((struct thread_info *)proces);
          imprimir_est(proces);
        }
        else imprimir_est(proces);
}

void activar_syscall(int crida) {
	try_module_get(THIS_MODULE);
	if(crida < 0) {
		sys_call_table[POS_WRITE] = crides[MY_WRITE];
		sys_call_table[POS_CLOSE] = crides[MY_CLOSE];
		sys_call_table[POS_LSEEK] = crides[MY_LSEEK];
		sys_call_table[POS_OPEN] = crides[MY_OPEN];
		sys_call_table[POS_CLONE] = crides[MY_CLONE];	
	}
	else {
		switch(crida) {
		case MY_WRITE:
			sys_call_table[POS_WRITE] = crides[MY_WRITE];
			printk("<7>Crida a sistema WRITE activada\n");
      			break;
    		case MY_CLOSE:
      			sys_call_table[POS_CLOSE] = crides[MY_CLOSE];
			printk("<7>Crida a sistema CLOSE activada\n");
      			break;
    		case MY_LSEEK:
      			sys_call_table[POS_LSEEK] = crides[MY_LSEEK];
			printk("<7>Crida a sistema LSEEK activada\n");
      			break;
    		case MY_OPEN:
      			sys_call_table[POS_OPEN] = crides[MY_OPEN];
			printk("<7>Crida a sistema OPEN activada\n");
     			break;
   		case MY_CLONE:
     			sys_call_table[POS_CLONE] = crides[MY_CLONE];
			printk("<7>Crida a sistema CLONE activada\n");
      			break;	
		default:
			printk("<7>Aquesta crida no te suport per a la monitoritzacio\n");
		}
	}
	module_put(THIS_MODULE);
}

void desactivar_syscall(int crida) {
	try_module_get(THIS_MODULE);
	if(crida < 0) {
		sys_call_table[POS_WRITE] = crides_old[MY_WRITE];
		sys_call_table[POS_CLOSE] = crides_old[MY_CLOSE];
		sys_call_table[POS_LSEEK] = crides_old[MY_LSEEK];
		sys_call_table[POS_OPEN] = crides_old[MY_OPEN];
		sys_call_table[POS_CLONE] = crides_old[MY_CLONE];	
	}
	else {
		switch(crida) {
		case MY_WRITE:
			sys_call_table[POS_WRITE] = crides_old[MY_WRITE];
			printk("<7>Crida a sistema WRITE desactivada\n");
      			break;
    		case MY_CLOSE:
      			sys_call_table[POS_CLOSE] = crides_old[MY_CLOSE];
			printk("<7>Crida a sistema CLOSE desactivada\n");
      			break;
    		case MY_LSEEK:
      			sys_call_table[POS_LSEEK] = crides_old[MY_LSEEK];
			printk("<7>Crida a sistema LSEEK desactivada\n");
      			break;
    		case MY_OPEN:
      			sys_call_table[POS_OPEN] = crides_old[MY_OPEN];
			printk("<7>Crida a sistema OPEN desactivada\n");
     			break;
   		case MY_CLONE:
     			sys_call_table[POS_CLONE] = crides_old[MY_CLONE];
			printk("<7>Crida a sistema CLONE desactivada\n");
      			break;	
		default:
			printk("<7>Aquesta crida no te suport per a la monitoritzacio\n");
		}
	}
	module_put(THIS_MODULE);
}

void mod1_setPID(int p) {
	try_module_get(THIS_MODULE);
	pid = p;
	module_put(THIS_MODULE);
}

struct t_info informacio(struct thread_info *p, int crida) {
	struct my_thread_info *proces = (struct my_thread_info *) p;
	try_module_get(THIS_MODULE);
	return proces->est.info_crida[crida];
	module_put(THIS_MODULE);
}

module_init(Mymodule_init);
module_exit(Mymodule_exit);

#include "mymodul.h"
#include "mymodul2.h"

MODULE_AUTHOR("Josep Albert Vilaverda i Jofre Mateu corporation");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("ProSo driver, consultor d'estadistiques");

/*
* Inicialització del modul
*/

char obert;
int pid_open;
struct cdev *my_cdev;
dev_t dev;
int crida;

struct file_operations myfop = {
	owner : THIS_MODULE,
	read : mod_read,
	ioctl : mod_ioctl,
	open : mod_open,
	release : mod_release,
};

int mod_open(struct inode *i, struct file *f) {
	if(current->euid != 0) return -EPERM;
	if(obert) return -EPERM;
        obert = 1;
	pid_open = current->pid;
	return 0;
}

int mod_release (struct inode *i, struct file *f) {
	obert = 0;
	return 0;
}

ssize_t mod_read(struct file *f, char __user *buffer, size_t s, loff_t *off) {
      struct t_info info;
      int res;
      info = informacio(current_thread_info(), crida);
      res = copy_to_user(buffer, &info, sizeof(struct t_info)); 
      return res;
}

int mod_ioctl(struct inode *i, struct file *f, unsigned int arg1, unsigned long arg2) {
      int j; 
      char t;
      struct task_struct *task;
      switch(arg1) {
	case 0:
		if((void *)arg2 == NULL) {
			mod1_setPID(pid_open); 
			return 0;
		}
		j = copy_from_user((void *)arg2, &j, sizeof(int)); 
		if(j < 0) return j;
		t = 0;
		for_each_process(task) if(task->pid == j) t = 1;
		if(!t) return -1;
		mod1_setPID(j);
		return 0;
	case 1:
		crida = arg2; 
		return 0;
	case 2:
		inicialitzar(current_thread_info());
		return 0;
	case 3:
		for_each_process(task) inicialitzar(task->thread_info);
		return 0;
	case 4:
		activar_syscall((int)arg2);
		return 0;
	case 5:
		desactivar_syscall((int)arg2);
		return 0;
	}
	return -1;
}

static int __init Mymodule_init(void) {
	/* Codi d'inicialització */
	
	int error = alloc_chrdev_region(&dev,0, COUNT, "mymodul2");
	if(error < 0) return error; 

        dev = MKDEV(MAJOR(dev), MINOR(dev));

	
	my_cdev = cdev_alloc(); //pot donar error, retorna null si falla
	if (my_cdev == NULL) return -ENOSPC;

	my_cdev->owner = THIS_MODULE;
	my_cdev->ops = &myfop;
	error = cdev_add(my_cdev, dev, COUNT); //pot donar error, retorna <0 si falla
	if(error < 0) return error;
	
	obert = 0;

  	printk(KERN_INFO "Mymodule carregat amb exit\n");
	return 0;
	/*Aquesta funció retorna 0 si tot va correctament i < 0 en cas d'error*/
}

static void __exit Mymodule_exit(void) {
	/* Codi de finalització */
	cdev_del(my_cdev);
	unregister_chrdev_region(dev, COUNT);	
}

module_init(Mymodule_init);
module_exit(Mymodule_exit);

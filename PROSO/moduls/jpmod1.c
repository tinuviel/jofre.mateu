#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define module_path "module1.ko"
#define module_name "module1"

inline int load(int pid, const char * path) {
    int ret, child;
    char pidarg[20];
    printf("Load\n");
    sprintf(pidarg,"pid=%d", pid);
    
    child = fork();
    if (!child) {
        close(2);
        execlp("sudo insmod", "sudo insmod", path, pidarg, NULL);
    }
    waitpid(child, &ret, 0);
    return 0;
}

inline int unload() {
    int ret, child;
    printf("Unload\n");
    child = fork();
    if (!child) {
        close(2);
        execlp("rmmod", "rmmod", module_name, NULL);
    }
    waitpid(child, &ret, 0);
    return 0;
}

int main(int argc, char *argv[]) {
  int fd;
  printf("Main\n");
  load(getpid(),module_path);
  fd = open("fitxer",O_CREAT|O_RDWR);
  write(fd,"hola\n",5);	
  lseek(fd,0,SEEK_END);
  //unload();
}
